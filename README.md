# vitaman

#### 项目介绍
vitaman=vitamin+man。一个应用于拼车的免费发布和搜索平台。维他命，即维生素，虽然默默无为，却对身体健康发挥着不可磨灭的作用。早出晚归的我们，不管男女，都是汉子。这一群vitaman是社会的顶梁柱，人民的希望，祖国的未来。中文名“哎哎拼车”，来源于原始QQ群顺路出行，取自日常陌生人打招呼习惯用语“哎哎”，响应国家号召，提倡绿色出行，为低碳生活出一份力。

#### 部署说明
- 这是一个jfinal项目，启动类为AppConfig，你懂得，不懂就去学习
- src/main/resources/config/system.properties用于配置使用哪个配置文件及是否打开开发模式
- src/main/resources/config/set文件夹下为jdbc配置
- src/main/resources/config/xml下文项目配置，调用方式为BtgSet.getXXXX
- db文件下有sql脚本，数据库为mysql，若需要支持mb4，推荐使用vitaman_mb4.sql
- 有问题，可以咨询我QQ，1715068078或者发邮件jounzhang@126.com
