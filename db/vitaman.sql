/*
Navicat MySQL Data Transfer

Source Server         : zcltd_mysql_3307@root
Source Server Version : 50725
Source Host           : 47.104.16.231:3307
Source Database       : a

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2019-05-30 14:44:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_tj
-- ----------------------------
DROP TABLE IF EXISTS `t_tj`;
CREATE TABLE `t_tj` (
  `tj_rq` date NOT NULL COMMENT '统计日期',
  `yh_sl` decimal(12,0) DEFAULT NULL COMMENT '用户数量',
  `xc_sl_c` decimal(12,0) DEFAULT NULL COMMENT '行程数量（车）',
  `xc_sl_r` decimal(12,0) DEFAULT NULL COMMENT '行程数量（人）',
  `hb_sl_c` decimal(12,0) DEFAULT NULL COMMENT '伙伴数量（车）',
  `hb_sl_r` decimal(12,0) DEFAULT NULL COMMENT '伙伴数量（人）',
  `tj_sj` datetime DEFAULT NULL COMMENT '统计时间',
  PRIMARY KEY (`tj_rq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='统计';

-- ----------------------------
-- Records of t_tj
-- ----------------------------

-- ----------------------------
-- Table structure for t_yh
-- ----------------------------
DROP TABLE IF EXISTS `t_yh`;
CREATE TABLE `t_yh` (
  `key_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
  `bh` decimal(12,0) DEFAULT NULL COMMENT '编号（平台唯一）',
  `lx` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '类型（默认为车，可手动切换）：C-车；R-人；',
  `union_id` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '微信unionid',
  `open_id` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '微信openid',
  `xm` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '姓名（如有性别追加在后面）',
  `sjh` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机号',
  `sjh_is_gk` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机号是否公开：YES-是；NO-否；',
  `zh_qq` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '账号-qq',
  `zh_wx` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '账号-微信',
  `zh_zfb` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '账号-支付宝',
  `cl_hm` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '车辆号码',
  `cl_ppxh` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '车辆品牌型号（如比亚迪唐、大众高尔夫、丰田卡罗拉）',
  `cl_ys` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '车辆颜色（白色、黑色、红色等）',
  `cl_lx` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '车辆类型：JC-轿车；SUV-SUV；',
  `cl_img` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '车辆照片（相对路径）',
  `mm` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '密码（MD5）',
  `hyz` decimal(12,0) DEFAULT NULL COMMENT '活跃值（发布、拼车等增加活跃值，活跃值越高越靠前）',
  `is_ty_xy` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '是否同意协议：YES-是；NO-否；',
  `zc_sj` datetime DEFAULT NULL COMMENT '注册时间',
  `dx_ts` decimal(12,0) DEFAULT NULL COMMENT '短信条数',
  `sy_is_kq_xc` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '声音是否开启-行程：YES-是；NO-否；',
  `dx_is_kq_sq` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '短信是否开启-申请：YES-是；NO-否；',
  `dx_is_kq_qr` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '短信是否开启-确认：YES-是；NO-否；',
  `dx_is_kq_jj` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '短信是否开启拒绝：YES-是；NO-否；',
  PRIMARY KEY (`key_id`),
  UNIQUE KEY `uk_yh_sjh` (`sjh`),
  KEY `uk_yh_bh` (`bh`),
  KEY `idx_yh_unionid` (`union_id`),
  KEY `idx_yh_openid` (`open_id`),
  KEY `idx_yh_hyz` (`hyz`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户';

-- ----------------------------
-- Records of t_yh
-- ----------------------------

-- ----------------------------
-- Table structure for t_yh_hyz
-- ----------------------------
DROP TABLE IF EXISTS `t_yh_hyz`;
CREATE TABLE `t_yh_hyz` (
  `key_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
  `yh_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户id',
  `rq` date DEFAULT NULL COMMENT '日期',
  `lx` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '类型：QD-签到；FB-发布；LX-联系；',
  `sl` decimal(12,0) DEFAULT NULL COMMENT '数量',
  `hq_sj` datetime DEFAULT NULL COMMENT '获取时间',
  PRIMARY KEY (`key_id`),
  KEY `idx_yh_hyz_yhid` (`yh_id`),
  KEY `idx_yh_hyz_rqlx` (`rq`,`lx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户活跃值';

-- ----------------------------
-- Records of t_yh_hyz
-- ----------------------------

-- ----------------------------
-- Table structure for t_yh_lx
-- ----------------------------
DROP TABLE IF EXISTS `t_yh_lx`;
CREATE TABLE `t_yh_lx` (
  `key_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
  `lx` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '类型：C-车；R-人；',
  `yh_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户id',
  `mc` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '名称',
  `cf_sj` datetime DEFAULT NULL COMMENT '出发时间（默认出发时间）',
  `sl` decimal(1,0) DEFAULT NULL COMMENT '数量（数量，为车表示座位数，为人表示人数）',
  `je` decimal(6,0) DEFAULT NULL COMMENT '金额',
  `dd_str` text COLLATE utf8mb4_bin COMMENT '地点（如站点1-站点2-站点3）',
  `bz` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`key_id`),
  KEY `idx_yh_lx_lx` (`lx`),
  KEY `idx_yh_lx_yhid` (`yh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户路线（预设，用于快速发布）';

-- ----------------------------
-- Records of t_yh_lx
-- ----------------------------

-- ----------------------------
-- Table structure for t_yh_xc
-- ----------------------------
DROP TABLE IF EXISTS `t_yh_xc`;
CREATE TABLE `t_yh_xc` (
  `key_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
  `xc_bh` decimal(12,0) DEFAULT NULL COMMENT '行程编号',
  `lx` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '类型：C-车；R-人；',
  `yh_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户id',
  `mc` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '名称',
  `cf_sj` datetime DEFAULT NULL COMMENT '出发时间',
  `sl` decimal(1,0) DEFAULT NULL COMMENT '数量（数量，为车表示座位数，为人表示人数）',
  `je` decimal(6,0) DEFAULT NULL COMMENT '金额',
  `dd_str` text COLLATE utf8mb4_bin COMMENT '地点（如站点1-站点2-站点3）',
  `bz` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `fb_sj` datetime DEFAULT NULL COMMENT '发布时间',
  `is_default` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '是否默认：YES-是；NO-否；',
  `zt` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '状态：DK-打开；GB-关闭；',
  PRIMARY KEY (`key_id`),
  KEY `uk_yh_xc_bh` (`xc_bh`),
  KEY `idx_yh_xc_lx` (`lx`),
  KEY `idx_yh_xc_yhid` (`yh_id`),
  KEY `idx_yh_xc_cfsj` (`cf_sj`),
  KEY `idx_yh_xc_zt` (`zt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户行程';

-- ----------------------------
-- Records of t_yh_xc
-- ----------------------------

-- ----------------------------
-- Table structure for t_yh_xc_hb
-- ----------------------------
DROP TABLE IF EXISTS `t_yh_xc_hb`;
CREATE TABLE `t_yh_xc_hb` (
  `key_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
  `lx` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '类型：C-车；R-人；',
  `xc_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '行程id',
  `hb_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '伙伴id',
  `ly` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '来源：ZD-主动（别人主动申请）；BD-被动（自己主动申请，别人被申请）；',
  `ly_xc_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '来源行程id',
  `zt` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '状态：SQ-申请；QR-确认；JJ-拒绝；SC-删除；',
  `sq_sj` datetime DEFAULT NULL COMMENT '申请时间',
  `qr_sj` datetime DEFAULT NULL COMMENT '确认时间',
  `jj_sj` datetime DEFAULT NULL COMMENT '拒绝时间',
  `sc_sj` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`key_id`),
  KEY `idx_yh_xc_hb_lx` (`lx`),
  KEY `idx_yh_xc_hb_xcidhbid` (`xc_id`,`hb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户行程伙伴';

-- ----------------------------
-- Records of t_yh_xc_hb
-- ----------------------------

-- ----------------------------
-- Table structure for t_yh_xc_hbhis
-- ----------------------------
DROP TABLE IF EXISTS `t_yh_xc_hbhis`;
CREATE TABLE `t_yh_xc_hbhis` (
  `key_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
  `lx` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '类型：C-车；R-人；',
  `xc_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '行程id',
  `hb_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '伙伴id',
  `ly` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '来源：ZD-主动（别人主动申请）；BD-被动（自己主动申请，别人被申请）；',
  `ly_xc_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '来源行程id',
  `zt` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '状态：SQ-申请；QR-确认；JJ-拒绝；SC-删除；',
  `sq_sj` datetime DEFAULT NULL COMMENT '申请时间',
  `qr_sj` datetime DEFAULT NULL COMMENT '确认时间',
  `jj_sj` datetime DEFAULT NULL COMMENT '拒绝时间',
  `sc_sj` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户行程伙伴历史';

-- ----------------------------
-- Records of t_yh_xc_hbhis
-- ----------------------------

-- ----------------------------
-- Table structure for t_yh_xc_his
-- ----------------------------
DROP TABLE IF EXISTS `t_yh_xc_his`;
CREATE TABLE `t_yh_xc_his` (
  `key_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
  `xc_bh` decimal(12,0) DEFAULT NULL COMMENT '行程编号',
  `lx` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '类型：C-车；R-人；',
  `yh_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户id',
  `mc` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '名称',
  `cf_sj` datetime DEFAULT NULL COMMENT '出发时间',
  `sl` decimal(1,0) DEFAULT NULL COMMENT '数量（数量，为车表示座位数，为人表示人数）',
  `je` decimal(6,0) DEFAULT NULL COMMENT '金额',
  `dd_str` text COLLATE utf8mb4_bin COMMENT '地点（如站点1-站点2-站点3）',
  `bz` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `fb_sj` datetime DEFAULT NULL COMMENT '发布时间',
  `is_default` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '是否默认：YES-是；NO-否；',
  `zt` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '状态：DK-打开；GB-关闭；',
  PRIMARY KEY (`key_id`),
  KEY `uk_yh_xc_his_xcbh` (`xc_bh`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户行程历史';

-- ----------------------------
-- Records of t_yh_xc_his
-- ----------------------------

-- ----------------------------
-- Table structure for _btgsession
-- ----------------------------
DROP TABLE IF EXISTS `_btgsession`;
CREATE TABLE `_btgsession` (
  `si` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'session id',
  `so` blob COMMENT 'session对象',
  `lat` bigint(20) DEFAULT NULL COMMENT '最后访问时间',
  `mi` bigint(20) DEFAULT NULL COMMENT '最大空闲时间，即session的失效时间',
  PRIMARY KEY (`si`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='分布式session存储';

-- ----------------------------
-- Records of _btgsession
-- ----------------------------
