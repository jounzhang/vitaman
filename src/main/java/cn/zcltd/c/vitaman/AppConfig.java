package cn.zcltd.c.vitaman;

import cn.zcltd.btg.httpsession.BTGSessionDao;
import cn.zcltd.btg.httpsession.impl.BTGDBSessionDao;
import cn.zcltd.btg.httpsession.servlet.interceptor.BTGHttpSessionInterceptor;
import cn.zcltd.btg.httpsession.servlet.plugin.BTGSessionPlugin;
import cn.zcltd.btg.jfinal.autoroute.AutoRoutesAnnotation;
import cn.zcltd.btg.set.core.BtgSet;
import cn.zcltd.btg.set.sourcemanager.SourceManager;
import cn.zcltd.btg.set.sourcemanager.XmlFileSourceManager;
import cn.zcltd.c.vitaman.controller.BaseController;
import cn.zcltd.c.vitaman.controller.IndexController;
import cn.zcltd.c.vitaman.core.controller.BtgController;
import cn.zcltd.c.vitaman.core.handler.ServletExcludeHandler;
import cn.zcltd.c.vitaman.core.handler.StaticUploadHandler;
import cn.zcltd.c.vitaman.core.intercepter.ExceptionInterceptor;
import cn.zcltd.c.vitaman.core.intercepter.GlobalInterceptor;
import cn.zcltd.c.vitaman.core.intercepter.SessionTimeoutInterceptor;
import cn.zcltd.c.vitaman.gen.GenJs;
import cn.zcltd.c.vitaman.task.TaskXcAutoGb;
import cn.zcltd.c.vitaman.task.TaskXcAutoSc;
import cn.zcltd.c.vitaman.task.TaskXcTj;
import com.alibaba.druid.filter.logging.Slf4jLogFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.*;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.server.undertow.PathKitExt;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;
import com.jfinal.template.source.ClassPathSourceFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * jfinal核心配置类
 */
public class AppConfig extends JFinalConfig {
    private final Logger log = Logger.getLogger(AppConfig.class);

    static {
        System.setProperty("sysLogDir", SysConfig.getSysLogDir());
    }

    /**
     * 配置常量
     */
    @Override
    public void configConstant(Constants me) {
        me.setDevMode(SysConfig.isSysDevModel());

        //初始化开发模式log4j
        if (SysConfig.isSysDevModel()) {
            PropertyConfigurator.configure(PathKit.getRootClassPath() + "/log4j-dev.properties");
        }

        //初始化配置管理
        SourceManager sourceManager = new XmlFileSourceManager("xml", PathKit.getRootClassPath() + "/config/xml", "UTF-8");
        BtgSet.init(SysConfig.isSysDevModel(), sourceManager, SysConfig.getSysCode());

        me.setBaseUploadPath(BtgSetConfig.sysDefaultTempPath());
    }

    /**
     * 配置路由
     */
    @Override
    public void configRoute(Routes me) {
        me.setMappingSuperClass(true);

        AutoRoutesAnnotation ara = AutoRoutesAnnotation.create(me);
        ara.setClassPath(PathKit.getRootClassPath());

        //jar包部署模式时从jar包加载
        ara.setJarPath(PathKitExt.getLocationPath());
        ara.addJar("vitaman-.*\\.jar");

        //设置要映射package及namespace
        ara.addPackage("cn.zcltd.c.vitaman.controller");

        //设置默认控制器(将"/"映射到默认控制器)
        ara.setDefaultController(IndexController.class);

        //设置忽略的类
        ara.addSkip(BtgController.class);
        ara.addSkip(BaseController.class);

        //启动路由
        ara.route();
    }

    @Override
    public void configEngine(Engine me) {
        me.setDevMode(SysConfig.isSysDevModel());
        me.addSharedFunction("/page/common/_layout.html");
    }

    /**
     * 配置插件
     */
    @Override
    public void configPlugin(Plugins me) {
        DruidPlugin druidPlugin = new DruidPlugin(SysConfig.getJdbcUrl(), SysConfig.getJdbcUsername(), SysConfig.getJdbcPassword());

        WallFilter wall = new WallFilter();
        wall.setDbType("mysql");
        druidPlugin.addFilter(wall);

        Slf4jLogFilter slf4jLogFilter = new Slf4jLogFilter();
        slf4jLogFilter.setDataSourceLogEnabled(false);
        slf4jLogFilter.setConnectionLogEnabled(false);
        slf4jLogFilter.setStatementLogEnabled(false);
        slf4jLogFilter.setResultSetLogEnabled(false);
        slf4jLogFilter.setStatementExecutableSqlLogEnable(true);
        druidPlugin.addFilter(slf4jLogFilter);
        me.add(druidPlugin);

        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        arp.setDialect(new MysqlDialect()); // 设置方言
        arp.setShowSql(false);
        arp.setContainerFactory(new CaseInsensitiveContainerFactory(true)); // 忽略大小写

        arp.getEngine().setSourceFactory(new ClassPathSourceFactory()); //sql加载位置
        arp.setBaseSqlTemplatePath("/sql"); //sql模板路径
        arp.addSqlTemplate("all.s");

        _MappingKit.mapping(arp);
        me.add(arp);

        BTGSessionDao dbSessionDao = new BTGDBSessionDao();
        BTGSessionPlugin sessionPlugin = new BTGSessionPlugin(dbSessionDao);
        sessionPlugin.setSessionIdKey(BtgSetConfig.sysSessionIdKey());
        sessionPlugin.setSessionTimeoutMillis(BtgSetConfig.sysSessionTimeoutMillis());
        me.add(sessionPlugin);
    }

    /**
     * 配置全局拦截器
     */
    @Override
    public void configInterceptor(Interceptors me) {
        me.add(new ExceptionInterceptor());
        me.add(new GlobalInterceptor());
        me.add(new BTGHttpSessionInterceptor());
        me.add(new SessionTimeoutInterceptor());
    }

    /**
     * 配置处理器
     */
    public void configHandler(Handlers me) {
        me.add(new ServletExcludeHandler());
        me.add(new StaticUploadHandler());
    }

    /**
     * 启动检查
     */
    @Override
    public void onStart() {
        // 检测数据库是否正常
        try {
            Db.findFirst("select 1");
            log.info("数据库连接成功");
        } catch (Exception e) {
            log.error("数据库连接异常，请确认配置是否正确", e);
        }

        //创建一个基础路径为rootClassPath的引擎用于生成文件
        Engine genEngine = Engine.create("gen");
        genEngine.setDevMode(true);
        genEngine.setBaseTemplatePath(PathKit.getRootClassPath());

        GenJs.config();

        TaskXcAutoGb.getInstance().start();
        TaskXcAutoSc.getInstance().start();
        TaskXcTj.getInstance().start();

        System.out.println("==================================================");
        System.out.println("vitaman 启动成功");
        System.out.println("==================================================");
    }

    /**
     * main
     */
    public static void main(String[] args) {

        UndertowServer.start(AppConfig.class);
    }
}