package cn.zcltd.c.vitaman;

import cn.zcltd.btg.set.core.BtgSet;

/**
 * btgset配置访问
 */
public class BtgSetConfig {

    public static String name() {
        return BtgSet.getString("name");
    }

    public static String sysDefaultUploadPath() {
        String path = BtgSet.getString("sysDefaultUploadPath");
        path = path.replaceAll("\\\\", "/");
        path = path.endsWith("/") ? path : path + "/";
        return path;
    }

    public static String sysDefaultDownloadPath() {
        String path = BtgSet.getString("sysDefaultDownloadPath");
        path = path.replaceAll("\\\\", "/");
        path = path.endsWith("/") ? path : path + "/";
        return path;
    }

    public static String sysDefaultTempPath() {
        String path = BtgSet.getString("sysDefaultTempPath");
        path = path.replaceAll("\\\\", "/");
        path = path.endsWith("/") ? path : path + "/";
        return path;
    }

    public static String sysDefaultPassword() {
        return BtgSet.getString("sysDefaultPassword");
    }

    public static int sysDefaultPageIndex() {
        return BtgSet.getInteger("sysDefaultPageIndex");
    }

    public static int sysDefaultPageSize() {
        return BtgSet.getInteger("sysDefaultPageSize");
    }

    public static String sysSessionIdKey() {
        return BtgSet.getString("sysSessionIdKey");
    }

    public static int sysSessionTimeoutMillis() {
        return BtgSet.getInteger("sysSessionTimeoutMillis");
    }

    public static String jzZfbUrl() {
        return BtgSet.getString("jzZfbUrl");
    }

    public static int taskXcAutoGbMinutes() {
        return BtgSet.getInteger("taskXcAutoGbMinutes");
    }

    public static int taskXcAutoGbSleepSeconds() {
        return BtgSet.getInteger("taskXcAutoGbSleepSeconds");
    }

    public static int taskXcAutoScMinutes() {
        return BtgSet.getInteger("taskXcAutoScMinutes");
    }

    public static int taskXcAutoScSleepSeconds() {
        return BtgSet.getInteger("taskXcAutoScSleepSeconds");
    }

    public static int taskXcTjSleepSeconds() {
        return BtgSet.getInteger("taskXcTjSleepSeconds");
    }

    public static String jzWxUrl() {
        return BtgSet.getString("jzWxUrl");
    }

    public static String smsConfigAccessKeyId() {
        return BtgSet.getString("smsConfigAccessKeyId");
    }

    public static String smsConfigAccessKeySecret() {
        return BtgSet.getString("smsConfigAccessKeySecret");
    }

    public static String smsConfigSignName() {
        return BtgSet.getString("smsConfigSignName");
    }

    public static String h5KeyWords() {
        return BtgSet.getString("h5KeyWords");
    }

    public static String h5Desc() {
        return BtgSet.getString("h5Desc");
    }

    public static int h5ZcDxTs() {
        return BtgSet.getInteger("h5ZcDxTs");
    }

    public static int h5TjTs() {
        return BtgSet.getInteger("h5TjTs");
    }

    public static Integer xcCCcNum() {
        return BtgSet.getInteger("xcCCcNum");
    }

    public static Integer xcRCcNum() {
        return BtgSet.getInteger("xcRCcNum");
    }

    public static Integer xcCSlMin() {
        return BtgSet.getInteger("xcCSlMin");
    }

    public static Integer xcCSlMax() {
        return BtgSet.getInteger("xcCSlMax");
    }

    public static Integer xcRSlMin() {
        return BtgSet.getInteger("xcRSlMin");
    }

    public static Integer xcRSlMax() {
        return BtgSet.getInteger("xcRSlMax");
    }

    public static Integer xcFbMaxNum() {
        return BtgSet.getInteger("xcFbMaxNum");
    }

    public static Integer hyzSlQd() {
        return BtgSet.getInteger("hyzSlQd");
    }

    public static Integer hyzSlFb() {
        return BtgSet.getInteger("hyzSlFb");
    }

    public static Integer hyzSlLx() {
        return BtgSet.getInteger("hyzSlLx");
    }
}