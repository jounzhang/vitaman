package cn.zcltd.c.vitaman;

/**
 * 存放常量的类
 */
public class Constant {

    /*
        session名称
     */
    public final static String SESSION_KEY_LOGINUSER = "loginUser";//登录用户
    public final static String SESSION_KEY_VALIDATE_IMAGE = "validateImage";//验证码

    /*
        操作标识
     */
    public final static String MGN_TYPE_ADD = "I";//新增
    public final static String MGN_TYPE_UPDATE = "U";//修改
    public final static String MGN_TYPE_DELETE = "D";//删除

    /*
        返回信息
     */
    public static final String RESP_KEY_RESP = "resp";
    public static final String RESP_KEY_RESP_CODE = "resp_code";
    public static final String RESP_KEY_RESP_DESC = "resp_desc";
    public static final String RESP_KEY_BIZ_CODE = "biz_code";
    public static final String RESP_KEY_BIZ_DESC = "biz_desc";
    public static final String RESP_KEY_TIMESTAMP = "timestamp";
    public static final String RESP_KEY_DATA = "data";
    public static final String RESP_KEY_TOTAL = "total";

    public static final String RESP_RESPCODE_SUCCESS = "0000";
    public static final String RESP_RESPDESC_SUCCESS = "请求成功";
    public static final String RESP_RESPCODE_ERROR = "9999";
    public static final String RESP_RESPDESC_ERROR = "系统出错，请联系管理员";
    public static final String RESP_RESPCODE_SESSION_TIMEOUT = "-1";
    public static final String RESP_RESPDESC_SESSION_TIMEOUT = "登录过期，请重新登录";
    public static final String RESP_RESPCODE_WTY_XX = "-2";
    public static final String RESP_RESPDESC_WTY_XX = "请先阅读使用协议并同意";
    public static final String RESP_BIZCODE_SUCCESS = "0000";
    public static final String RESP_BIZDESC_SUCCESS = "操作成功";
    public static final String RESP_BIZCODE_ERROR = "9999";
    public static final String RESP_BIZDESC_ERROR = "系统出错，请联系管理员";

    /*
        其他
     */
    public static final String PAGE_KEY_INDEX = "pageIndex";//分页页码参数名
    public static final String PAGE_KEY_SIZE = "pageSize";//分页大小参数名

    public static final String UPLOAD_STATUS = "work_status";//处理状态

    public static final String TAG_UNCHECK_SESSION_TIMEOUT = "uncheckSessionTimeout";//不拦截session的标记tag
    public static final String TAG_NO_VALID_PARAM = "noValidParam";//不验证请求参数的标记tag
}