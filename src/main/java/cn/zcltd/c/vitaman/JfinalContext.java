package cn.zcltd.c.vitaman;

import cn.zcltd.btg.set.core.BtgSet;
import cn.zcltd.btg.set.sourcemanager.SourceManager;
import cn.zcltd.btg.set.sourcemanager.XmlFileSourceManager;
import com.alibaba.druid.filter.logging.Slf4jLogFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;
import com.jfinal.template.source.ClassPathSourceFactory;
import org.apache.log4j.PropertyConfigurator;

/**
 * jfinal环境手工初始化
 */
public class JfinalContext {

    public static void init() {
        PropertyConfigurator.configure(PathKit.getRootClassPath() + "/log4j-dev.properties");//初始化log4j

        //初始化配置管理
        SourceManager sourceManager = new XmlFileSourceManager("xml", PathKit.getRootClassPath() + "/config/xml", "UTF-8");
        BtgSet.init(SysConfig.isSysDevModel(), sourceManager, SysConfig.getSysCode());

        DruidPlugin druidPlugin = new DruidPlugin(SysConfig.getJdbcUrl(), SysConfig.getJdbcUsername(), SysConfig.getJdbcPassword());

        WallFilter wall = new WallFilter();
        wall.setDbType("mysql");
        druidPlugin.addFilter(wall);

        Slf4jLogFilter slf4jLogFilter = new Slf4jLogFilter();
        slf4jLogFilter.setDataSourceLogEnabled(false);
        slf4jLogFilter.setConnectionLogEnabled(false);
        slf4jLogFilter.setStatementLogEnabled(false);
        slf4jLogFilter.setResultSetLogEnabled(false);
        slf4jLogFilter.setStatementExecutableSqlLogEnable(true);
        druidPlugin.addFilter(slf4jLogFilter);
        druidPlugin.start();

        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        arp.setDialect(new MysqlDialect()); // 设置方言
        arp.setShowSql(false);
        arp.setContainerFactory(new CaseInsensitiveContainerFactory(true)); // 忽略大小写

        arp.getEngine().setSourceFactory(new ClassPathSourceFactory()); //sql加载位置
        arp.setBaseSqlTemplatePath("/sql"); //sql模板路径
        arp.addSqlTemplate("all.s");

        _MappingKit.mapping(arp);
        arp.start();

        Engine engine = Engine.use();
        engine.setBaseTemplatePath(PathKit.getRootClassPath());
    }
}