package cn.zcltd.c.vitaman;

import cn.zcltd.btg.sutil.EmptyUtil;
import cn.zcltd.btg.sutil.StringUtil;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;

/**
 * 系统参数配置
 */
public class SysConfig {
    private static String SYS_CODE;
    private static boolean SYS_DEV_MODEL;
    private static String SYS_VERSION;
    private static String SYS_LOG_DIR;

    private static String JDBC_URL;
    private static String JDBC_USERNAME;
    private static String JDBC_PASSWORD;

    private static void log(String str) {
        System.out.println(str);
    }

    static {
        try {
            int titleLenth = 25;

            Prop sysProp = PropKit.use("config/system.properties");
            SYS_CODE = sysProp.get("sysCode");
            if (EmptyUtil.isEmpty(SYS_CODE)) {
                throw new RuntimeException("未获取到系统配置[sysCode]");
            }
            SYS_VERSION = sysProp.get("sysVersion");
            if (EmptyUtil.isEmpty(SYS_VERSION)) {
                throw new RuntimeException("未获取到系统配置[sysVersion]");
            }
            SYS_DEV_MODEL = sysProp.getBoolean("sysDevModel");
            if (EmptyUtil.isEmpty(SYS_DEV_MODEL)) {
                throw new RuntimeException("未获取到系统配置[sysDevModel]");
            }
            SYS_LOG_DIR = sysProp.get("sysLogDir");

            Prop setProp = PropKit.use("config/set/" + SYS_CODE + ".properties");
            JDBC_URL = setProp.get("jdbcUrl");
            if (EmptyUtil.isEmpty(JDBC_URL)) {
                throw new RuntimeException("未获取到jdbc配置[jdbcUrl]");
            }
            JDBC_USERNAME = setProp.get("jdbcUsername");
            if (EmptyUtil.isEmpty(JDBC_USERNAME)) {
                throw new RuntimeException("未获取到jdbc配置[jdbcUsername]");
            }
            JDBC_PASSWORD = setProp.get("jdbcPassword");
            if (EmptyUtil.isEmpty(JDBC_PASSWORD)) {
                throw new RuntimeException("未获取到jdbc配置[jdbcPassword]");
            }

            if (EmptyUtil.isEmpty(SYS_LOG_DIR)) {//如果全局配置未指定log目录，使用个性化配置，使用user.dir
                SYS_LOG_DIR = setProp.get("sysLogDir");
            }
            if (EmptyUtil.isEmpty(SYS_LOG_DIR)) {//如果全局配置和个性化配置均为配置，使用${user.dir}/logs
                SYS_LOG_DIR = System.getProperty("user.dir") + "/logs";
            }

            log(StringUtil.format2length("", 120, "-"));
            log(StringUtil.format2length("sysVersion：", titleLenth) + SYS_VERSION);
            log(StringUtil.format2length("sysCode：", titleLenth) + SYS_CODE);
            log(StringUtil.format2length("sysDevModel：", titleLenth) + SYS_DEV_MODEL);
            log(StringUtil.format2length("sysLogDir：", titleLenth) + SYS_LOG_DIR);
            log(StringUtil.format2length("", 120, "-"));
            log(StringUtil.format2length("jdbcUrl：", titleLenth) + JDBC_URL);
            log(StringUtil.format2length("jdbcUsername：", titleLenth) + JDBC_USERNAME);
            log(StringUtil.format2length("jdbcPassword：", titleLenth) + JDBC_PASSWORD);
            log(StringUtil.format2length("", 120, "-"));
        } catch (Exception e) {
            throw new RuntimeException("加载环境配置文件出错，" + e.getMessage());
        }
    }

    public static String getSysCode() {
        return SYS_CODE;
    }

    public static boolean isSysDevModel() {
        return SYS_DEV_MODEL;
    }

    public static String getSysVersion() {
        return SYS_VERSION;
    }

    public static String getSysLogDir() {
        return SYS_LOG_DIR;
    }

    public static String getJdbcUrl() {
        return JDBC_URL;
    }

    public static String getJdbcUsername() {
        return JDBC_USERNAME;
    }

    public static String getJdbcPassword() {
        return JDBC_PASSWORD;
    }
}