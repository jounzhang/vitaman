package cn.zcltd.c.vitaman;

import cn.zcltd.c.vitaman.dao.*;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

/**
 * Generated by JFinal, do not modify this file.
 * <pre>
 * Example:
 * public void configPlugin(Plugins me) {
 *     ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
 *     _MappingKit.mapping(arp);
 *     me.add(arp);
 * }
 * </pre>
 */
public class _MappingKit {

	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("t_tj", "tj_rq", Tj.class);
		arp.addMapping("t_yh", "key_id", Yh.class);
		arp.addMapping("t_yh_hyz", "key_id", YhHyz.class);
		arp.addMapping("t_yh_lx", "key_id", YhLx.class);
		arp.addMapping("t_yh_xc", "key_id", YhXc.class);
		arp.addMapping("t_yh_xc_hb", "key_id", YhXcHb.class);
		arp.addMapping("t_yh_xc_hbhis", "key_id", YhXcHbhis.class);
		arp.addMapping("t_yh_xc_his", "key_id", YhXcHis.class);
	}
}