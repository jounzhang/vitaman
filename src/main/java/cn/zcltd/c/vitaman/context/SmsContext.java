package cn.zcltd.c.vitaman.context;

import cn.zcltd.btg.core.exception.BtgBizRuntimeException;
import cn.zcltd.btg.sutil.DateUtil;
import cn.zcltd.btg.sutil.EmptyUtil;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 短信验证码上下文
 */
public class SmsContext {
    private static final Map<String, SmsRecord> codeContext = new ConcurrentHashMap<>();
    private static final SmsContext context = new SmsContext();

    private static long timer = 1000 * 60 * 2;//2分钟内不能重复获取验证码

    private SmsContext() {

    }

    public static SmsContext getContext() {
        return context;
    }

    /**
     * 设置验证码
     *
     * @param sjh  用户id
     * @param code 验证码
     */
    public void setCode(String token, String sjh, String code) {
        SmsRecord smsRecord = codeContext.get(token);
        if (EmptyUtil.isNotEmpty(smsRecord) && DateUtil.getNow().getTime() - smsRecord.getLast() <= timer) {
            throw new BtgBizRuntimeException("资源有限，10分钟内不能重复获取，请稍后重试");
        }
        codeContext.put(token, new SmsRecord(sjh, code));
    }

    /**
     * 设置验证码
     *
     * @param token 用户id
     * @param code  验证码
     * @return 是否验证通过
     */
    public boolean validCode(String token, String code) {
        SmsRecord smsRecord = codeContext.get(token);
        return EmptyUtil.isNotEmpty(smsRecord) && smsRecord.getCode().equals(code);
    }

    /**
     * 销毁验证码
     *
     * @param token 用户id
     */
    public void dieCode(String token) {
        codeContext.remove(token);
    }

    private class SmsRecord {
        private String token;
        private String code;
        private long last;

        public SmsRecord(String token, String code) {
            this.token = token;
            this.code = code;
            this.last = DateUtil.getNow().getTime();
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public long getLast() {
            return last;
        }

        public void setLast(long last) {
            this.last = last;
        }
    }
}