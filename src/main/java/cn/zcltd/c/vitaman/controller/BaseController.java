package cn.zcltd.c.vitaman.controller;

import cn.zcltd.btg.sutil.EmptyUtil;
import cn.zcltd.c.vitaman.BtgSetConfig;
import cn.zcltd.c.vitaman.Constant;
import cn.zcltd.c.vitaman.core.controller.BtgController;
import cn.zcltd.c.vitaman.core.model.PageJson;
import cn.zcltd.c.vitaman.dao.Yh;
import cn.zcltd.c.vitaman.dao.YhLx;
import cn.zcltd.c.vitaman.dao.YhXc;
import com.jfinal.core.NotAction;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Page;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 控制器基类：提供基础controller扩展
 */
public class BaseController extends BtgController {

    @Override
    @NotAction
    public String getPara(String name) {
        String value = super.getPara(name);
        return EmptyUtil.isNotEmpty(value) ? value.replaceAll(" ", "").trim() : value;
    }

    @Override
    @NotAction
    public String getPara(int index) {
        String value = super.getPara(index);
        return EmptyUtil.isNotEmpty(value) ? value.replaceAll(" ", "").trim() : value;
    }

    @Override
    @NotAction
    public String getPara(int index, String defaultValue) {
        String value = super.getPara(index, defaultValue);
        return EmptyUtil.isNotEmpty(value) ? value.replaceAll(" ", "").trim() : value;
    }

    @Override
    @NotAction
    public String getPara(String name, String defaultValue) {
        String value = super.getPara(name, defaultValue);
        return EmptyUtil.isNotEmpty(value) ? value.replaceAll(" ", "").trim() : value;
    }

    @NotAction
    public Map<String, String> builtResp() {
        Map<String, String> respMap = getAttr(Constant.RESP_KEY_RESP);
        if (EmptyUtil.isEmpty(respMap)) {
            respMap = new HashMap<>();
            respMap.put(Constant.RESP_KEY_RESP_CODE, Constant.RESP_RESPCODE_SUCCESS);
            respMap.put(Constant.RESP_KEY_RESP_DESC, Constant.RESP_RESPDESC_SUCCESS);
            setAttr(Constant.RESP_KEY_RESP, respMap);
        }
        return respMap;
    }

    @NotAction
    public void setRespInfo(String respCode, String respDesc) {
        Map<String, String> respMap = builtResp();
        respMap.put(Constant.RESP_KEY_RESP_CODE, respCode);
        respMap.put(Constant.RESP_KEY_RESP_DESC, respDesc);
        setAttr(Constant.RESP_KEY_RESP, respMap);
    }

    @NotAction
    public void setRespDescSuccess(String respDesc) {
        setRespInfo(Constant.RESP_RESPCODE_SUCCESS, respDesc);
    }

    @NotAction
    public void setRespDescSuccess() {
        setRespDescSuccess(Constant.RESP_RESPDESC_SUCCESS);
    }

    @NotAction
    public void setRespDescError(String respDesc) {
        setRespInfo(Constant.RESP_RESPCODE_ERROR, respDesc);
    }

    @NotAction
    public void setRespDescError() {
        setRespDescError(Constant.RESP_RESPDESC_ERROR);
    }

    @NotAction
    public void setBizInfo(String bizCode, String bizDesc) {
        Map<String, String> respMap = builtResp();
        respMap.put(Constant.RESP_KEY_BIZ_CODE, bizCode);
        respMap.put(Constant.RESP_KEY_BIZ_DESC, bizDesc);
        setAttr(Constant.RESP_KEY_RESP, respMap);
    }

    @NotAction
    public void setBizDescSuccess(String respDesc) {
        setBizInfo(Constant.RESP_BIZCODE_SUCCESS, respDesc);
    }

    @NotAction
    public void setBizDescSuccess() {
        setBizDescSuccess(Constant.RESP_BIZDESC_SUCCESS);
    }

    @NotAction
    public void setBizDescError(String respDesc) {
        setBizInfo(Constant.RESP_BIZCODE_ERROR, respDesc);
    }

    @NotAction
    public void setBizDescError() {
        setBizInfo(Constant.RESP_BIZCODE_ERROR, Constant.RESP_BIZDESC_ERROR);
    }

    /**
     * 设置返回数据
     *
     * @param data 数据集
     */
    @NotAction
    public void setData(Object data) {
        setAttr(Constant.RESP_KEY_DATA, data);
    }

    /**
     * 设置分页
     *
     * @param page 分页
     */
    @NotAction
    public void setPage(Page page) {
        setAttr(Constant.RESP_KEY_DATA, page.getList());
        setAttr(Constant.RESP_KEY_TOTAL, page.getTotalRow());
    }

    /**
     * 设置分页
     *
     * @param page 分页
     */
    @NotAction
    public void setPage(PageJson page) {
        setAttr(Constant.RESP_KEY_DATA, page.getList());
        setAttr(Constant.RESP_KEY_TOTAL, page.getTotalRow());
    }

    /**
     * 获取分页参数page_index
     *
     * @return 当前页码
     */
    @NotAction
    public int getPageIndex() {
        return getParaToInt(Constant.PAGE_KEY_INDEX, BtgSetConfig.sysDefaultPageIndex());
    }

    /**
     * 获取分页参数page_size
     *
     * @return 每页大小
     */
    @NotAction
    public int getPageSize() {
        return getParaToInt(Constant.PAGE_KEY_SIZE, BtgSetConfig.sysDefaultPageSize());
    }

    /**
     * 渲染结果
     */
    @NotAction
    public void renderResult() {
        Map<String, String> respMap = builtResp();

        String respCode = respMap.get(Constant.RESP_KEY_RESP_CODE);
        if (EmptyUtil.isEmpty(respCode)) {
            setRespDescSuccess();
        }

        if (Constant.RESP_RESPCODE_SUCCESS.equals(respCode)) {
            String bizCode = respMap.get(Constant.RESP_KEY_BIZ_CODE);
            if (EmptyUtil.isEmpty(bizCode)) {
                setBizDescSuccess();
            }
        } else {
            respMap.remove(Constant.RESP_KEY_BIZ_CODE);
            respMap.remove(Constant.RESP_KEY_BIZ_DESC);

            Enumeration<String> attrNames = getAttrNames();
            while (attrNames.hasMoreElements()) {
                String attrName = attrNames.nextElement();
                if (!Constant.RESP_KEY_RESP.equals(attrName)) {
                    removeAttr(attrName);
                }
            }
        }

        super.renderJson();
    }

    //////////////////////////

    /**
     * 设置登录用户
     *
     * @param yh 用户
     */
    public void setSessionUser(Yh yh) {
        setSessionAttr(Constant.SESSION_KEY_LOGINUSER, yh);
    }

    /**
     * 获取登录用户
     *
     * @return 用户
     */
    @NotAction
    public Yh getSessionUser() {
        return getSessionAttr(Constant.SESSION_KEY_LOGINUSER);
    }

    /**
     * 初始化参数，信息检查
     */
    public void initParaCheck(boolean checkJbxx, boolean checkClxx, boolean checkLx, boolean checkXc) {
        Yh yh = getSessionUser();

        yh = Yh.dao.findById(yh.getKeyId());

        //是否完善基础信息
        if (checkJbxx) {
            setAttr("_jbxx", EmptyUtil.isEmpty(yh.getXm()) && EmptyUtil.isEmpty(yh.getZhQq()) && EmptyUtil.isEmpty(yh.getZhWx()) && EmptyUtil.isEmpty(yh.getZhZfb()) ? "NO" : "YES");
        }

        //是否完善车辆信息
        if (checkClxx) {
            setAttr("_clxx", EmptyUtil.isEmpty(yh.getClHm()) && EmptyUtil.isEmpty(yh.getClPpxh()) && EmptyUtil.isEmpty(yh.getClYs()) && EmptyUtil.isEmpty(yh.getClLx()) ? "NO" : "YES");
        }

        //是否设置预设行程
        if (checkLx) {
            setAttr("_sl_lx", YhLx.dao.countByLxYhId(yh.getLx(), yh.getKeyId()));
        }

        //是否发布默认行程
        if (checkXc) {
            setAttr("_sl_xc_default", EmptyUtil.isNotEmpty(YhXc.dao.getDefaultByLxYhId(yh.getLx(), yh.getKeyId())) ? 1 : 0);
        }
    }

    /**
     * 初始化参数，用于页面顶部
     * 车找人、人找车、注册用户
     */
    public void initParaTop() {
        setAttr("_sl_c", YhXc.dao.countByLxZt("C", "DK"));
        setAttr("_sl_r", YhXc.dao.countByLxZt("R", "DK"));
        setAttr("_sl_yh", Yh.dao.countByKv(Kv.create()));
    }
}