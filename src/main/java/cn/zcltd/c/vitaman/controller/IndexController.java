package cn.zcltd.c.vitaman.controller;

import cn.zcltd.btg.core.exception.BtgBizRuntimeException;
import cn.zcltd.btg.httpsession.kit.SessionKit;
import cn.zcltd.btg.sutil.DateUtil;
import cn.zcltd.btg.sutil.EmptyUtil;
import cn.zcltd.btg.sutil.FileUtil;
import cn.zcltd.btg.sutil.Identities;
import cn.zcltd.btg.sutil.encrypt.Encryption;
import cn.zcltd.c.vitaman.BtgSetConfig;
import cn.zcltd.c.vitaman.context.SmsContext;
import cn.zcltd.c.vitaman.dao.*;
import cn.zcltd.c.vitaman.service.SmsService;
import cn.zcltd.c.vitaman.util.MyUtil;
import cn.zcltd.c.vitaman.util.SmsUtil;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.exceptions.ClientException;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;

import java.io.File;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

/**
 * 默认action
 */
public class IndexController extends BaseController {

    /**
     * 跳转到首页
     */
    public void index() {
        String xc_bh = getPara(0);

        if (EmptyUtil.isEmpty(xc_bh)) {//如果未获取到了行程跳转到行程信息，跳转到首页
            redirect("/home");
            return;
        }

        YhXc yhXc = YhXc.dao.getByXcBh(xc_bh);
        if (EmptyUtil.isEmpty(yhXc)) {//如果未获取到了行程跳转到行程信息，跳转到首页
            redirect("/home");
            return;
        }

        Yh xcYh = Yh.dao.findById(yhXc.getYhId());

        if (DateUtil.formatDate(yhXc.getCfSj()).equals(DateUtil.formatDate(DateUtil.getNow()))) {
            yhXc.put("cf_rq_lx", "JR");
        } else if (DateUtil.formatDate(yhXc.getCfSj()).equals(DateUtil.formatDate(DateUtil.dateAdd(DateUtil.getNow(), Calendar.DATE, 1)))) {
            yhXc.put("cf_rq_lx", "MR");
        } else {
            yhXc.put("cf_rq_lx", "QT");
        }
        yhXc.put("cf_rq", DateUtil.formatDate(yhXc.getCfSj()));
        yhXc.put("cf_sj", DateUtil.format(yhXc.getCfSj(), "H:" + DateUtil.DEFAULT_PATTERN_MINUTE));
        yhXc.put("fb_sj", DateUtil.formatDateTime(yhXc.getFbSj()));
        yhXc.put("bh", xcYh.getBh());

        Yh thisUser = getSessionUser();
        if (EmptyUtil.isNotEmpty(thisUser)) {
            YhXcHb yhXcHb = YhXcHb.dao.getByXcIdHbId(yhXc.getKeyId(), thisUser.getKeyId());
            yhXc.put("is_sq", EmptyUtil.isNotEmpty(yhXcHb) ? "YES" : "NO");
        } else {
            yhXc.put("is_sq", "NO");
        }

        //组装车辆型号
        String cl_xh = "";
        if (EmptyUtil.isNotEmpty(xcYh.getClPpxh())) {
            cl_xh += xcYh.getClPpxh();
        }
        if (EmptyUtil.isNotEmpty(xcYh.getClYs())) {
            if (EmptyUtil.isNotEmpty(cl_xh)) {
                cl_xh += "-";
            }
            cl_xh += xcYh.getClYs();
        }
        if (EmptyUtil.isNotEmpty(xcYh.getClLx())) {
            if (EmptyUtil.isNotEmpty(cl_xh)) {
                cl_xh += "-";
            }
            String cl_lx = xcYh.getClLx();
            if (cl_lx.equals("JC")) {
                cl_lx = "轿车";
            }
            cl_xh += cl_lx;
        }
        yhXc.put("cl_xh", cl_xh);

        yhXc.put("xm", xcYh.getXm());

        //处理车牌号
        String clHm = xcYh.getClHm();
        if (EmptyUtil.isNotEmpty(clHm)) {
            if (clHm.length() > 4) {
                clHm = clHm.substring(0, 2) + "***" + clHm.substring(4);
            }
        } else {
            clHm = null;
        }
        yhXc.put("cl_hm", clHm);

        //处理手机号
        if (xcYh.getSjhIsGk().equals("NO")) {
            String sjh = xcYh.getSjh();
            yhXc.put("sjh", sjh.substring(0, 3) + "****" + sjh.substring(7));
        } else {
            yhXc.put("sjh", xcYh.getSjh());
        }

        setData(yhXc);

        initParaTop();

        render("/page/xc_info.html");
    }

    /**
     * 统计
     */
    public void tj() {
        Page<Tj> page = Tj.dao.pageByKv(1, BtgSetConfig.h5TjTs(), Kv.create());

        for (Tj tj : page.getList()) {
            tj.put("tj_rq", DateUtil.format(tj.getTjRq(), DateUtil.DEFAULT_PATTERN_MONTH + "-" + DateUtil.DEFAULT_PATTERN_DAY));
        }

        setData(Kv.by("list", page.getList()));

        render("/page/tj.html");
    }

    /**
     * 获取验证码
     */
    public void yzm() throws ClientException {
        String sjh = getPara("sjh");
        String code = SmsUtil.genSmsCode();

        SmsContext.getContext().setCode(sjh, sjh, code);

        SmsService.me.sendCode(sjh, code);
    }

    /**
     * 注册
     */
    public void zc() {
        render("/page/zc.html");
    }

    /**
     * 注册保存
     */
    public void zcSave() {
        String sjh = getPara("sjh");
        String pwd = getPara("pwd");
        String yzh = getPara("yzh");

        Yh yh = Yh.dao.getBySjh(sjh);
        if (EmptyUtil.isNotEmpty(yh)) {
            setBizDescError("手机号已被注册");
            return;
        }

        if (!SmsContext.getContext().validCode(sjh, yzh)) {
            setBizDescError("验证码错误");
            return;
        }

        SmsContext.getContext().dieCode(sjh);

        yh = new Yh();
        yh.setKeyId(Identities.uuid());
        yh.setBh(Yh.dao.getNextUserNo());
        yh.setLx("C");//默认为车找人
        yh.setSjh(sjh);
        yh.setSjhIsGk("NO");//默认不公开手机号
        yh.setMm(Encryption.encryptMD5ToHex(pwd));
        yh.setHyz(new BigDecimal(0));
        yh.setIsTyXy("NO");
        yh.setZcSj(DateUtil.getNow());
        yh.setDxTs(new BigDecimal(BtgSetConfig.h5ZcDxTs()));
        yh.setSyIsKqXc("YES");
        yh.setDxIsKqSq("YES");
        yh.setDxIsKqQr("YES");
        yh.setDxIsKqJj("YES");
        yh.save();
    }

    /**
     * 登录
     */
    public void dl() {
        render("/page/dl.html");
    }

    /**
     * 登录保存
     */
    public void dlSave() {
        String sjh = getPara("sjh");
        String pwd = getPara("pwd");

        Yh yh = Yh.dao.getBySjh(sjh);
        if (EmptyUtil.isEmpty(yh)) {
            setBizDescError("手机号或密码错误");
            return;
        }

        if (!Encryption.encryptMD5ToHex(pwd).equals(yh.getMm())) {
            setBizDescError("手机号或密码错误");
            return;
        }

        setSessionUser(yh);
    }

    /**
     * 密码找回
     */
    public void mmZh() {
        render("/page/mm_zh.html");
    }

    /**
     * 密码找回保存
     */
    public void mmZhSave() {
        String sjh = getPara("sjh");
        String pwd = getPara("pwd");
        String yzh = getPara("yzh");

        Yh yh = Yh.dao.getBySjh(sjh);
        if (EmptyUtil.isEmpty(yh)) {
            setBizDescError("手机号尚未注册");
            return;
        }

        if (!SmsContext.getContext().validCode(sjh, yzh)) {
            setBizDescError("验证码错误");
            return;
        }

        SmsContext.getContext().dieCode(sjh);

        yh.setMm(Encryption.encryptMD5ToHex(pwd));
        yh.update();
    }

    /**
     * 密码修改
     */
    public void mmXg() {
        render("/page/mm_xg.html");
    }

    /**
     * 密码修改保存
     */
    public void mmXgSave() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String pwd_old = getPara("pwd_old");
        String pwd = getPara("pwd");

        if (!Encryption.encryptMD5ToHex(pwd_old).equals(thisUser.getMm())) {
            setBizDescError("原始密码错误");
            return;
        }

        thisUser = Yh.dao.findById(thisUser.getKeyId());
        thisUser.setMm(Encryption.encryptMD5ToHex(pwd));
        thisUser.update();
    }

    /**
     * 切换
     */
    public void qh() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        if (thisUser.getLx().equals("C")) {
            thisUser.setLx("R");
        } else {
            thisUser.setLx("C");
        }
        thisUser.update();
    }

    /**
     * 跳转到首页
     */
    public void home() {
        setData(Kv.by("pageIndex", getPageIndex()).set("pageSize", getPageSize()).set("cf_rq", DateUtil.formatDate(DateUtil.getNow())));

        initParaTop();

        initParaCheck(true, true, false, true);

        render("/page/home.html");
    }

    /**
     * 获取首页行程
     */
    public void homeXc() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String ddBegin = getPara("ddBegin");
        String ddEnd = getPara("ddEnd");
        String HHBegin = getPara("HHBegin");
        String mmBegin = getPara("mmBegin");
        String HHEnd = getPara("HHEnd");
        String mmEnd = getPara("mmEnd");
        String cf_rq = getPara("cf_rq");

        Kv kv = Kv.create();
        kv.set("lx", thisUser.getLx().equals("C") ? "R" : "C");
        //kv.set("yh_id_not", yh.getKeyId());

        String cf_sj_begin = "";
        if (EmptyUtil.isNotEmpty(HHBegin)) {
            cf_sj_begin += HHBegin;
        } else {
            cf_sj_begin += "00";
        }
        if (EmptyUtil.isNotEmpty(mmBegin)) {
            cf_sj_begin += ":" + mmBegin + ":00";
        } else {
            cf_sj_begin += ":00:00";
        }
        if (EmptyUtil.isNotEmpty(cf_rq)) {
            kv.set("cf_sj_begin", cf_rq + " " + cf_sj_begin);
        }

        String cf_sj_end = "";
        if (EmptyUtil.isNotEmpty(HHEnd)) {
            cf_sj_end += HHEnd;
        } else {
            cf_sj_end += "23";
        }
        if (EmptyUtil.isNotEmpty(mmEnd)) {
            cf_sj_end += ":" + mmEnd + ":00";
        } else {
            cf_sj_end += ":59:59";
        }
        if (EmptyUtil.isNotEmpty(cf_rq)) {
            kv.set("cf_sj_end", cf_rq + " " + cf_sj_end);
        }

        String ddStrLike = "%" + (EmptyUtil.isNotEmpty(ddBegin) ? ddBegin.replaceAll("%", "") : "") + "%" + (EmptyUtil.isNotEmpty(ddEnd) ? ddEnd.replaceAll("%", "") : "") + "%";
        ddStrLike = ddStrLike.replaceAll("%%", "%").replaceAll("%%", "%");
        if (!ddStrLike.equals("%")) {
            kv.set("dd_str_like", ddStrLike);
        }

        kv.set("hb_id", thisUser.getKeyId());
        kv.set("zt", "DK");

        Page<YhXc> page = YhXc.dao.pageHomeByKv(getPageIndex(), getPageSize(), kv);
        for (YhXc yhXc : page.getList()) {
            if (DateUtil.formatDate(yhXc.getCfSj()).equals(DateUtil.formatDate(DateUtil.getNow()))) {
                yhXc.put("cf_rq_lx", "JR");
            } else if (DateUtil.formatDate(yhXc.getCfSj()).equals(DateUtil.formatDate(DateUtil.dateAdd(DateUtil.getNow(), Calendar.DATE, 1)))) {
                yhXc.put("cf_rq_lx", "MR");
            } else {
                yhXc.put("cf_rq_lx", "QT");
            }
            yhXc.put("cf_rq", DateUtil.formatDate(yhXc.getCfSj()));
            yhXc.put("cf_sj", DateUtil.format(yhXc.getCfSj(), "H:" + DateUtil.DEFAULT_PATTERN_MINUTE));
            yhXc.put("fb_sj", DateUtil.formatDateTime(yhXc.getFbSj()));
            yhXc.put("is_sq", yhXc.getInt("is_sq") > 0 ? "YES" : "NO");

            //搜索关键字红色高亮
            if (EmptyUtil.isNotEmpty(ddBegin)) {
                yhXc.put("mc", yhXc.getMc().replaceAll(ddBegin, "<span class=\"key-word\">" + ddBegin + "</span>"));
                yhXc.put("dd_str", yhXc.getDdStr().replaceAll(ddBegin, "<span class=\"key-word\">" + ddBegin + "</span>"));
            }
            if (EmptyUtil.isNotEmpty(ddEnd)) {
                yhXc.put("mc", yhXc.getMc().replaceAll(ddEnd, "<span class=\"key-word\">" + ddEnd + "</span>"));
                yhXc.put("dd_str", yhXc.getDdStr().replaceAll(ddEnd, "<span class=\"key-word\">" + ddEnd + "</span>"));
            }

            //组装车辆型号
            String cl_xh = "";
            if (EmptyUtil.isNotEmpty(yhXc.getStr("cl_ppxh"))) {
                cl_xh += yhXc.getStr("cl_ppxh");
            }
            if (EmptyUtil.isNotEmpty(yhXc.getStr("cl_ys"))) {
                if (EmptyUtil.isNotEmpty(cl_xh)) {
                    cl_xh += "-";
                }
                cl_xh += yhXc.getStr("cl_ys");
            }
            if (EmptyUtil.isNotEmpty(yhXc.getStr("cl_lx"))) {
                if (EmptyUtil.isNotEmpty(cl_xh)) {
                    cl_xh += "-";
                }
                String cl_lx = yhXc.getStr("cl_lx");
                if (cl_lx.equals("JC")) {
                    cl_lx = "轿车";
                }
                cl_xh += cl_lx;
            }
            yhXc.put("cl_xh", cl_xh);

            //处理车牌号
            String clHm = yhXc.getStr("cl_hm");
            if (EmptyUtil.isNotEmpty(clHm)) {
                if (clHm.length() > 4) {
                    clHm = clHm.substring(0, 2) + "***" + clHm.substring(4);
                }
            } else {
                clHm = null;
            }
            yhXc.put("cl_hm", clHm);

            //处理手机号
            if (yhXc.getStr("sjh_is_gk").equals("NO")) {
                String sjh = yhXc.getStr("sjh");
                yhXc.put("sjh", sjh.substring(0, 3) + "****" + sjh.substring(7));
            }

            //行程复制文本
            yhXc.put("xc_copy", MyUtil.genXcCopy(getRootRequestUrl(), yhXc));
        }

        setData(Kv.by("list", page.getList()).set("total", page.getTotalRow()));
    }

    /**
     * 行程申请
     */
    public void homeXcSq() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String xc_id = getPara("xc_id");

        YhXc yhXc = YhXc.dao.findById(xc_id);
        if (EmptyUtil.isEmpty(yhXc)) {
            setBizDescError("行程已失效，请稍后重试");
            return;
        }
        if (yhXc.getZt().equals("GB")) {
            setBizDescError("行程已关闭，请稍后重试");
            return;
        }
        //目标行程：如果行程为车，检查最大申请数量
        if (yhXc.getLx().equals("C") && YhXcHb.dao.countByXcIdZtNotStr(yhXc.getKeyId(), "JJ,SC") - BtgSetConfig.xcCCcNum() >= yhXc.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue()) {
            setBizDescError("超员违法，你是想坐后备箱啊");
            return;
        }
        //目标行程：如果行程为人，检查最大申请数量
        if (yhXc.getLx().equals("R") && YhXcHb.dao.countByXcIdZtNotStr(yhXc.getKeyId(), "JJ,SC") - BtgSetConfig.xcRCcNum() >= yhXc.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue()) {
            setBizDescError("对面是个大神，太抢手了");
            return;
        }

        YhXc yhXcDefault = YhXc.dao.getDefaultByLxYhId(thisUser.getLx(), thisUser.getKeyId());
        if (EmptyUtil.isEmpty(yhXcDefault)) {
            setBizDescError("您尚未发布任何行程，先到“行程”中进行发布");
            return;
        }
        if (yhXcDefault.getYhId().equals(yhXc.getYhId())) {
            setBizDescError("简直乱来噢，自己不能跟自己拼");
            return;
        }
        if (yhXcDefault.getLx().equals(yhXc.getLx())) {
            setBizDescError("尴尬了，对方也是" + (yhXc.getLx().equals("C") ? "车找人" : "人找车"));
            return;
        }
        //我的行程：如果行程为车，检查最大申请数量
        if (yhXcDefault.getLx().equals("C") && YhXcHb.dao.countByXcIdZtNotStr(yhXcDefault.getKeyId(), "JJ,SC") - BtgSetConfig.xcCCcNum() >= yhXcDefault.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue()) {
            setBizDescError("超员违法，你是想赛后备箱啊");
            return;
        }
        //我的行程：如果行程为人，检查最大申请数量
        if (yhXcDefault.getLx().equals("R") && YhXcHb.dao.countByXcIdZtNotStr(yhXcDefault.getKeyId(), "JJ,SC") - BtgSetConfig.xcRCcNum() >= yhXcDefault.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue()) {
            setBizDescError("你是有多胖，需要那么多车");
            return;
        }

        if (!DateUtil.formatDate(yhXc.getCfSj()).equals(DateUtil.formatDate(yhXcDefault.getCfSj()))) {
            setBizDescError("出发日期不一致，请确认后再试");
            return;
        }

        YhXcHb yhXcHb = YhXcHb.dao.getByXcIdLyXcId(yhXc.getKeyId(), yhXcDefault.getKeyId());
        if (EmptyUtil.isNotEmpty(yhXcHb)) {
            setBizDescError("狗屎运好，对方先一步发起了申请");
            return;
        }

        Yh finalThisUser = thisUser;
        Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {

                //主动申请目标行程
                YhXcHb yhXcHbZd = new YhXcHb();
                yhXcHbZd.setKeyId(Identities.uuid());
                yhXcHbZd.setXcId(yhXc.getKeyId());
                yhXcHbZd.setLx(yhXc.getLx());
                yhXcHbZd.setHbId(finalThisUser.getKeyId());
                yhXcHbZd.setZt("SQ");
                yhXcHbZd.setLy("ZD");
                yhXcHbZd.setLyXcId(yhXcDefault.getKeyId());
                yhXcHbZd.setSqSj(DateUtil.getNow());
                yhXcHbZd.save();

                //为自己行程建立记录
                YhXcHb yhXcHbBd = new YhXcHb();
                yhXcHbBd.setKeyId(Identities.uuid());
                yhXcHbBd.setXcId(yhXcDefault.getKeyId());
                yhXcHbBd.setLx(yhXcDefault.getLx());
                yhXcHbBd.setHbId(finalThisUser.getKeyId());
                yhXcHbBd.setZt("SQ");
                yhXcHbBd.setLy("BD");
                yhXcHbBd.setLyXcId(yhXc.getKeyId());
                yhXcHbBd.setSqSj(DateUtil.getNow());
                yhXcHbBd.save();

                //计算活跃值
                YhHyz.dao.addHyz(finalThisUser.getKeyId(), DateUtil.getNow(), "LX", new BigDecimal(BtgSetConfig.hyzSlLx()));

                //短信通知-申请
                Yh xcYh = Yh.dao.findById(yhXc.getYhId());
                if (xcYh.getDxIsKqSq().equals("YES")) {
                    if (xcYh.getDxTs().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() > 0) {
                        SmsService.me.xcTzSq(xcYh.getSjh(), finalThisUser.getBh().toString() + (EmptyUtil.isNotEmpty(finalThisUser.getXm()) ? finalThisUser.getXm() : ""), DateUtil.format(yhXc.getCfSj(), "MM月dd日HH:mm"), SmsUtil.checkSmsText(yhXc.getMc().replaceAll("-", "到")));
                        xcYh.setDxTs(xcYh.getDxTs().subtract(new BigDecimal(1)));
                        xcYh.update();

                        if (xcYh.getDxTs().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() <= 0) {
                            Yh.dao.closeAllDxTs(xcYh);
                        }
                    }
                }

                return true;
            }
        });
    }

    /**
     * 行程
     */
    public void xc() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        setData(Kv.by("pageIndex", getPageIndex()).set("pageSize", getPageSize()).set("sy_is_kq_xc", thisUser.getSyIsKqXc()));

        initParaTop();

        initParaCheck(true, true, true, true);

        render("/page/xc.html");
    }

    /**
     * 行程常用
     */
    public void xcCy() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        //查询常用路线
        List<YhLx> yhLxList = YhLx.dao.listByLxYhId(thisUser.getLx(), thisUser.getKeyId());
        for (YhLx yhLx : yhLxList) {
            yhLx.put("cf_sj", DateUtil.format(yhLx.getCfSj(), "H:" + DateUtil.DEFAULT_PATTERN_MINUTE));
        }

        setData(Kv.by("lx_list", yhLxList));

        initParaCheck(false, false, true, false);

        render("/page/xc_lx.html");
    }

    /**
     * 常用路线编辑后发布
     */
    public void xcCyFb() {
        String lx_id = getPara("lx_id");

        YhLx yhLx = YhLx.dao.findById(lx_id);

        yhLx.put("cf_rq", DateUtil.formatDate(DateUtil.getNow()));
        yhLx.put("cf_sj_h", Integer.parseInt(DateUtil.format(yhLx.getCfSj(), DateUtil.DEFAULT_PATTERN_HOUR)));
        yhLx.put("cf_sj_m", Integer.parseInt(DateUtil.format(yhLx.getCfSj(), DateUtil.DEFAULT_PATTERN_MINUTE)));

        setData(yhLx);

        render("/page/xc_lx_fb.html");
    }

    /**
     * 行程复制信息
     */
    public void xcFz() {
        String xc_id = getPara("xc_id");

        YhXc yhXc = YhXc.dao.findById(xc_id);

        if (DateUtil.formatDate(yhXc.getCfSj()).equals(DateUtil.formatDate(DateUtil.getNow()))) {
            yhXc.put("cf_rq_lx", "JR");
        } else if (DateUtil.formatDate(yhXc.getCfSj()).equals(DateUtil.formatDate(DateUtil.dateAdd(DateUtil.getNow(), Calendar.DATE, 1)))) {
            yhXc.put("cf_rq_lx", "MR");
        } else {
            yhXc.put("cf_rq_lx", "QT");
        }
        yhXc.put("cf_rq", DateUtil.formatDate(yhXc.getCfSj()));

        yhXc.put("cf_sj", DateUtil.format(yhXc.getCfSj(), "H:" + DateUtil.DEFAULT_PATTERN_MINUTE));

        setData(Kv.by("xc_copy", MyUtil.genXcCopy(getRootRequestUrl(), yhXc)));
    }

    /**
     * 行程列表
     */
    public void xcList() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        Kv kv = Kv.create();
        kv.set("lx", thisUser.getLx());
        kv.set("yh_id", thisUser.getKeyId());

        Page<YhXc> page = YhXc.dao.pageByKv(getPageIndex(), getPageSize(), kv);

        String isWcl = "NO";
        for (YhXc yhXc : page.getList()) {
            if (DateUtil.formatDate(yhXc.getCfSj()).equals(DateUtil.formatDate(DateUtil.getNow()))) {
                yhXc.put("cf_rq_lx", "JR");
            } else if (DateUtil.formatDate(yhXc.getCfSj()).equals(DateUtil.formatDate(DateUtil.dateAdd(DateUtil.getNow(), Calendar.DATE, 1)))) {
                yhXc.put("cf_rq_lx", "MR");
            } else {
                yhXc.put("cf_rq_lx", "QT");
            }
            yhXc.put("cf_rq", DateUtil.formatDate(yhXc.getCfSj()));

            yhXc.put("cf_sj", DateUtil.format(yhXc.getCfSj(), "H:" + DateUtil.DEFAULT_PATTERN_MINUTE));

            //查看行程申请记录
            List<YhXcHb> yhXcHbList = YhXcHb.dao.listByXcIdZtNot(yhXc.getKeyId(), "SC");
            for (YhXcHb yhXcHb : yhXcHbList) {
                if (thisUser.getSyIsKqXc().equals("YES") && yhXcHb.getLy().equals("ZD") && yhXcHb.getZt().equals("SQ")) {
                    isWcl = "YES";
                }

                if (DateUtil.formatDate(yhXcHb.getDate("cf_sj")).equals(DateUtil.formatDate(DateUtil.getNow()))) {
                    yhXcHb.put("cf_rq_lx", "JR");
                } else if (DateUtil.formatDate(yhXcHb.getDate("cf_sj")).equals(DateUtil.formatDate(DateUtil.dateAdd(DateUtil.getNow(), Calendar.DATE, 1)))) {
                    yhXcHb.put("cf_rq_lx", "MR");
                } else {
                    yhXcHb.put("cf_rq_lx", "QT");
                }
                yhXcHb.put("cf_rq", DateUtil.formatDate(yhXcHb.getDate("cf_sj")));
                yhXcHb.put("cf_sj", DateUtil.format(yhXcHb.getDate("cf_sj"), "H:" + DateUtil.DEFAULT_PATTERN_MINUTE));

                //组装车辆型号
                String cl_xh = "";
                if (EmptyUtil.isNotEmpty(yhXcHb.getStr("cl_ppxh"))) {
                    cl_xh += yhXcHb.getStr("cl_ppxh");
                }
                if (EmptyUtil.isNotEmpty(yhXcHb.getStr("cl_ys"))) {
                    if (EmptyUtil.isNotEmpty(cl_xh)) {
                        cl_xh += "-";
                    }
                    cl_xh += yhXcHb.getStr("cl_ys");
                }
                if (EmptyUtil.isNotEmpty(yhXcHb.getStr("cl_lx"))) {
                    if (EmptyUtil.isNotEmpty(cl_xh)) {
                        cl_xh += "-";
                    }
                    String cl_lx = yhXcHb.getStr("cl_lx");
                    if (cl_lx.equals("JC")) {
                        cl_lx = "轿车";
                    }
                    cl_xh += cl_lx;
                }
                yhXcHb.put("cl_xh", cl_xh);

                String clHm = yhXcHb.getStr("cl_hm");
                yhXcHb.put("cl_hm", EmptyUtil.isNotEmpty(clHm) ? clHm : "未填写");
            }
            yhXc.put("hb_list", yhXcHbList);

            //行程复制文本
            yhXc.put("xc_copy", MyUtil.genXcCopy(getRootRequestUrl(), yhXc));
        }

        setData(Kv.by("list", page.getList()).set("sy_is_kq_xc", thisUser.getSyIsKqXc()).set("is_wcl", isWcl));
    }

    /**
     * 行程发布
     */
    public void xcFb() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        List<YhLx> yhLxList = YhLx.dao.listByLxYhId(thisUser.getLx(), thisUser.getKeyId());
        for (YhLx yhLx : yhLxList) {
            yhLx.put("cf_sj_h", DateUtil.format(yhLx.getCfSj(), "H"));
            yhLx.put("cf_sj_m", DateUtil.format(yhLx.getCfSj(), DateUtil.DEFAULT_PATTERN_MINUTE));
        }

        setData(Kv.by("lx_list", yhLxList).set("cf_rq", DateUtil.formatDate(DateUtil.getNow())));

        render("/page/xc_fb.html");
    }

    /**
     * 行程发布保存
     */
    public void xcFbSave() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String mc = getPara("mc");
        int HHBegin = getInt("HHBegin");
        int mmBegin = getInt("mmBegin");
        String sl = getPara("sl");
        String je = getPara("je");
        String dd_str = getPara("dd_str");
        String bz = getPara("bz");
        String cf_rq = getPara("cf_rq");

        String cfSj = cf_rq + " " + (HHBegin < 10 ? "0" + HHBegin : HHBegin) + ":" + mmBegin + ":00";

        int xcNum = YhXc.dao.countByLxYhId(thisUser.getLx(), thisUser.getKeyId());
        if (xcNum >= BtgSetConfig.xcFbMaxNum()) {
            setBizDescError("行程数量超出系统限制");
            return;
        }

        YhXc yhXcExists = YhXc.dao.getByLxYhIdMcCfSjSlJeDdStrBzKeyIdNot(thisUser.getLx(), thisUser.getKeyId(), mc, cfSj, sl, je, dd_str, bz, null);
        if (EmptyUtil.isNotEmpty(yhXcExists)) {
            setBizDescError("行程已存在");
            return;
        }

        Yh finalThisUser = thisUser;
        Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                YhXc.dao.updateIsDefaultNoByLxYhId(finalThisUser.getLx(), finalThisUser.getKeyId());

                YhXc yhXc = new YhXc();
                yhXc.setKeyId(Identities.uuid());
                yhXc.setXcBh(YhXc.dao.getNextXcBh());
                yhXc.setLx(finalThisUser.getLx());
                yhXc.setYhId(finalThisUser.getKeyId());
                yhXc.setMc(mc);
                yhXc.setCfSj(DateUtil.parseDateTime(cfSj));
                yhXc.setSl(new BigDecimal(sl));
                yhXc.setJe(new BigDecimal(je));
                yhXc.setDdStr(dd_str);
                yhXc.setBz(bz);
                yhXc.setFbSj(DateUtil.getNow());
                yhXc.setZt("DK");
                yhXc.setIsDefault("YES");
                yhXc.save();

                YhHyz.dao.addHyz(finalThisUser.getKeyId(), DateUtil.getNow(), "FB", new BigDecimal(BtgSetConfig.hyzSlFb()));

                return true;
            }
        });
    }

    /**
     * 行程发布（常用路线快速发布）
     */
    public void xcFbLx() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String lx_id = getPara("lx_id");

        int xcNum = YhXc.dao.countByLxYhId(thisUser.getLx(), thisUser.getKeyId());
        if (xcNum >= BtgSetConfig.xcFbMaxNum()) {
            setBizDescError("行程数量超出系统限制");
            return;
        }

        YhLx yhLx = YhLx.dao.findById(lx_id);

        String cfSj = DateUtil.formatDate(DateUtil.getNow()) + " " + DateUtil.formatTime(yhLx.getCfSj());

        YhXc yhXcExists = YhXc.dao.getByLxYhIdMcCfSjSlJeDdStrBzKeyIdNot(thisUser.getLx(), thisUser.getKeyId(), yhLx.getMc(), cfSj, yhLx.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).toString(), yhLx.getJe().setScale(0, BigDecimal.ROUND_HALF_UP).toString(), yhLx.getDdStr(), yhLx.getBz(), null);
        if (EmptyUtil.isNotEmpty(yhXcExists)) {
            setBizDescError("行程已存在");
            return;
        }

        Yh finalThisUser = thisUser;
        Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                YhXc.dao.updateIsDefaultNoByLxYhId(finalThisUser.getLx(), finalThisUser.getKeyId());

                YhXc yhXc = new YhXc();
                yhXc.setKeyId(Identities.uuid());
                yhXc.setXcBh(YhXc.dao.getNextXcBh());
                yhXc.setLx(finalThisUser.getLx());
                yhXc.setYhId(finalThisUser.getKeyId());
                yhXc.setMc(yhLx.getMc());
                yhXc.setCfSj(DateUtil.parseDateTime(cfSj));
                yhXc.setSl(yhLx.getSl());
                yhXc.setJe(yhLx.getJe());
                yhXc.setDdStr(yhLx.getDdStr());
                yhXc.setBz(yhLx.getBz());
                yhXc.setFbSj(DateUtil.getNow());
                yhXc.setZt("DK");
                yhXc.setIsDefault("YES");
                yhXc.save();

                YhHyz.dao.addHyz(finalThisUser.getKeyId(), DateUtil.getNow(), "FB", new BigDecimal(BtgSetConfig.hyzSlFb()));

                return true;
            }
        });
    }

    /**
     * 行程状态
     */
    public void xcZt() {
        String xc_id = getPara("xc_id");
        String zt = getPara("zt");

        YhXc yhXc = YhXc.dao.findById(xc_id);
        yhXc.setZt(zt);
        yhXc.update();
    }

    /**
     * 行程默认（设置默认）
     */
    public void xcDefault() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String xc_id = getPara("xc_id");

        Yh finalThisUser = thisUser;
        Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                YhXc.dao.updateIsDefaultNoByLxYhId(finalThisUser.getLx(), finalThisUser.getKeyId());
                YhXc.dao.updateIsDefaultYesById(xc_id);
                return true;
            }
        });
    }

    /**
     * 行程编辑
     */
    public void xcEdit() {
        String xc_id = getPara("xc_id");
        if (EmptyUtil.isNotEmpty(xc_id)) {
            YhXc yhXc = YhXc.dao.findById(xc_id);
            yhXc.put("cf_rq", DateUtil.formatDate(yhXc.getCfSj()));
            yhXc.put("cf_sj_h", Integer.parseInt(DateUtil.format(yhXc.getCfSj(), DateUtil.DEFAULT_PATTERN_HOUR)));
            yhXc.put("cf_sj_m", Integer.parseInt(DateUtil.format(yhXc.getCfSj(), DateUtil.DEFAULT_PATTERN_MINUTE)));
            setData(yhXc);
        }
        render("/page/xc_edit.html");
    }

    /**
     * 行程编辑保存
     */
    public void xcEditSave() {
        String xc_id = getPara("xc_id");
        String mc = getPara("mc");
        String cf_rq = getPara("cf_rq");
        int HHBegin = getInt("HHBegin");
        int mmBegin = getInt("mmBegin");
        String sl = getPara("sl");
        String je = getPara("je");
        String dd_str = getPara("dd_str");
        String bz = getPara("bz");

        String cfSj = cf_rq + " " + (HHBegin < 10 ? "0" + HHBegin : HHBegin) + ":" + mmBegin + ":00";

        YhXc yhXc = YhXc.dao.findById(xc_id);

        YhXc yhXcExists = YhXc.dao.getByLxYhIdMcCfSjSlJeDdStrBzKeyIdNot(yhXc.getLx(), yhXc.getYhId(), mc, cfSj, sl, je, dd_str, bz, xc_id);
        if (EmptyUtil.isNotEmpty(yhXcExists)) {
            setBizDescError("行程已存在");
            return;
        }

        yhXc.setMc(mc);
        yhXc.setCfSj(DateUtil.parseDateTime(cfSj));
        yhXc.setSl(new BigDecimal(sl));
        yhXc.setJe(new BigDecimal(je));
        yhXc.setDdStr(dd_str);
        yhXc.setBz(bz);
        yhXc.update();
    }

    /**
     * 行程数量加
     */
    public void xcSlAdd() {
        String xc_id = getPara("xc_id");

        YhXc yhXc = YhXc.dao.findById(xc_id);

        int slMax = yhXc.getLx().equals("C") ? BtgSetConfig.xcCSlMax() : BtgSetConfig.xcRSlMax();
        int sqSl = YhXcHb.dao.countByXcIdZtNotStr(yhXc.getKeyId(), "JJ,SC");
        slMax -= sqSl;
        if (yhXc.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() >= slMax) {
            setBizDescError("加不动了");
            return;
        }

        yhXc.setSl(yhXc.getSl().add(new BigDecimal(1)));
        yhXc.update();

        setData(Kv.by("sl", yhXc.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue()));
    }

    /**
     * 行程数量减
     */
    public void xcSlMinus() {
        String xc_id = getPara("xc_id");

        YhXc yhXc = YhXc.dao.findById(xc_id);

        int slMin = yhXc.getLx().equals("C") ? BtgSetConfig.xcCSlMin() : BtgSetConfig.xcRSlMin();
        if (yhXc.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() <= slMin) {
            setBizDescError("减到底了");
            return;
        }

        yhXc.setSl(yhXc.getSl().subtract(new BigDecimal(1)));
        if (yhXc.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() == 0) {
            yhXc.setZt("GB");
        }
        yhXc.update();

        setData(Kv.by("sl", yhXc.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue()));
    }

    /**
     * 行程备注（直接修改备注）
     */
    public void xcBz() {
        String xc_id = getPara("xc_id");
        String bz = getPara("bz");

        YhXc yhXc = YhXc.dao.findById(xc_id);
        yhXc.setBz(bz);
        yhXc.update();
    }

    /**
     * 行程取消（删除）
     */
    public void xcQx() {
        String xc_id = getPara("xc_id");

        Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                YhXc.dao.sc(xc_id);
                return true;
            }
        });
    }

    /**
     * 行程伙伴确认
     */
    public void xcHbQr() {
        String hb_id = getPara("hb_id");

        Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                //别人主动申请的记录
                YhXcHb yhXcHbZd = YhXcHb.dao.findById(hb_id);

                //自己被申请记录
                YhXcHb yhXcHbBd = YhXcHb.dao.getByXcIdLyXcId(yhXcHbZd.getLyXcId(), yhXcHbZd.getXcId());

                //别人主动申请的行程
                YhXc yhXcZd = YhXc.dao.findById(yhXcHbZd.getXcId());

                //自己被申请的行程
                YhXc yhXcBd = YhXc.dao.findById(yhXcHbBd.getXcId());

                if (yhXcZd.getLx().equals("C")) {//若行程为车找人，扣减行程座位数量
                    yhXcZd.setSl(yhXcZd.getSl().subtract(yhXcBd.getSl()));

                    if (yhXcZd.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() < 0) {
                        throw new BtgBizRuntimeException("座位不够，请联系对方确认");
                    }

                    yhXcZd.update();
                }
                if (yhXcBd.getLx().equals("C")) {//若行程为车找人，扣减行程座位数量
                    yhXcBd.setSl(yhXcBd.getSl().subtract(yhXcZd.getSl()));

                    if (yhXcBd.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() < 0) {
                        throw new BtgBizRuntimeException("座位不够，请联系对方确认");
                    }

                    yhXcBd.update();
                }

                //别人主动申请记录确认
                yhXcHbZd.setZt("QR");
                yhXcHbZd.setQrSj(DateUtil.getNow());
                yhXcHbZd.update();

                //自己被申请记录确认
                yhXcHbBd.setZt("QR");
                yhXcHbBd.setQrSj(DateUtil.getNow());
                yhXcHbBd.update();

                /*
                    短信通知-确认
                 */
                //主动申请用户
                Yh xcYhZd = Yh.dao.findById(yhXcZd.getYhId());
                //被申请用户
                Yh xcYhBd = Yh.dao.findById(yhXcBd.getYhId());
                if (xcYhBd.getDxIsKqSq().equals("YES")) {
                    if (xcYhBd.getDxTs().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() > 0) {
                        SmsService.me.xcTzQr(xcYhBd.getSjh(), xcYhZd.getBh().toString() + (EmptyUtil.isNotEmpty(xcYhZd.getXm()) ? xcYhZd.getXm() : ""), DateUtil.format(yhXcBd.getCfSj(), "MM月dd日HH:mm"), SmsUtil.checkSmsText(yhXcBd.getMc().replaceAll("-", "到")));
                        xcYhBd.setDxTs(xcYhBd.getDxTs().subtract(new BigDecimal(1)));
                        xcYhBd.update();

                        if (xcYhBd.getDxTs().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() <= 0) {
                            Yh.dao.closeAllDxTs(xcYhBd);
                        }
                    }
                }

                setData(Kv.by("sl", yhXcZd.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue()));

                return true;
            }
        });
    }

    /**
     * 行程伙伴拒绝
     */
    public void xcHbJj() {
        String hb_id = getPara("hb_id");

        Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                //别人主动申请的记录
                YhXcHb yhXcHbZd = YhXcHb.dao.findById(hb_id);

                //自己被申请记录
                YhXcHb yhXcHbBd = YhXcHb.dao.getByXcIdLyXcId(yhXcHbZd.getLyXcId(), yhXcHbZd.getXcId());

                //别人主动申请的行程
                YhXc yhXcZd = YhXc.dao.findById(yhXcHbZd.getXcId());

                //自己被申请的行程
                YhXc yhXcBd = YhXc.dao.findById(yhXcHbBd.getXcId());

                if (yhXcZd.getLx().equals("C") && yhXcHbBd.getZt().equals("QR")) {//若行程为车找人，伙伴状态为缘起，增加行程座位数量
                    yhXcZd.setSl(yhXcZd.getSl().add(yhXcBd.getSl()));
                    yhXcZd.update();
                }
                if (yhXcBd.getLx().equals("C") && yhXcHbZd.getZt().equals("QR")) {//若行程为车找人，伙伴状态为缘起，增加行程座位数量
                    yhXcBd.setSl(yhXcBd.getSl().add(yhXcZd.getSl()));
                    yhXcBd.update();
                }

                //别人主动申请记录拒绝
                yhXcHbZd.setZt("JJ");
                yhXcHbZd.setJjSj(DateUtil.getNow());
                yhXcHbZd.update();

                //自己被申请记录拒绝
                yhXcHbBd.setZt("JJ");
                yhXcHbBd.setJjSj(DateUtil.getNow());
                yhXcHbBd.update();

                /*
                    短信通知-确认
                 */
                //主动申请用户
                Yh xcYhZd = Yh.dao.findById(yhXcZd.getYhId());
                //被申请用户
                Yh xcYhBd = Yh.dao.findById(yhXcBd.getYhId());
                if (xcYhBd.getDxIsKqSq().equals("YES")) {
                    if (xcYhBd.getDxTs().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() > 0) {
                        SmsService.me.xcTzJj(xcYhBd.getSjh(), xcYhZd.getBh().toString() + (EmptyUtil.isNotEmpty(xcYhZd.getXm()) ? xcYhZd.getXm() : ""), DateUtil.format(yhXcBd.getCfSj(), "MM月dd日HH:mm"), SmsUtil.checkSmsText(yhXcBd.getMc().replaceAll("-", "到")));
                        xcYhBd.setDxTs(xcYhBd.getDxTs().subtract(new BigDecimal(1)));
                        xcYhBd.update();

                        if (xcYhBd.getDxTs().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() <= 0) {
                            Yh.dao.closeAllDxTs(xcYhBd);
                        }
                    }
                }

                setData(Kv.by("sl", yhXcZd.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue()));

                return true;
            }
        });
    }

    /**
     * 行程伙伴删除（逻辑删除）
     */
    public void xcHbSc() {
        String hb_id = getPara("hb_id");

        Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                //别人主动申请的记录
                YhXcHb yhXcHbZd = YhXcHb.dao.findById(hb_id);

                //自己被申请记录
                YhXcHb yhXcHbBd = YhXcHb.dao.getByXcIdLyXcId(yhXcHbZd.getLyXcId(), yhXcHbZd.getXcId());

                //别人主动申请的行程
                YhXc yhXcZd = YhXc.dao.findById(yhXcHbZd.getXcId());

                //自己被申请的行程
                YhXc yhXcBd = YhXc.dao.findById(yhXcHbBd.getXcId());

                if (yhXcZd.getLx().equals("C") && yhXcHbBd.getZt().equals("QR")) {//若行程为车找人，伙伴状态为缘起，增加行程座位数量
                    yhXcZd.setSl(yhXcZd.getSl().add(yhXcBd.getSl()));
                    yhXcZd.update();
                }
                if (yhXcBd.getLx().equals("C") && yhXcHbZd.getZt().equals("QR")) {//若行程为车找人，伙伴状态为缘起，增加行程座位数量
                    yhXcBd.setSl(yhXcBd.getSl().add(yhXcZd.getSl()));
                    yhXcBd.update();
                }

                /*
                    短信通知-确认
                 */
                if (yhXcHbZd.getZt().equals("QR")) {
                    //主动申请用户
                    Yh xcYhZd = Yh.dao.findById(yhXcZd.getYhId());
                    //被申请用户
                    Yh xcYhBd = Yh.dao.findById(yhXcBd.getYhId());
                    if (xcYhBd.getDxIsKqSq().equals("YES")) {
                        if (xcYhBd.getDxTs().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() > 0) {
                            SmsService.me.xcTzJj(xcYhBd.getSjh(), xcYhZd.getBh().toString() + (EmptyUtil.isNotEmpty(xcYhZd.getXm()) ? xcYhZd.getXm() : ""), DateUtil.format(yhXcBd.getCfSj(), "MM月dd日HH:mm"), SmsUtil.checkSmsText(yhXcBd.getMc().replaceAll("-", "到")));
                            xcYhBd.setDxTs(xcYhBd.getDxTs().subtract(new BigDecimal(1)));
                            xcYhBd.update();

                            if (xcYhBd.getDxTs().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() <= 0) {
                                Yh.dao.closeAllDxTs(xcYhBd);
                            }
                        }
                    }
                }

                //别人主动申请记录拒绝
                yhXcHbZd.setZt("SC");
                yhXcHbZd.setScSj(DateUtil.getNow());
                yhXcHbZd.update();

                //自己被申请记录拒绝
                yhXcHbBd.setZt("SC");
                yhXcHbBd.setScSj(DateUtil.getNow());
                yhXcHbBd.update();

                return true;
            }
        });
    }

    /**
     * 帮助
     */
    public void bz() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        setData(Kv.by("is_ty_xx", thisUser.getIsTyXy()));

        render("/page/bz.html");
    }

    /**
     * 帮助协议（同意、拒绝使用协议）
     */
    public void bzXx() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String is_ty_xx = getPara("is_ty_xx");

        thisUser.setIsTyXy(is_ty_xx);
        thisUser.update();
    }

    /**
     * 我的
     */
    public void my() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        //组装车辆型号
        String cl_xh = "";
        if (EmptyUtil.isNotEmpty(thisUser.getClPpxh())) {
            cl_xh += thisUser.getClPpxh();
        }
        if (EmptyUtil.isNotEmpty(thisUser.getClYs())) {
            if (EmptyUtil.isNotEmpty(cl_xh)) {
                cl_xh += "-";
            }
            cl_xh += thisUser.getClYs();
        }
        if (EmptyUtil.isNotEmpty(thisUser.getClLx())) {
            if (EmptyUtil.isNotEmpty(cl_xh)) {
                cl_xh += "-";
            }
            String cl_lx = thisUser.getClLx();
            if (cl_lx.equals("JC")) {
                cl_lx = "轿车";
            }
            cl_xh += cl_lx;
        }
        thisUser.put("cl_xh", cl_xh);

        //是否签到
        YhHyz yhHyz = YhHyz.dao.getByYhIdRqLx(thisUser.getKeyId(), DateUtil.formatDate(DateUtil.getNow()), "QD");
        thisUser.put("is_qd", EmptyUtil.isNotEmpty(yhHyz) ? "YES" : "NO");

        setData(thisUser);

        initParaTop();

        initParaCheck(true, true, true, false);

        render("/page/my.html");
    }

    /**
     * 编辑基本信息
     */
    public void myJbxx() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        setData(thisUser);

        render("/page/my_jbxx.html");
    }

    /**
     * 基本信息保存
     */
    public void myJbxxSave() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String xm = getPara("xm");
        String zh_qq = getPara("zh_qq");
        String zh_wx = getPara("zh_wx");
        String zh_zfb = getPara("zh_zfb");

        thisUser.setXm(xm);
        thisUser.setZhQq(zh_qq);
        thisUser.setZhWx(zh_wx);
        thisUser.setZhZfb(zh_zfb);
        thisUser.update();
    }

    /**
     * 基本信息声音提醒（是否开启）
     */
    public void myJbxxSyIsKqXc() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String sy_is_kq_xc = getPara("sy_is_kq_xc");

        thisUser.setSyIsKqXc(sy_is_kq_xc);
        thisUser.update();
    }

    /**
     * 基本信息短信推送（是否开启申请）
     */
    public void myJbxxDxIsKqSq() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String dx_is_kq_sq = getPara("dx_is_kq_sq");

        if (thisUser.getDxTs().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() <= 0) {
            setBizDescError("开启失败，短信条数不足，请联系管理员充值");
            Yh.dao.closeAllDxTs(thisUser);
            return;
        }

        thisUser.setDxIsKqSq(dx_is_kq_sq);
        thisUser.update();
    }

    /**
     * 基本信息短信推送（是否开启确认）
     */
    public void myJbxxDxIsKqQr() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String dx_is_kq_qr = getPara("dx_is_kq_qr");

        if (thisUser.getDxTs().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() <= 0) {
            setBizDescError("开启失败，短信条数不足，请联系管理员充值");
            Yh.dao.closeAllDxTs(thisUser);
            return;
        }

        thisUser.setDxIsKqQr(dx_is_kq_qr);
        thisUser.update();
    }

    /**
     * 基本信息短信推送（是否开启拒绝）
     */
    public void myJbxxDxIsKqJj() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String dx_is_kq_jj = getPara("dx_is_kq_jj");

        if (thisUser.getDxTs().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() <= 0) {
            setBizDescError("开启失败，短信条数不足，请联系管理员充值");
            Yh.dao.closeAllDxTs(thisUser);
            return;
        }

        thisUser.setDxIsKqJj(dx_is_kq_jj);
        thisUser.update();
    }

    /**
     * 基本信息手机号（是否公开）
     */
    public void myJbxxSjh() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String sjh_is_gk = getPara("sjh_is_gk");

        thisUser.setSjhIsGk(sjh_is_gk);
        thisUser.update();
    }

    /**
     * 编辑车辆信息
     */
    public void myClxx() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        if ("JC".equals(thisUser.getClLx())) {
            thisUser.setClLx("轿车");
        }
        setData(thisUser);

        render("/page/my_clxx.html");
    }

    /**
     * 车辆信息保存
     */
    public void myClxxSave() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String cl_hm = getPara("cl_hm");
        String cl_ppxh = getPara("cl_ppxh");
        String cl_ys = getPara("cl_ys");
        String cl_lx = getPara("cl_lx");

        thisUser.setClHm(cl_hm);
        thisUser.setClPpxh(cl_ppxh);
        thisUser.setClYs(cl_ys);
        thisUser.setClLx(cl_lx);
        thisUser.update();
    }

    /**
     * 车辆图片上传
     */
    public void myClxxImg() {
        UploadFile uploadFile = getFile();
        File file = uploadFile.getFile();

        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String fileUrl = "c/" + thisUser.getKeyId() + "." + FileUtil.getSuffix(file);
        File newFile = new File(BtgSetConfig.sysDefaultUploadPath() + fileUrl);
        FileUtil.copyTo(file, newFile);

        String clImgUrl = "upload/" + fileUrl;
        thisUser.setClImg(clImgUrl);
        thisUser.update();

        setData(Kv.by("url", clImgUrl));
    }

    /**
     * 编辑预设行程
     */
    public void myLx() {
        String lx_id = getPara("lx_id");
        if (EmptyUtil.isNotEmpty(lx_id)) {
            YhLx yhLx = YhLx.dao.findById(lx_id);
            yhLx.put("cf_sj_h", Integer.parseInt(DateUtil.format(yhLx.getCfSj(), DateUtil.DEFAULT_PATTERN_HOUR)));
            yhLx.put("cf_sj_m", Integer.parseInt(DateUtil.format(yhLx.getCfSj(), DateUtil.DEFAULT_PATTERN_MINUTE)));
            setData(yhLx);
        }
        render("/page/my_lx.html");
    }

    /**
     * 预设行程保存
     */
    public void myLxSave() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String lx_id = getPara("lx_id");
        String mc = getPara("mc");
        int HHBegin = getInt("HHBegin");
        int mmBegin = getInt("mmBegin");
        String sl = getPara("sl");
        String je = getPara("je");
        String dd_str = getPara("dd_str");
        String bz = getPara("bz");

        String cfSj = "2018-05-12 " + (HHBegin < 10 ? "0" + HHBegin : HHBegin) + ":" + mmBegin + ":00";

        boolean isAdd = EmptyUtil.isEmpty(lx_id);
        YhLx yhLx;
        if (isAdd) {
            YhLx yhLxExists = YhLx.dao.getByLxYhIdMcCfSjSlJeDdStrBzKeyIdNot(thisUser.getLx(), thisUser.getKeyId(), mc, cfSj, sl, je, dd_str, bz, null);
            if (EmptyUtil.isNotEmpty(yhLxExists)) {
                setBizDescError("路线已存在");
                return;
            }

            yhLx = new YhLx();
            yhLx.setKeyId(Identities.uuid());
            yhLx.setLx(thisUser.getLx());
            yhLx.setYhId(thisUser.getKeyId());
        } else {
            YhLx yhLxExists = YhLx.dao.getByLxYhIdMcCfSjSlJeDdStrBzKeyIdNot(thisUser.getLx(), thisUser.getKeyId(), mc, cfSj, sl, je, dd_str, bz, lx_id);
            if (EmptyUtil.isNotEmpty(yhLxExists)) {
                setBizDescError("路线已存在");
                return;
            }

            yhLx = YhLx.dao.findById(lx_id);
        }
        yhLx.setMc(mc);
        yhLx.setCfSj(DateUtil.parseDateTime(cfSj));
        yhLx.setSl(new BigDecimal(sl));
        yhLx.setJe(new BigDecimal(je));
        yhLx.setDdStr(dd_str);
        yhLx.setBz(bz);

        if (isAdd) {
            yhLx.save();
        } else {
            yhLx.update();
        }
    }

    /**
     * 预设行程删除
     */
    public void myLxDel() {
        String lx_id = getPara("lx_id");
        YhLx.dao.deleteById(lx_id);
    }

    /**
     * 更改手机号
     */
    public void mySjh() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        setData(Kv.by("sjh", thisUser.getSjh()));

        render("/page/my_sjh.html");
    }

    /**
     * 更改手机号保存
     */
    public void mySjhSave() {
        Yh thisUser = getSessionUser();

        thisUser = Yh.dao.findById(thisUser.getKeyId());

        String sjh = getPara("sjh");
        String yzh = getPara("yzh");

        if (!SmsContext.getContext().validCode(sjh, yzh)) {
            setBizDescError("验证码错误");
            return;
        }

        if (thisUser.getSjh().equals(sjh)) {
            setBizDescError("新手机号不能为原始手机号");
            return;
        }

        Yh yhExists = Yh.dao.getBySjh(sjh);
        if (EmptyUtil.isNotEmpty(yhExists)) {
            setBizDescError("新手机号已被注册");
            return;
        }

        SmsContext.getContext().dieCode(sjh);

        thisUser.setSjh(sjh);
        thisUser.update();
    }

    /**
     * 签到
     */
    public void myQd() {
        Yh thisUser = getSessionUser();
        Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                YhHyz yhHyz = YhHyz.dao.getByYhIdRqLx(thisUser.getKeyId(), DateUtil.formatDate(DateUtil.getNow()), "QD");
                if (EmptyUtil.isNotEmpty(yhHyz)) {
                    throw new BtgBizRuntimeException("今日已签到，记得明日再来噢");
                }
                BigDecimal hyz = YhHyz.dao.addHyz(thisUser.getKeyId(), DateUtil.getNow(), "QD", new BigDecimal(BtgSetConfig.hyzSlQd()));
                setData(Kv.by("hyz", hyz.setScale(0, BigDecimal.ROUND_HALF_UP).toString()));
                return true;
            }
        });
    }

    /**
     * 活跃值排行
     */
    public void myHyzPh() {
        List<JSONObject> rowsJr = YhHyz.dao.listTopByRqOrderHyz(20, DateUtil.formatDate(DateUtil.getNow()));
        List<JSONObject> rows = Yh.dao.listTopOrderByHyz(100);

        setData(Kv.by("list_jr", rowsJr).set("list", rows));

        render("/page/my_hyz_ph.html");
    }

    /**
     * 活跃值明细
     */
    public void myHyzMx() {
        setData(Kv.by("pageIndex", getPageIndex()).set("pageSize", getPageSize()));

        render("/page/my_hyz_mx.html");
    }

    /**
     * 活跃值明细列表
     */
    public void myHyzMxList() {
        Yh thisUser = getSessionUser();

        Kv kv = Kv.create();
        kv.set("yh_id", thisUser.getKeyId());

        Page<YhHyz> page = YhHyz.dao.pageByKv(getPageIndex(), getPageSize(), kv);
        for (YhHyz yhHyz : page.getList()) {
            yhHyz.put("rq", DateUtil.formatDate(yhHyz.getRq()));

            String lx = yhHyz.getLx();
            String lx_mc = null;
            switch (lx) {
                case "QD":
                    lx_mc = "签到";
                    break;
                case "FB":
                    lx_mc = "发布行程";
                    break;
                case "LX":
                    lx_mc = "火速联系";
                    break;
            }
            yhHyz.put("lx_mc", lx_mc);
        }

        setData(Kv.by("list", page.getList()).set("total", page.getTotalRow()));
    }

    /**
     * 系统出错
     */
    public void error() {
        render("/page/error/error.html");
    }

    /**
     * 注销
     */
    public void zx() {
        SessionKit.removeSession(getSession().getId());
        redirect("/dl");
    }

    /**
     * 未登录或登录过期
     */
    public void noSession() {
        render("/page/error/no_session.html");
    }

    /**
     * 没有协议（未同意协议）
     */
    public void noXx() {
        render("/page/error/no_xx.html");
    }

    /**
     * 捐助-支付宝
     */
    public void jzZfb() {
        renderQrCode(BtgSetConfig.jzZfbUrl(), 300, 300);
    }

    /**
     * 捐助-微信
     */
    public void jzWx() {
        renderQrCode(BtgSetConfig.jzWxUrl(), 300, 300);
    }
}
