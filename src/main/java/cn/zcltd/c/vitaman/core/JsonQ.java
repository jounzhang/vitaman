package cn.zcltd.c.vitaman.core;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * 快速构建Json对象
 * ps:取名为JsonQ的意义在于，Q位于键盘左上角
 * ，由左手小指负责，小指平时使用较少
 * ，这样可以多锻炼，利于开发右脑
 */
public class JsonQ extends JSONObject {

    public JsonQ() {

    }

    public static JsonQ by(String key, Object value) {
        return new JsonQ().set(key, value);
    }

    public static JsonQ create() {
        return new JsonQ();
    }

    public JsonQ set(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public JsonQ set(Map<String, Object> map) {
        super.putAll(map);
        return this;
    }

    public JsonQ delete(String key) {
        super.remove(key);
        return this;
    }

    public <T> T getAs(String key) {
        return (T) get(key);
    }
}