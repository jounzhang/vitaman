package cn.zcltd.c.vitaman.core.controller;

import cn.zcltd.btg.sutil.EmptyUtil;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.core.NotAction;
import com.jfinal.kit.Kv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 控制器基类：提供通用controller扩展
 */
public class BtgController extends Controller {
    private static final Logger log = LoggerFactory.getLogger(BtgController.class);

    /**
     * 获取请求基础url
     *
     * @return url
     */
    public String getRootRequestUrl() {
        HttpServletRequest request = getRequest();
        String basePath = request.getScheme() + "://" + request.getServerName() + (request.getServerPort() == 80 ? "" : ":" + request.getServerPort());
        String contextPath = request.getContextPath();
        return basePath + contextPath;
    }

    /**
     * 获取url参数
     *
     * @return url参数kv
     */
    @NotAction
    public Kv getPathParamsKv() {
        Kv urlParamKv = Kv.create();
        Enumeration<String> enumeration = getAttrNames();
        while (enumeration.hasMoreElements()) {
            String paramName = enumeration.nextElement();
            if (paramName.startsWith("path.")) {
                Object paramValue = getAttr(paramName);
                urlParamKv.set(paramName.substring(5), paramValue);
            }
        }

        return urlParamKv;
    }

    /**
     * 获取header参数
     *
     * @return header参数kv
     */
    @NotAction
    public Kv getHeaderParamsKv() {
        Set<String> skipHeaders = new HashSet<String>() {{
            add("content-type");
            add("accept-encoding");
            add("user-agent");
            add("host");
            add("connection");
        }};

        Kv headerParamKv = Kv.create();
        Enumeration<String> enumeration = getRequest().getHeaderNames();
        while (enumeration.hasMoreElements()) {
            String paramName = enumeration.nextElement();
            if (!skipHeaders.contains(paramName)) {
                Object paramValue = getHeader(paramName);
                headerParamKv.set(paramName, paramValue);
            }
        }

        return headerParamKv;
    }

    /**
     * 获取param参数
     *
     * @return pram参数kv
     */
    @NotAction
    public Kv getParamsKv() {
        Kv kv = Kv.create();
        for (Map.Entry<String, String[]> param : getParaMap().entrySet()) {
            String key = param.getKey();
            String[] values = param.getValue();
            if (EmptyUtil.isNotEmpty(values)) {
                StringBuilder valueSb = new StringBuilder();
                for (String value : values) {
                    valueSb.append(",").append(value);
                }
                kv.set(key, valueSb.substring(1));
            }
        }
        return kv;
    }

    /**
     * 获取session参数
     *
     * @return session参数kv
     */
    @NotAction
    public Kv getSessionParamsKv() {
        Kv kv = Kv.create();
        Enumeration<String> paramNames = getSession().getAttributeNames();
        while (paramNames.hasMoreElements()) {
            String paramName = paramNames.nextElement();
            Object paramValue = getSessionAttr(paramName);
            if (EmptyUtil.isNotEmpty(paramValue)) {
                kv.set(paramName, paramValue);
            }
        }
        return kv;
    }

    /**
     * 获取application参数
     *
     * @return application参数kv
     */
    @NotAction
    public Kv getApplicationParamsKv() {
        Kv kv = Kv.create();
        Enumeration<String> paramNames = getRequest().getServletContext().getAttributeNames();
        while (paramNames.hasMoreElements()) {
            String paramName = paramNames.nextElement();
            Object paramValue = getSessionAttr(paramName);
            if (EmptyUtil.isNotEmpty(paramValue)) {
                kv.set(paramName, paramValue);
            }
        }
        return kv;
    }

    /**
     * 获取attr参数
     *
     * @return attr参数kv
     */
    @NotAction
    public Kv getAttrParamsKv() {
        Kv kv = Kv.create();
        Enumeration<String> paramNames = getAttrNames();
        while (paramNames.hasMoreElements()) {
            String paramName = paramNames.nextElement();
            Object paramValue = getAttr(paramName);
            if (EmptyUtil.isNotEmpty(paramValue)) {
                kv.set(paramName, paramValue);
            }
        }
        return kv;
    }

    /**
     * 获取所有参数
     *
     * @return 所有参数kv
     */
    @NotAction
    public Kv getAllParamsKv() {
        Kv kv = Kv.create();
        kv.set(getPathParamsKv());
        kv.set(getHeaderParamsKv());
        kv.set(getParamsKv());
        kv.set(getSessionParamsKv());
        kv.set(getApplicationParamsKv());
        kv.set(getAttrParamsKv());
        return kv;
    }

    /**
     * 获取客户端ip
     * 不使用request.getRemoteAddr()的原因是有可能用户使用了代理软件方式避免真实IP地址，
     * 如果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，
     * 而是一串IP值，究竟哪个才是真正的用户端的真实IP呢？
     * 答案是取X-Forwarded-For中第一个非unknown的有效IP字符串。
     * 如：
     * X-Forwarded-For：192.168.1.110, 192.168.1.120, 192.168.1.130,192.168.1.100
     * 用户真实IP为： 192.168.1.110
     *
     * @return ip地址
     */
    @NotAction
    public String getIp() {
        HttpServletRequest request = getRequest();
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
            if (ip.equals("127.0.0.1")) {
                //根据网卡取本机配置的IP
                InetAddress inet = null;
                try {
                    inet = InetAddress.getLocalHost();
                    ip = inet.getHostAddress();
                } catch (UnknownHostException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }

        //对于通过多个代理的情况（多个IP按照,分割），第一个IP为客户端真实IP
        if (ip != null && ip.length() > 15) { //"***.***.***.***".length() = 15
            if (ip.indexOf(",") > 0) {
                ip = ip.substring(0, ip.indexOf(","));
            }
        }

        return ip;
    }

    /**
     * 获取日志参数信息
     *
     * @return 获取日志参数信息
     */
    @NotAction
    public String getLogParams() {
        JSONObject paramJson = new JSONObject();

        Kv paramsKv = getAllParamsKv();
        for (Object paramName : paramsKv.keySet()) {
            Object paramValue = paramsKv.get(paramName);
            paramJson.put("path." + paramName.toString(), paramValue);
        }

        return paramJson.toJSONString();
    }
}