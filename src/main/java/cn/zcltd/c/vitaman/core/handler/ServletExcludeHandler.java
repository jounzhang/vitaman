package cn.zcltd.c.vitaman.core.handler;

import com.jfinal.handler.Handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.Set;

/**
 * Handler：控制jfinal不接管的url
 */
public class ServletExcludeHandler extends Handler {

    private static final Set<String> skipUrlStartWith = new HashSet<String>() {
        {
            add("/ReportServer");
            add("/druid");
        }
    };

    public static boolean isSkip(String url) {
        boolean isSkip = false;
        for (String key : skipUrlStartWith) {
            if (url.startsWith(key)) {
                isSkip = true;
                break;
            }
        }
        return isSkip;
    }

    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        if (!isSkip(target)) {
            next.handle(target, request, response, isHandled);
        }
    }
}