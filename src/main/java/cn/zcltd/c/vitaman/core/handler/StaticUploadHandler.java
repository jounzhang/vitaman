package cn.zcltd.c.vitaman.core.handler;

import cn.zcltd.btg.sutil.FileUtil;
import cn.zcltd.c.vitaman.BtgSetConfig;
import com.jfinal.handler.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;

/**
 * upload开头的静态资源自动从配置的upload目录中读取
 */
public class StaticUploadHandler extends Handler {
    private static final Logger log = LoggerFactory.getLogger(StaticUploadHandler.class);

    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        if (target.startsWith("/upload")) {
            String path = BtgSetConfig.sysDefaultUploadPath() + target.substring(target.indexOf("/upload") + 8);
            File file = new File(path);
            if (file.exists()) {
                try {
                    response.setContentType("multipart/form-data");
                    FileUtil.write(new FileInputStream(file), response.getOutputStream());
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
            isHandled[0] = true;
            return;
        }

        next.handle(target, request, response, isHandled);
    }
}