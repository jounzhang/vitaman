package cn.zcltd.c.vitaman.core.intercepter;

import cn.zcltd.btg.core.exception.BtgBizRuntimeException;
import cn.zcltd.btg.core.exception.BtgRespRuntimeException;
import cn.zcltd.btg.core.exception.BtgRuntimeException;
import cn.zcltd.c.vitaman.controller.BaseController;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 拦截后端处理异常，返回友好提示信息
 */
public class ExceptionInterceptor implements Interceptor {
    private static final Logger log = LoggerFactory.getLogger(ExceptionInterceptor.class);

    @Override
    public void intercept(Invocation inv) {
        Controller controller = inv.getController();

        try {
            inv.invoke();
        } catch (Exception e) {
            boolean isAjax = "XMLHttpRequest".equals(controller.getHeader("X-Requested-With"));

            if (controller instanceof BaseController) {
                BaseController baseController = (BaseController) controller;

                if (isAjax) {
                    if (e instanceof BtgRespRuntimeException) {
                        BtgRuntimeException btgRuntimeException = (BtgRuntimeException) e;
                        baseController.setRespDescError(btgRuntimeException.getDesc());
                        log.warn(btgRuntimeException.getDesc());
                    } else if (e instanceof BtgBizRuntimeException) {
                        BtgBizRuntimeException btgRuntimeException = (BtgBizRuntimeException) e;
                        baseController.setBizDescError(btgRuntimeException.getDesc());
                        log.info(btgRuntimeException.getDesc());
                    } else {
                        baseController.setRespDescError();
                        log.error(e.getMessage(), e);
                    }
                    baseController.renderResult();
                } else {
                    log.error(e.getMessage(), e);
                    controller.redirect("/error");
                }

                return;
            }

            log.error(e.getMessage(), e);
        }
    }
}