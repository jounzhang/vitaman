package cn.zcltd.c.vitaman.core.intercepter;

import cn.zcltd.btg.sutil.EmptyUtil;
import cn.zcltd.c.vitaman.BtgSetConfig;
import cn.zcltd.c.vitaman.controller.BaseController;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.render.Render;
import com.jfinal.render.TemplateRender;

import javax.servlet.http.HttpServletRequest;

/**
 * Interceptor：全局处理
 */
public class GlobalInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation inv) {
        Controller controller = inv.getController();

        HttpServletRequest request = controller.getRequest();

        Render render = controller.getRender();

        //模板渲染时，设置全局参数
        if (render instanceof TemplateRender) {
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
            controller.setAttr("_basePath", basePath);

            controller.setAttr("_name", BtgSetConfig.name());
            controller.setAttr("_keywords", BtgSetConfig.h5KeyWords());
            controller.setAttr("_desc", BtgSetConfig.h5Desc());
        }

        //为显示render时添加默认render
        if (controller instanceof BaseController) {
            BaseController baseController = (BaseController) controller;
            if (EmptyUtil.isEmpty(render)) {
                baseController.renderResult();
            }
        } else {
            if (EmptyUtil.isEmpty(render)) {
                controller.renderJson();
            }
        }

        inv.invoke();
    }
}