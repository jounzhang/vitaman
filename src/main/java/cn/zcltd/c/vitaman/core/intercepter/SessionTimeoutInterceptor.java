package cn.zcltd.c.vitaman.core.intercepter;

import cn.zcltd.btg.jfinal.autoroute.AutoRoutesAnnotation;
import cn.zcltd.btg.sutil.EmptyUtil;
import cn.zcltd.c.vitaman.Constant;
import cn.zcltd.c.vitaman.controller.BaseController;
import cn.zcltd.c.vitaman.dao.Yh;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Set;

/**
 * Interceptor：session过期拦截
 */
public class SessionTimeoutInterceptor implements Interceptor {

    //需要排除的url
    private static final Set<String> skipUrl = new HashSet<String>() {
        {
            add("/index");

            add("/yzm");

            add("/zc");
            add("/zcSave");
            add("/dl");
            add("/dlSave");
            add("/mmZh");
            add("/mmZhSave");

            add("/tj");
            add("/myHyzPh");

            add("/error");
            add("/noSession");
            add("/noXx");
        }
    };

    //需要排除的url开头规则
    private static final Set<String> skipUrlStartWith = new HashSet<String>() {
        {
            add("/ws");
        }
    };

    private boolean isSkip(String url) {
        for (String key : skipUrlStartWith) {
            if (url.startsWith(key)) {
                return true;
            }
        }

        return skipUrl.contains(url);
    }

    @Override
    public void intercept(Invocation inv) {
        Controller controller = inv.getController();
        HttpServletRequest request = controller.getRequest();

        boolean isAjax = "XMLHttpRequest".equals(controller.getHeader("X-Requested-With"));
        String basePath = request.getScheme() + "://" + request.getServerName() + (request.getServerPort() == 80 ? "" : ":" + request.getServerPort());
        String queryStr = request.getQueryString();
        String requestUrl = basePath + request.getRequestURI() + (EmptyUtil.isNotEmpty(queryStr) ? "?" + queryStr : "");

        if (controller instanceof BaseController) {
            BaseController baseController = (BaseController) controller;

            String controllerKey = inv.getControllerKey();
            controllerKey = controllerKey.substring(controllerKey.length() - 1).equals("/") ? controllerKey : controllerKey + "/";
            String url = controllerKey + inv.getMethodName();

            AutoRoutesAnnotation autoRoutesAnnotation = AutoRoutesAnnotation.getInstance();
            if (isSkip(url) || autoRoutesAnnotation.getTags(url).contains(Constant.TAG_UNCHECK_SESSION_TIMEOUT)) {
                inv.invoke();
                return;
            }

            Yh yh = baseController.getSessionUser();
            if (EmptyUtil.isNotEmpty(yh)) {
                yh = Yh.dao.findById(yh.getKeyId());

                //如果未同意协议，跳转到同意协议
                if (!url.equals("/bz") && !url.equals("/bzXx") && yh.getIsTyXy().equals("NO")) {
                    if (isAjax) {
                        baseController.setRespInfo(Constant.RESP_RESPCODE_WTY_XX, Constant.RESP_RESPDESC_WTY_XX);
                        baseController.setAttr("_url", requestUrl);
                        baseController.renderResult();
                    } else {
                        baseController.redirect("/noXx?_url=" + requestUrl);
                    }
                    return;
                }

                //用户类型
                baseController.setAttr("_yh_lx", yh.getLx());

                inv.invoke();
                return;
            }

            baseController.getResponse().setCharacterEncoding("UTF-8");
            if (isAjax) {
                baseController.setRespInfo(Constant.RESP_RESPCODE_SESSION_TIMEOUT, Constant.RESP_RESPDESC_SESSION_TIMEOUT);
                baseController.setAttr("_url", requestUrl);
                baseController.renderResult();
            } else {
                baseController.redirect("/noSession?_url=" + requestUrl);
            }
            return;
        }

        inv.invoke();
    }
}