package cn.zcltd.c.vitaman.core.model;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("rawtypes")
public class BaseModel<M extends BaseModel<M>> extends Model<M> implements IBean {
    private static final long serialVersionUID = 1L;

    public List<M> findSM(String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        return find(sqlPara);
    }

    public List<M> findSM(String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        return find(sqlPara);
    }

    public M findFirstSM(String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        return findFirst(sqlPara);
    }

    public M findFirstSM(String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        return findFirst(sqlPara);
    }

    public Page<M> paginateSM(int pageNumber, int pageSize, String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        return paginate(pageNumber, pageSize, sqlPara);
    }

    public Page<M> paginateSM(int pageNumber, int pageSize, String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        return paginate(pageNumber, pageSize, sqlPara);
    }

    public List<Record> findSM4Record(String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        return Db.find(sqlPara);
    }

    public List<Record> findSM4Record(String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        return Db.find(sqlPara);
    }

    public Record findFirstSM4Record(String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        return Db.findFirst(sqlPara);
    }

    public Record findFirstSM4Record(String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        return Db.findFirst(sqlPara);
    }

    public Page<Record> paginateSM4Record(int pageNumber, int pageSize, String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        return Db.paginate(pageNumber, pageSize, sqlPara);
    }

    public Page<Record> paginateSM4Record(int pageNumber, int pageSize, String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        return Db.paginate(pageNumber, pageSize, sqlPara);
    }

    public List<JSONObject> findJsonSM(SqlPara sqlPara) {
        List<JSONObject> list = new ArrayList<>();
        List<Record> ms = Db.find(sqlPara);
        for (Record m : ms) {
            list.add(JSONObject.parseObject(m.toJson()));
        }
        return list;
    }

    public List<JSONObject> findJsonSM(String sqlId, Kv kv) {
        List<JSONObject> list = new ArrayList<>();
        List<Record> ms = findSM4Record(sqlId, kv);
        for (Record m : ms) {
            list.add(JSONObject.parseObject(m.toJson()));
        }
        return list;
    }

    public List<JSONObject> findJsonSM(String sqlId) {
        List<JSONObject> list = new ArrayList<>();
        List<Record> ms = findSM4Record(sqlId);
        for (Record m : ms) {
            list.add(JSONObject.parseObject(m.toJson()));
        }
        return list;
    }

    public JSONObject findFirstJsonSM(SqlPara sqlPara) {
        Record first = Db.findFirst(sqlPara);
        return first == null ? null : JSONObject.parseObject(first.toJson());
    }

    public JSONObject findFirstJsonSM(String sqlId, Kv kv) {
        Record first = findFirstSM4Record(sqlId, kv);
        return first == null ? null : JSONObject.parseObject(first.toJson());
    }

    public JSONObject findFirstJsonSM(String sqlId) {
        Record first = findFirstSM4Record(sqlId);
        return first == null ? null : JSONObject.parseObject(first.toJson());
    }

    public JSONArray findJsonArraySM(SqlPara sqlPara) {
        JSONArray jsonArray = new JSONArray();
        List<Record> ms = Db.find(sqlPara);
        for (Record m : ms) {
            jsonArray.add(JSONObject.parseObject(m.toJson()));
        }
        return jsonArray;
    }

    public JSONArray findJsonArraySM(String sqlId, Kv kv) {
        JSONArray jsonArray = new JSONArray();
        List<Record> ms = findSM4Record(sqlId, kv);
        for (Record m : ms) {
            jsonArray.add(JSONObject.parseObject(m.toJson()));
        }
        return jsonArray;
    }

    public JSONArray findJsonArraySM(String sqlId) {
        JSONArray jsonArray = new JSONArray();
        List<Record> ms = findSM4Record(sqlId);
        for (Record m : ms) {
            jsonArray.add(JSONObject.parseObject(m.toJson()));
        }
        return jsonArray;
    }

    public JSONArray findJsonArray(String sql, Object... objects) {
        JSONArray jsonArray = new JSONArray();
        List<Record> ms = Db.find(sql, objects);
        for (Record m : ms) {
            jsonArray.add(JSONObject.parseObject(m.toJson()));
        }
        return jsonArray;
    }

    public JSONObject findJsonObject(String sql, Object... objects) {
        Record re = Db.findFirst(sql, objects);
        return JSONObject.parseObject(re.toJson());
    }

    public JSONObject findJsonObjectSM(SqlPara sqlPara) {
        Record first = Db.findFirst(sqlPara);
        return first == null ? null : JSONObject.parseObject(first.toJson());
    }

    public JSONObject findJsonObjectSM(String sqlId, Kv kv) {
        Record m = findFirstSM4Record(sqlId, kv);
        return JSONObject.parseObject(m.toJson());
    }

    public JSONObject findJsonObjectSM(String sqlId) {
        Record m = findFirstSM4Record(sqlId);
        return JSONObject.parseObject(m.toJson());
    }

    public PageJson paginateJsonSM(int pageNumber, int pageSize, SqlPara sqlPara) {
        Page<Record> page = Db.paginate(pageNumber, pageSize, sqlPara);
        List<JSONObject> jsonObjectList = new ArrayList<>();
        List<Record> mList = page.getList();
        for (Record m : mList) {
            jsonObjectList.add(JSONObject.parseObject(m.toJson()));
        }
        return new PageJson(jsonObjectList, page.getPageNumber(), page.getPageSize(), page.getTotalPage(), page.getTotalRow());
    }

    public PageJson paginateJsonSM(int pageNumber, int pageSize, String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        Page<Record> page = Db.paginate(pageNumber, pageSize, sqlPara);
        List<JSONObject> jsonObjectList = new ArrayList<>();
        List<Record> mList = page.getList();
        for (Record m : mList) {
            jsonObjectList.add(JSONObject.parseObject(m.toJson()));
        }
        return new PageJson(jsonObjectList, page.getPageNumber(), page.getPageSize(), page.getTotalPage(), page.getTotalRow());
    }

    public PageJson paginateJsonSM(int pageNumber, int pageSize, String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        Page<Record> page = Db.paginate(pageNumber, pageSize, sqlPara);
        List<JSONObject> jsonObjectList = new ArrayList<>();
        List<Record> mList = page.getList();
        for (Record m : mList) {
            jsonObjectList.add(JSONObject.parseObject(m.toJson()));
        }
        return new PageJson(jsonObjectList, page.getPageNumber(), page.getPageSize(), page.getTotalPage(), page.getTotalRow());
    }

    public void executeUpdateSm(String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        Db.update(sqlPara);
    }

    public void executeUpdateSm(String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        Db.update(sqlPara);
    }

    public Integer queryIntSM(String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        return Db.queryInt(sqlPara.getSql(), sqlPara.getPara());
    }

    public Integer queryIntSM(String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        return Db.queryInt(sqlPara.getSql());
    }

    public Long queryLongSM(String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        return Db.queryLong(sqlPara.getSql(), sqlPara.getPara());
    }

    public Long queryLongSM(String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        return Db.queryLong(sqlPara.getSql());
    }

    public String queryStringSM(String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        return Db.queryStr(sqlPara.getSql(), sqlPara.getPara());
    }

    public String queryStringSM(String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        return Db.queryStr(sqlPara.getSql());
    }

    public BigDecimal queryBigDecimalSM(String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        return Db.queryBigDecimal(sqlPara.getSql(), sqlPara.getPara());
    }

    public BigDecimal queryBigDecimalSM(String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        return Db.queryBigDecimal(sqlPara.getSql());
    }

    public int updateSM(String sqlId, Kv kv) {
        SqlPara sqlPara = getSqlPara(sqlId, kv);
        return Db.update(sqlPara);
    }

    public int updateSM(String sqlId) {
        SqlPara sqlPara = getSqlPara(sqlId);
        return Db.update(sqlPara);
    }
}