package cn.zcltd.c.vitaman.core.model;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.ArrayList;
import java.util.List;

/**
 * json分页
 */
public class PageJson {
    private static final long serialVersionUID = -1L;

    private List<JSONObject> list;
    private int pageNumber;
    private int pageSize;
    private int totalPage;
    private int totalRow;

    public PageJson(List<JSONObject> list, int pageNumber, int pageSize, int totalPage, int totalRow) {
        this.list = list;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.totalPage = totalPage;
        this.totalRow = totalRow;
    }

    public PageJson() {

    }

    public List<JSONObject> getList() {
        if (list == null) {
            list = new ArrayList<>();
        }
        return list;
    }

    public void setList(List<JSONObject> list) {
        this.list = list;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }

    @Override
    public String toString() {
        return "PageJson{" +
                "list=" + list +
                ", pageNumber=" + pageNumber +
                ", pageSize=" + pageSize +
                ", totalPage=" + totalPage +
                ", totalRow=" + totalRow +
                '}';
    }

    @Deprecated
    public static PageJson toPageJson(Page<Record> paginate) {
        List<JSONObject> list = new ArrayList<>();
        for (Record record : paginate.getList()) {
            list.add(JSON.parseObject(record.toJson()));
        }
        PageJson pageJson = new PageJson(list, paginate.getPageNumber(), paginate.getPageSize(), paginate.getTotalPage(), paginate.getTotalRow());
        return pageJson;
    }
}