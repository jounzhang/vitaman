package cn.zcltd.c.vitaman.dao;

import cn.zcltd.c.vitaman.model.BaseYhXcHb;
import com.jfinal.kit.Kv;

import java.util.List;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class YhXcHb extends BaseYhXcHb<YhXcHb> {
    private static final long serialVersionUID = 1L;
    public static final YhXcHb dao = new YhXcHb().dao();

    public int countByKv(Kv kv) {
        return queryIntSM("yh.xc_hb.countByKv", kv);
    }

    public int countByHbId(String hbId) {
        return countByKv(Kv.by("hb_id", hbId));
    }

    public int countByXcId(String xcId) {
        return countByKv(Kv.by("xc_id", xcId));
    }

    public int countByXcIdZtNotStr(String xcId, String ztNotStr) {
        return countByKv(Kv.by("xc_id", xcId).set("zt_not_str", ztNotStr));
    }

    public int countByLxSqRq(String lx, String sqRq) {
        return countByKv(Kv.by("lx", lx).set("sq_rq", sqRq));
    }

    public YhXcHb getByKv(Kv kv) {
        return findFirstSM("yh.xc_hb.getByKv", kv);
    }

    public YhXcHb getByXcIdLyXcId(String xcId, String lyXcId) {
        return getByKv(Kv.by("xc_id", xcId).set("ly_xc_id", lyXcId));
    }

    public YhXcHb getByXcIdHbId(String xcId, String hbId) {
        return getByKv(Kv.by("xc_id", xcId).set("hb_id", hbId));
    }

    public List<YhXcHb> listByKv(Kv kv) {
        return findSM("yh.xc_hb.list", kv);
    }

    public List<YhXcHb> listByXcId(String xcId) {
        return listByKv(Kv.by("xc_id", xcId));
    }

    public List<YhXcHb> listByXcIdZtNot(String xcId, String ztNot) {
        return listByKv(Kv.by("xc_id", xcId).set("zt_not", ztNot));
    }

    public void deleteByKv(Kv kv) {
        updateSM("yh.xc_hb.deleteByKv", kv);
    }

    public void deleteByXcId(String xcId) {
        deleteByKv(Kv.by("xc_id", xcId));
    }

    public void deleteByLyXcId(String lyXcId) {
        deleteByKv(Kv.by("ly_xc_id", lyXcId));
    }
}