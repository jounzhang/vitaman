package cn.zcltd.c.vitaman.dao;

import cn.zcltd.c.vitaman.model.BaseYhXcHis;
import com.jfinal.kit.Kv;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class YhXcHis extends BaseYhXcHis<YhXcHis> {
    private static final long serialVersionUID = 1L;
    public static final YhXcHis dao = new YhXcHis().dao();

    public void cdByKv(Kv kv) {
        updateSM("yh.xc_his.cdByKv", kv);
    }

    public void cdById(String keyId) {
        cdByKv(Kv.by("key_id", keyId));
    }

    public int countByKv(Kv kv) {
        return queryIntSM("yh.xc_his.countByKv", kv);
    }

    public int countByLxCfRq(String lx, String cfRq) {
        return countByKv(Kv.by("lx", lx).set("cf_rq", cfRq));
    }
}