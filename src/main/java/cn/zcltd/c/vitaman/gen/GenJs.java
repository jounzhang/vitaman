package cn.zcltd.c.vitaman.gen;

import cn.zcltd.btg.sutil.FileUtil;
import cn.zcltd.c.vitaman.AppConfig;
import com.jfinal.core.JFinal;
import com.jfinal.kit.Kv;
import com.jfinal.kit.PathKit;
import com.jfinal.template.Engine;
import com.jfinal.template.Template;
import org.apache.log4j.Logger;

public class GenJs {
    private static final Logger log = Logger.getLogger(AppConfig.class);

    public static void config() {
        String webRootPath = PathKit.getWebRootPath();
        String contextPath = JFinal.me().getContextPath();

        Template template = Engine.use("gen").getTemplate("gen/config.js.e");
        String dicStr = template.renderToString(Kv.by("contextPath", contextPath));

        String filePath = webRootPath + "/js/common/config.js";
        FileUtil.write(filePath, dicStr, "UTF-8");
    }
}
