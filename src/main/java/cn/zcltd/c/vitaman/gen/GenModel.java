package cn.zcltd.c.vitaman.gen;

import cn.zcltd.c.vitaman.JfinalContext;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.generator.*;

import javax.sql.DataSource;

/**
 * 生成器：生成model、dao、mapping
 */
public class GenModel {

    public static void main(String[] args) {
        JfinalContext.init();

        DataSource dataSource = DbKit.getConfig().getDataSource();

        String basePath = System.getProperty("user.dir") + "/src/main/java/cn/zcltd/c/vitaman";

        String baseModelPackageName = "cn.zcltd.c.vitaman.model";
        String baseModelOutputDir = basePath + "/model";

        String modelPackageName = "cn.zcltd.c.vitaman.dao";
        String modelOutputDir = basePath + "/dao";

        String mappingPackageName = "cn.zcltd.c.vitaman";
        String mappingOutputDir = basePath;

        BaseModelGenerator myBaseModelGenerator = new BaseModelGenerator(baseModelPackageName, baseModelOutputDir);
        myBaseModelGenerator.setTemplate("gen/model.e");

        ModelGenerator modelGenerator = new ModelGenerator(modelPackageName, baseModelPackageName, modelOutputDir);
        modelGenerator.setTemplate("gen/dao.e");

        MappingKitGenerator mappingKitGenerator = new MappingKitGenerator(mappingPackageName, mappingOutputDir);
        mappingKitGenerator.setTemplate("gen/mapping_kit.e");
        //mappingKitGenerator.setTemplate("gen/mapping_kit_cols.e");
        mappingKitGenerator.setMappingKitClassName("_MappingKit");

        Generator gernerator = new Generator(dataSource, myBaseModelGenerator, modelGenerator);
        gernerator.setMappingKitGenerator(mappingKitGenerator);

        gernerator.setMetaBuilder(new MetaBuilder(dataSource) {
            @Override
            protected boolean isSkipTable(String tableName) {
                return !tableName.startsWith("t_");
            }
        });

        gernerator.setDialect(new MysqlDialect());
        gernerator.setGenerateDaoInModel(true);
        gernerator.setGenerateDataDictionary(true);
        gernerator.setRemovedTableNamePrefixes("t_");

        gernerator.generate();
    }
}