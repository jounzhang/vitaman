package cn.zcltd.c.vitaman.service;

import cn.zcltd.btg.core.exception.BtgBizRuntimeException;
import cn.zcltd.btg.core.exception.BtgRuntimeException;
import cn.zcltd.c.vitaman.BtgSetConfig;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

/**
 * service：短信服务
 */
public class SmsService {
    public static final SmsService me = new SmsService();

    private static final String product = "Dysmsapi";
    private static final String domain = "dysmsapi.aliyuncs.com";
    private static final String accessKeyId = BtgSetConfig.smsConfigAccessKeyId();
    private static final String accessKeySecret = BtgSetConfig.smsConfigAccessKeySecret();
    private static final String signName = BtgSetConfig.smsConfigSignName();

    /**
     * 发送验证码
     *
     * @param sjh  手机号
     * @param code 验证码
     */
    public void sendCode(String sjh, String code) {
        sms(sjh, "SMS_154380063", "{\"code\":\"" + code + "\"}");
    }

    /**
     * 行程申请通知
     *
     * @param sjh  手机号
     * @param zh   账号
     * @param xcMc 行程名称
     */
    public void xcTzSq(String sjh, String zh, String cfSj, String xcMc) {
        sms(sjh, "SMS_166371252", "{\"zh\":\"" + zh + "\",\"cf_sj\":\"" + cfSj + "\",\"xc_mc\":\"" + xcMc + "\"}");
    }

    /**
     * 行程确认通知
     *
     * @param sjh  手机号
     * @param zh   账号
     * @param xcMc 行程名称
     */
    public void xcTzQr(String sjh, String zh, String cfSj, String xcMc) {
        sms(sjh, "SMS_166371254", "{\"zh\":\"" + zh + "\",\"cf_sj\":\"" + cfSj + "\",\"xc_mc\":\"" + xcMc + "\"}");
    }

    /**
     * 行程拒绝通知
     *
     * @param sjh  手机号
     * @param zh   账号
     * @param xcMc 行程名称
     */
    public void xcTzJj(String sjh, String zh, String cfSj, String xcMc) {
        sms(sjh, "SMS_166371257", "{\"zh\":\"" + zh + "\",\"cf_sj\":\"" + cfSj + "\",\"xc_mc\":\"" + xcMc + "\"}");
    }

    /**
     * 行程申请通知
     *
     * @param sjh              手机号
     * @param smsTemplateCode  短信模板编号
     * @param smsTemplateParam 短信模板参数
     */
    private void sms(String sjh, String smsTemplateCode, String smsTemplateParam) {
        try {
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);

            SendSmsRequest request = new SendSmsRequest();
            request.setPhoneNumbers(sjh);//手机号
            request.setSignName(signName);//签名
            request.setTemplateCode(smsTemplateCode);//模板编号
            request.setTemplateParam(smsTemplateParam);//模板参数
            SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
            if (!sendSmsResponse.getCode().equals("OK")) {
                throw new BtgBizRuntimeException("[" + sendSmsResponse.getCode() + "]" + sendSmsResponse.getMessage());
            }
        } catch (Exception e) {
            throw new BtgRuntimeException(e);
        }
    }
}