package cn.zcltd.c.vitaman.task;

import cn.zcltd.btg.sutil.DateUtil;
import cn.zcltd.c.vitaman.BtgSetConfig;
import cn.zcltd.c.vitaman.dao.YhXc;
import cn.zcltd.c.vitaman.task.base.BaseTask;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * task:行程自动关闭
 */
public class TaskXcAutoGb extends BaseTask {
    private static final Logger log = LoggerFactory.getLogger(TaskXcAutoGb.class);

    private static TaskXcAutoGb instance = new TaskXcAutoGb();

    public static TaskXcAutoGb getInstance() {
        return instance;
    }

    private TaskXcAutoGb() {
        this.setName("vitamam-TaskXcAutoGb");
    }

    private static final int sleep = BtgSetConfig.taskXcAutoGbSleepSeconds() * 1000;

    @Override
    public void run() {
        while (true) {
            work();
            try {
                Thread.sleep(sleep);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * 超过当前时间指定分钟数的行程自动关闭
     */
    public void work() {
        try {
            log.debug("task：[开始]行程自动关闭");

            Page<YhXc> yhXcPage = YhXc.dao.pageByKv(1, 100, Kv.create());
            for (YhXc yhXc : yhXcPage.getList()) {
                if (yhXc.getZt().equals("GB")) {
                    continue;
                }

                //出发时间超出当前时间指定分钟数，自动关闭
                if (DateUtil.dateAdd(yhXc.getCfSj(), Calendar.MINUTE, BtgSetConfig.taskXcAutoGbMinutes()).getTime() < DateUtil.getNow().getTime()) {
                    yhXc.setZt("GB");
                    yhXc.update();
                }

                if (yhXc.getZt().equals("GB")) {
                    continue;
                }

                //数量小于等于0的行程，自动关闭
                if (yhXc.getSl().setScale(0, BigDecimal.ROUND_HALF_UP).intValue() <= 0) {
                    yhXc.setZt("GB");
                    yhXc.update();
                }
            }

            log.debug("task：[结束]行程自动关闭");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
