package cn.zcltd.c.vitaman.task;


import cn.zcltd.btg.sutil.DateUtil;
import cn.zcltd.c.vitaman.BtgSetConfig;
import cn.zcltd.c.vitaman.dao.YhXc;
import cn.zcltd.c.vitaman.task.base.BaseTask;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Calendar;

/**
 * task:行程自动删除
 */
public class TaskXcAutoSc extends BaseTask {
    private static final Logger log = LoggerFactory.getLogger(TaskXcAutoSc.class);

    private static TaskXcAutoSc instance = new TaskXcAutoSc();

    public static TaskXcAutoSc getInstance() {
        return instance;
    }

    private TaskXcAutoSc() {
        this.setName("vitamam-TaskXcAutoSc");
    }

    private static final int sleep = BtgSetConfig.taskXcAutoScSleepSeconds() * 1000;

    @Override
    public void run() {
        while (true) {
            work();
            try {
                Thread.sleep(sleep);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * 超过当前时间指定分钟数的行程自动删除
     */
    public void work() {
        try {
            log.debug("task：[开始]行程自动删除");

            Page<YhXc> yhXcPage = YhXc.dao.pageByKv(1, 100, Kv.create());
            for (YhXc yhXc : yhXcPage.getList()) {

                //出发时间超出当前时间指定分钟数，自动关闭
                if (DateUtil.dateAdd(yhXc.getCfSj(), Calendar.MINUTE, BtgSetConfig.taskXcAutoScMinutes()).getTime() < DateUtil.getNow().getTime()) {
                    Db.tx(new IAtom() {
                        @Override
                        public boolean run() throws SQLException {
                            YhXc.dao.sc(yhXc.getKeyId());
                            return true;
                        }
                    });
                }
            }

            log.debug("task：[结束]行程自动删除");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
