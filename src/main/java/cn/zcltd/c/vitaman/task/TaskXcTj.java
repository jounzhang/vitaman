package cn.zcltd.c.vitaman.task;


import cn.zcltd.btg.sutil.DateUtil;
import cn.zcltd.btg.sutil.EmptyUtil;
import cn.zcltd.c.vitaman.BtgSetConfig;
import cn.zcltd.c.vitaman.dao.*;
import cn.zcltd.c.vitaman.task.base.BaseTask;
import com.jfinal.kit.Kv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * task:行程统计数据清理
 */
public class TaskXcTj extends BaseTask {
    private static final Logger log = LoggerFactory.getLogger(TaskXcTj.class);

    private static TaskXcTj instance = new TaskXcTj();

    public static TaskXcTj getInstance() {
        return instance;
    }

    private TaskXcTj() {
        this.setName("vitamam-TaskXcTj");
    }

    private static final int sleep = BtgSetConfig.taskXcTjSleepSeconds() * 1000;

    @Override
    public void run() {
        while (true) {
            work();
            try {
                Thread.sleep(sleep);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * 每日检测前3日是否有数据
     * 没有就进行清算
     * 仅计算到前一日，当日数据右上角为最新
     */
    public void work() {
        try {
            log.debug("task：[开始]清算行程统计");

            //查找昨日行程数据是否已经统计
            String preDate = DateUtil.formatDate(DateUtil.dateAdd(DateUtil.getNow(), Calendar.DATE, -1));
            day(preDate);

            log.debug("task：[结束]行程统计清算");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void day(String date) {
        Tj tj = Tj.dao.getByTjRq(date);
        if (EmptyUtil.isEmpty(tj)) {
            /*
                行程由两部分组成，在线的和归档的
             */
            //用户数
            int numYh = Yh.dao.countByKv(Kv.create());

            //行程车
            int numXcC = YhXc.dao.countByLxCfRq("C", date);
            int numXcHisC = YhXcHis.dao.countByLxCfRq("C", date);

            //行程人
            int numXcR = YhXc.dao.countByLxCfRq("R", date);
            int numXcHisR = YhXcHis.dao.countByLxCfRq("R", date);

            //伙伴车
            int numHbC = YhXcHb.dao.countByLxSqRq("C", date);
            int numHbHisC = YhXcHbhis.dao.countByLxSqRq("C", date);

            //伙伴人
            int numHbR = YhXcHb.dao.countByLxSqRq("R", date);
            int numHbHisR = YhXcHbhis.dao.countByLxSqRq("R", date);

            tj = new Tj();
            tj.setTjRq(DateUtil.parseDate(date));
            tj.setYhSl(new BigDecimal(numYh));
            tj.setXcSlC(new BigDecimal(numXcC).add(new BigDecimal(numXcHisC)));
            tj.setXcSlR(new BigDecimal(numXcR).add(new BigDecimal(numXcHisR)));
            tj.setHbSlC(new BigDecimal(numHbC).add(new BigDecimal(numHbHisC)));
            tj.setHbSlR(new BigDecimal(numHbR).add(new BigDecimal(numHbHisR)));
            tj.setTjSj(DateUtil.getNow());
            tj.save();
        }
    }
}
