package cn.zcltd.c.vitaman.task.base;

/**
 * 任务处理
 */
public abstract class BaseTask extends Thread {

    /**
     * 执行任务
     */
    public abstract void work();
}
