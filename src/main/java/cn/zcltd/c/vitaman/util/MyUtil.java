package cn.zcltd.c.vitaman.util;

import cn.zcltd.btg.sutil.EmptyUtil;
import cn.zcltd.c.vitaman.dao.YhXc;

public class MyUtil {

    /**
     * 生成行程复制内容
     *
     * @param requestRootUrl 基础url
     * @param yhXc           行程
     * @return 行程文本
     */
    public static String genXcCopy(String requestRootUrl, YhXc yhXc) {
        StringBuilder xcCopySb = new StringBuilder();
        xcCopySb.append("哎哎~");
        if (yhXc.getLx().equals("C")) {
            xcCopySb.append("车找人");
        } else {
            xcCopySb.append("人找车");
        }
        xcCopySb.append("，");
        if (yhXc.getStr("cf_rq_lx").equals("MR")) {
            xcCopySb.append("明日");
        } else if (yhXc.getStr("cf_rq_lx").equals("QT")) {
            xcCopySb.append(yhXc.getStr("cf_rq")).append(" ");
        }
        xcCopySb.append(yhXc.getStr("cf_sj"));
        xcCopySb.append(yhXc.getMc().replace("-", "到"));
        if (EmptyUtil.isNotEmpty(yhXc.getDdStr())) {
            xcCopySb.append("，途径").append(yhXc.getDdStr());
        }
        xcCopySb.append("，");
        if (yhXc.getLx().equals("C")) {
            xcCopySb.append("空").append(yhXc.getSl());
        } else {
            xcCopySb.append(yhXc.getSl()).append("人");
        }
        xcCopySb.append("，");
        xcCopySb.append(yhXc.getJe()).append("元/人");
        if (EmptyUtil.isNotEmpty(yhXc.getBz())) {
            xcCopySb.append("，").append(yhXc.getBz());
        }
        xcCopySb.append("，走的火速联系\r\n");
        xcCopySb.append(requestRootUrl).append("/").append(yhXc.getXcBh());
        return xcCopySb.toString();
    }
}
