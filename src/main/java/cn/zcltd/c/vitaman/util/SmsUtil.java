package cn.zcltd.c.vitaman.util;

import java.util.Random;

/**
 * 项目工具类
 */
public class SmsUtil {

    /**
     * 生成短信验证码
     *
     * @return 6位随机短信验证码
     */
    public static String genSmsCode() {
        return String.valueOf(new Random().nextInt(899999) + 100000);
    }

    /**
     * 检查短信文本
     *
     * @param text 短信文本
     * @return 处理后的文本
     */
    public static String checkSmsText(String text) {
        String pattern = "([-+*/^()\\]\\[\"|/\\\\<>?？&$￥% @;:：；“”‘’\t\n])";
        return text.replaceAll(pattern, "");
    }
}