package cn.zcltd.c.vitaman.util;

import java.nio.charset.Charset;

public class UTF8MB4 {
    private final static Charset UTF8 = Charset.forName("UTF-8");

    /**
     * 判断字符串中是否存在4字节字符
     *
     * @param input 输入字符串
     * @return 包含4字节返回true， 否则为false
     */
    public static boolean containsMb4Char(String input) {
        if (input == null) {
            return false;
        }
        byte[] bytes = input.getBytes(UTF8);
        for (int i = 0; i < bytes.length; i++) {
            byte b = bytes[i];
            //four bytes
            if ((b & 0XF0) == 0XF0) {
                return true;
            } else if ((b & 0XE0) == 0XE0) {
                //three bytes
                //forward 2 byte
                i += 2;
            } else if ((b & 0XC0) == 0XC0) {
                i += 1;
            }
        }
        return false;
    }

    /**
     * 替换可能存在的utf8 4字节字符
     *
     * @param input       输入字符串
     * @param replacement 替换为的字符串
     * @return 替换后的utf8字符串
     */
    public static String replaceMb4Char(String input, String replacement) {
        if (input == null) {
            throw new IllegalArgumentException("input can not be null when replaceMb4Char");
        }

        StringBuilder sb = new StringBuilder(input.length());
        byte[] bytes = input.getBytes(UTF8);
        char[] chars = input.toCharArray();
        int charIdx = 0;
        for (int i = 0; i < bytes.length; i++) {
            byte b = bytes[i];
            //four bytes
            if ((b & 0XF0) == 0XF0) {
                sb.append(replacement);
                //utf-8四字节字符unicode后变为2个字符， 故字符下标多加1
                charIdx += 2;
                i += 3;
                continue;
            } else if ((b & 0XE0) == 0XE0) {
                //three bytes
                //forward 2 byte
                i += 2;
            } else if ((b & 0XC0) == 0XC0) {
                i += 1;
            }
            sb.append(chars[charIdx]);
            charIdx++;
        }
        return sb.toString();
    }

    /**
     * 去除可能存在的utf8 4字节字符
     *
     * @param s 输入字符串
     * @return 替换后的utf8字符串
     */
    public static String removeMb4Char(String s, String replacement) {
        return s.replaceAll("[\ud800\udc00-\udbff\udfff\ud800-\udfff]", replacement);
    }

    /**
     * 替换四个字节的字符 '\xF0\x9F\x98\x84\xF0\x9F的解决方案
     * 如😁
     *
     * @param content 内容
     * @return 替换后的内容
     */
    public static String removeFourChar(String content) {
        byte[] conbyte = content.getBytes();
        for (int i = 0; i < conbyte.length; i++) {
            if ((conbyte[i] & 0xF8) == 0xF0) {
                for (int j = 0; j < 4; j++) {
                    conbyte[i + j] = 0x30;
                }
                i += 3;
            }
        }
        content = new String(conbyte);
        return content.replaceAll("0000", "");
    }
}
