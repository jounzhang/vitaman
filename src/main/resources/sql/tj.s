#namespace("tj")
  #sql("getByKv")
    SELECT
    	tj.*
    FROM
    	t_tj tj
    WHERE
    	1 = 1
    #if(tj_rq)
    AND tj.tj_rq = #para(tj_rq)
    #end
  #end

  #sql("list")
    SELECT
    	tj.*
    FROM
    	t_tj tj
    WHERE
    	1 = 1
    ORDER BY
    	tj.tj_rq DESC
  #end
#end