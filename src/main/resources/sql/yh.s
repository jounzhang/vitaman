#namespace("yh")
  #sql("getByKv")
    SELECT
      yh.*
    FROM
      t_yh yh
    WHERE
      1 = 1
    #if(sjh)
    AND yh.sjh = #para(sjh)
    #end
  #end

  #sql("getNextUserNo")
    SELECT
    	ifnull(max(yh.bh), 1000000) + 1 AS bh
    FROM
    	t_yh yh
  #end

  #sql("listTopOrderByHyz")
    SELECT
    	yh.*
    FROM
    	t_yh yh
    ORDER BY
      yh.hyz desc,
      yh.zc_sj
  #end

  #sql("countByKv")
    SELECT
    	count(1)
    FROM
    	t_yh yh
    WHERE
      1 = 1
    #if(zc_rq)
    AND DATE_FORMAT(yh.zc_sj, '%Y-%m-%d') = #para(zc_rq)
    #end
  #end
#end