#namespace("yh")
  #sql("hyz.getByKv")
    SELECT
      hyz.*
    FROM
      t_yh_hyz hyz
    WHERE
      1 = 1
    #if(yh_id)
    AND hyz.yh_id = #para(yh_id)
    #end
    #if(rq)
    AND hyz.rq = #para(rq)
    #end
    #if(lx)
    AND hyz.lx = #para(lx)
    #end
  #end

  #sql("hyz.list")
    SELECT
      hyz.*
    FROM
      t_yh_hyz hyz
    WHERE
      1 = 1
    #if(yh_id)
    AND hyz.yh_id = #para(yh_id)
    #end
    ORDER BY
    	hyz.rq DESC,
    	hyz.lx
  #end

  #sql("hyz.listTopByKvOrderHyz")
    SELECT
    	yh.*, v.sl AS yhz_jr
    FROM
    	(
    		SELECT
    			hyz.yh_id,
    			sum(sl) AS sl,
    			max(hq_sj) AS hq_sj
    		FROM
    			t_yh_hyz hyz
    		WHERE
    			1 = 1
    	  #if(rq)
    		AND hyz.rq = #para(rq)
    		#end
    		GROUP BY
          hyz.yh_id
        ORDER BY
          sl DESC,
          hq_sj
      ) v
    INNER JOIN t_yh yh ON yh.key_id = v.yh_id
  #end
#end