#namespace("yh")
  #sql("lx.countByKv")
    SELECT
    	count(1)
    FROM
    	t_yh_lx lx
    WHERE
    	1 = 1
    #if(lx)
    AND lx.lx = #para(lx)
    #end
    #if(yh_id)
    AND lx.yh_id = #para(yh_id)
    #end
  #end

  #sql("lx.get")
    SELECT
    	lx.*
    FROM
    	t_yh_lx lx
    WHERE
    	1 = 1
    #if(lx)
    AND lx.lx = #para(lx)
    #end
    #if(yh_id)
    AND lx.yh_id = #para(yh_id)
    #end
    #if(mc)
    AND lx.mc = #para(mc)
    #end
    #if(cf_sj)
    AND lx.cf_sj = #para(cf_sj)
    #end
    #if(sl)
    AND lx.sl = #para(sl)
    #end
    #if(je)
    AND lx.je = #para(je)
    #end
    #if(dd_str)
    AND lx.dd_str = #para(dd_str)
    #end
    #if(bz)
    AND lx.bz = #para(bz)
    #end
    #if(key_id_not)
    AND lx.key_id != #para(key_id_not)
    #end
  #end

  #sql("lx.list")
    SELECT
      lx.*
    FROM
      t_yh_lx lx
    WHERE
      1 = 1
    #if(lx)
    AND lx.lx = #para(lx)
    #end
    #if(yh_id)
    AND lx.yh_id = #para(yh_id)
    #end
    ORDER BY
    	lx.cf_sj
  #end
#end