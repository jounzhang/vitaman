#namespace("yh")
  #sql("xc.countByKv")
    SELECT
    	count(1)
    FROM
    	t_yh_xc xc
    WHERE
    	1 = 1
    #if(lx)
    AND xc.lx = #para(lx)
    #end
    #if(yh_id)
    AND xc.yh_id = #para(yh_id)
    #end
    #if(cf_rq)
    AND DATE_FORMAT(xc.cf_sj, '%Y-%m-%d') = #para(cf_rq)
    #end
    #if(zt)
    AND xc.zt = #para(zt)
    #end
  #end

  #sql("xc.getByKv")
    SELECT
    	xc.*
    FROM
    	t_yh_xc xc
    WHERE
    	1 = 1
    #if(xc_bh)
    AND xc.xc_bh = #para(xc_bh)
    #end
    #if(lx)
    AND xc.lx = #para(lx)
    #end
    #if(yh_id)
    AND xc.yh_id = #para(yh_id)
    #end
    #if(mc)
    AND xc.mc = #para(mc)
    #end
    #if(cf_sj)
    AND xc.cf_sj = #para(cf_sj)
    #end
    #if(sl)
    AND xc.sl = #para(sl)
    #end
    #if(je)
    AND xc.je = #para(je)
    #end
    #if(dd_str)
    AND xc.dd_str = #para(dd_str)
    #end
    #if(bz)
    AND xc.bz = #para(bz)
    #end
    #if(key_id_not)
    AND xc.key_id != #para(key_id_not)
    #end
    #if(is_default)
    AND xc.is_default = #para(is_default)
    #end
  #end

  #sql("xc.list")
    SELECT
    	xc.*, yh.bh,
    	yh.xm,
    	yh.sjh,
    	yh.sjh_is_gk,
    	yh.zh_qq,
    	yh.zh_wx,
    	yh.zh_zfb,
    	yh.cl_hm,
    	yh.cl_ppxh,
    	yh.cl_ys,
    	yh.cl_lx,
    	yh.cl_img,
    	yh.hyz,
    	(
    		SELECT
    			sign(count(1))
    		FROM
    			t_yh_xc_hb hb
    		WHERE
    			hb.xc_id = xc.key_id
    		AND hb.hb_id = '#(hb_id)'
    	) AS is_sq,
    	abs(
        DATE_FORMAT(xc.cf_sj, '%Y%m%d%H%i%s') - DATE_FORMAT(now(), '%Y%m%d%H%i%s')
      ) AS cf_sj_jdz
    FROM
    	t_yh_xc xc
    INNER JOIN t_yh yh ON yh.key_id = xc.yh_id
    WHERE
    	1 = 1
    #if(lx)
    AND xc.lx = #para(lx)
    #end
    #if(yh_id)
    AND xc.yh_id = #para(yh_id)
    #end
    #if(yh_id_not)
    AND xc.yh_id != #para(yh_id_not)
    #end
    #if(cf_sj_begin)
    AND xc.cf_sj >= #para(cf_sj_begin)
    #end
    #if(cf_sj_end)
    AND xc.cf_sj <= #para(cf_sj_end)
    #end
    #if(dd_str_like)
    AND (xc.mc like #para(dd_str_like) OR xc.dd_str like #para(dd_str_like))
    #end
    #if(zt)
    AND xc.zt = #para(zt)
    #end
    ORDER BY
    	xc.is_default DESC,
      cf_sj_jdz,
    	xc.fb_sj
  #end

  #sql("xc.listHome")
    SELECT
    	xc.*, yh.bh,
    	yh.xm,
    	yh.sjh,
    	yh.sjh_is_gk,
    	yh.zh_qq,
    	yh.zh_wx,
    	yh.zh_zfb,
    	yh.cl_hm,
    	yh.cl_ppxh,
    	yh.cl_ys,
    	yh.cl_lx,
    	yh.cl_img,
    	yh.hyz,
    	(
    		SELECT
    			sign(count(1))
    		FROM
    			t_yh_xc_hb hb
    		WHERE
    			hb.xc_id = xc.key_id
    		AND hb.hb_id = '#(hb_id)'
    	) AS is_sq,
    	abs(
        DATE_FORMAT(xc.cf_sj, '%Y%m%d%H%i%s') - DATE_FORMAT(now(), '%Y%m%d%H%i%s')
      ) AS cf_sj_jdz
    FROM
    	t_yh_xc xc
    INNER JOIN t_yh yh ON yh.key_id = xc.yh_id
    WHERE
    	1 = 1
    #if(lx)
    AND xc.lx = #para(lx)
    #end
    #if(yh_id)
    AND xc.yh_id = #para(yh_id)
    #end
    #if(yh_id_not)
    AND xc.yh_id != #para(yh_id_not)
    #end
    #if(cf_sj_begin)
    AND xc.cf_sj >= #para(cf_sj_begin)
    #end
    #if(cf_sj_end)
    AND xc.cf_sj <= #para(cf_sj_end)
    #end
    #if(dd_str_like)
    AND (xc.mc like #para(dd_str_like) OR xc.dd_str like #para(dd_str_like))
    #end
    #if(zt)
    AND xc.zt = #para(zt)
    #end
    ORDER BY
      cf_sj_jdz,
    	yh.hyz,
    	xc.je,
    	xc.fb_sj
  #end

  #sql("xc.updateDefaultByKv")
    UPDATE t_yh_xc xc
    SET xc.is_default = '#(value)'
    WHERE
    	1 = 1
    #if(lx)
    AND xc.lx = #para(lx)
    #end
    #if(yh_id)
    AND xc.yh_id = #para(yh_id)
    #end
    #if(key_id)
    AND xc.key_id = #para(key_id)
    #end
  #end

  #sql("xc.getNextXcBh")
    SELECT
    	ifnull(max(v.xc_bh), 1000000) + 1 AS xc_bh
    FROM
    	(
    		SELECT
    			max(xc.xc_bh) AS xc_bh
    		FROM
    			t_yh_xc xc
    		UNION ALL
    			SELECT
    				max(xchis.xc_bh) AS xc_bh
    			FROM
    				t_yh_xc_his xchis
    	) v
  #end
#end