#namespace("yh")
  #sql("xc_hbhis.cdByKv")
    INSERT INTO t_yh_xc_hbhis SELECT
    	hb.*
    FROM
    	t_yh_xc_hb hb
    WHERE
    	1 = 1
    #if(xc_id)
    AND hb.xc_id = #para(xc_id)
    #end
    #if(ly_xc_id)
    AND hb.ly_xc_id = #para(ly_xc_id)
    #end
  #end

  #sql("xc_hbhis.countByKv")
    SELECT
    	count(1)
    FROM
    	t_yh_xc_hbhis hbhis
    WHERE
    	1 = 1
    #if(lx)
    AND hbhis.lx = #para(lx)
    #end
    #if(sq_rq)
    AND DATE_FORMAT(hbhis.sq_sj, '%Y-%m-%d') = #para(sq_rq)
    #end
  #end
#end