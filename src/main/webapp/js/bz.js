layui.use(['index', 'layout'], function () {
  var $ = layui.$,
    admin = layui.admin,
    layout = layui.layout,

    bindTyxx = function () {
      $('button[tyxx]').unbind('click').bind('click', function () {
        var that = $(this);

        admin.req({
          type: 'post',
          data: {
            is_ty_xx: 'YES'
          },
          url: '/bzXx',
          success: function (res) {
            that.addClass('layui-hide');
            that.unbind('click');

            var jj = that.next();
            jj.removeClass('layui-hide');
            bindJjxx();

            var txt = that.prev();
            txt.text('（已同意）');

            admin.msg('感谢支持', function () {
              var _url = admin.getUrlParam("_url");
              if (_url) {
                admin.changeUrl(_url);
              }
            });
          }
        });
        return false;
      });
    },
    bindJjxx = function () {
      $('button[jjxx]').unbind('click').bind('click', function () {
        var that = $(this);
        layer.confirm('确认暂停使用？', {icon: 3, title: '提示'}, function (index) {
          admin.req({
            type: 'post',
            data: {
              is_ty_xx: 'NO'
            },
            url: '/bzXx',
            success: function (res) {
              that.addClass('layui-hide');
              that.unbind('click');

              var ty = that.prev();
              ty.removeClass('layui-hide');
              bindTyxx();

              var txt = that.prev().prev();
              txt.text('（请阅读后点击同意）');

              admin.msg('感谢支持，欢迎再次回来使用！');
            }
          });
        });
        return false;
      });
    }
  ;

  admin.execHash();

  admin.fixbar();

  layout.layout(2);

  bindTyxx();
  bindJjxx();

  if (admin.isPc()) {
    $('.qq').removeClass('layui-hide');
  }
});