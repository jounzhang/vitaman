layui.use(['index', 'form'], function () {
  var $ = layui.$,
    admin = layui.admin,
    form = layui.form,

    sjh = $('#sjh'),
    pwd = $('#pwd'),

    loadindex = -1,
    showLoading = function () {
      loadindex = layer.load();
    },
    closeLoading = function () {
      loadindex > 0 && (layer.close(loadindex) , loadindex = -1);
    }
  ;

  sjh.focus();

  form.verify({
    pass: [
      /^[\S]{6,12}$/
      , '密码必须6到12位，且不能出现空格'
    ]
  });

  form.on('submit(btnSubmit)', function (obj) {
    showLoading();
    admin.req({
      type: 'post',
      data: obj.field,
      url: '/dlSave',
      success: function (res) {
        admin.msg('登录成功', function () {
          var _url = admin.getUrlParam("_url");
          admin.changeUrl(_url ? _url : "/home");
        })
      },
      done: function () {
        closeLoading();
      }
    })
    return false;
  });
});