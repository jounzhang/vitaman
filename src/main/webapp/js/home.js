layui.use(['index', 'form', 'laypage', 'laydate', 'layout', 'top'], function () {
  var $ = layui.$,
    element = layui.element,
    laytpl = layui.laytpl,
    admin = layui.admin,
    form = layui.form,
    laypage = layui.laypage,
    layout = layui.layout,
    laydate = layui.laydate,

    pageIndex = $('#pageIndex'),
    pageSize = $('#pageSize'),
    contentList = $('#contentList'),
    tplItem = $('#tplItem'),
    cf_rq = $('#cf_rq'),

    bindSq = function () {
      contentList.find('input[type=button][sq]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        admin.req({
          type: 'post',
          data: {
            xc_id: id
          },
          url: '/homeXcSq',
          success: function (res) {
            that.addClass('layui-btn-disabled');
            that.unbind('click');
            admin.msg('申请成功，快到行程查看各种联系方式进行联系');
            admin.changeUrl('/xc')
          }
        });
        return false;
      });
    },
    bindCopy = function () {
      var clipboard = new ClipboardJS('input[type=button][fz]');
      clipboard.on('success', function (e) {
        admin.msg('复制成功，快去粘贴');
      });
      clipboard.on('error', function (e) {
        admin.msg('喔豁，当前环境不支持');
      });
      return false;
    },
    bindAll = function () {
      bindSq();
      showQq();
    },
    showQq = function () {
      if (admin.isPc()) {
        $('.qq').removeClass('layui-hide');
      }
    },
    search = function () {
      var param = form.val('formSearch');
      param = $.extend(param, {
        pageIndex: pageIndex.val(),
        pageSize: pageSize.val()
      });
      admin.req({
        type: 'post',
        data: param,
        url: '/homeXc',
        success: function (res) {
          var data = res.data;

          var pageConfig = {
            count: data.total,
            curr: pageIndex.val(),
            limit: pageSize.val(),
            layout: ['prev', 'next'],
            jump: function (obj, first) {
              if (!first) {
                pageIndex.val(obj.curr);
                pageSize.val(obj.limit);
                search();
              }
            }
          };

          laypage.render($.extend({elem: 'pageTop'}, pageConfig));
          laypage.render($.extend({elem: 'pageBottom'}, pageConfig));

          contentList.html('');
          laytpl(tplItem.html()).render(data, function (html) {
            contentList.html(html);
            element.render('collapse');
            bindAll();
          });
        }
      })
    }
  ;

  layout.layout(0);

  admin.fixbar();
  admin.checkInfo({
    isCheckJbxx: true,
    isCheckClxx: true,
    isCheckXc: true
  });

  laydate.render({
    elem: '#cf_rq',
    btns: ['clear', 'now'],
    value: cf_rq.val(),
    done: function (value, date, endDate) {
      cf_rq.val(value);
      search();
    }
  });

  form.on('submit(btnSubmit)', function (obj) {
    search();
    return false;
  });

  search();

  bindCopy();
});