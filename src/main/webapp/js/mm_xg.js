layui.use(['index', 'form'], function () {
  var $ = layui.$,
    admin = layui.admin,
    form = layui.form,

    pwd_old = $('#pwd_old'),
    pwd = $('#pwd')
  ;

  pwd_old.focus();

  form.verify({
    pass: [
      /^[\S]{6,12}$/
      , '密码必须6到12位，且不能出现空格'
    ],
    pass2: function (value, item) {
      if (!new RegExp("^[\\S]{6,12}$").test(value)) {
        return '密码必须6到12位，且不能出现空格';
      }
      if (pwd.val() !== value) {
        return '两次密码输入不一致';
      }
    }
  });

  form.on('submit(btnSubmit)', function (obj) {
    admin.req({
      type: 'post',
      data: obj.field,
      url: '/mmXgSave',
      success: function (res) {
        admin.msg('修改成功', function () {
          admin.changeUrl("/zx");
        })
      }
    })
    return false;
  });
});