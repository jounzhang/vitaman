layui.define('view', function (exports) {
  var view = layui.view;

  var admin = {
    v: '1.0.1'
    , isPc: view.isPc
    , back: view.back
    , getUrlParam: view.getUrlParam
    , getData: view.getData
    , getToken: view.getToken
    , getToken4Url: view.getToken4Url
    , f5: view.f5
    , changeUrl: view.changeUrl
    , changeUrl4Token: view.changeUrl4Token
    , token: view.token
    , load: view.load
    , removeLoad: view.removeLoad
    , msg: view.msg
    , req: view.req
    , upload: view.upload
    , bindDataUrl: view.bindDataUrl
    , bindImgShow: view.bindImgShow
    , execHash: view.execHash
    , validSjh: view.validSjh
    , setTjr: view.setTjr
    , fixbar: view.fixbar
    , checkInfo: view.checkInfo
    , init: view.init
  };

  exports('admin', admin);
});