layui.define(['index'], function (exports) {
  var $ = layui.$;

  var layout = {
    /**
     * 下方模块选中样式控制
     * @param index 模块索引，从0开始
     */
    layout: function (index) {
      var items = $('.bottom .icon-list');
      items.children().css('color', '#555555');
      items.children(':eq(' + index + ')').css('color', '#F29700');
    }
  };

  exports('layout', layout);
});