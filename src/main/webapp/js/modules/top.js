layui.define(['admin'], function (exports) {
  var $ = layui.$,
    admin = layui.admin
  ;

  //切换身份
  $('.top > .nav-list > .nav-item[qh]').unbind('click').bind('click', function () {
    admin.req({
      type: 'post',
      data: {},
      url: '/qh',
      success: function (res) {
        admin.msg('切换成功', function () {
          admin.f5();
        })
      }
    })
  });

  var methods = {};

  exports('top', methods);
});