layui.define(['laytpl', 'layer', 'upload'], function (exports) {
  var $ = layui.jquery
    , util = layui.util
    , laytpl = layui.laytpl
    , layer = layui.layer
    , upload = layui.upload
    , setter = layui.setter
    , hint = layui.hint()
    , $win = $(window)
    , keys = layui.cache.keys
    , cookie = layui.cookie;

  //构造器
  Class = function (id) {
    this.id = id;
    this.container = $('#' + (id || LAY_BODY));
  }

  //对外接口
  view = function (id) {
    return new Class(id);
  }

  LAY_BODY = 'LAY_app_body'

  /**
   * 是否pc浏览器
   * @returns {boolean}
   */
  view.isPc = function () {
    if (!navigator.userAgent) {
      return false;
    }
    var ua = navigator.userAgent;
    var regex_match = /(nokia|iphone|android|motorola|^mot-|softbank|foma|docomo|kddi|up.browser|up.link|htc|dopod|blazer|netfront|helio|hosin|huawei|novarra|CoolPad|webos|techfaith|palmsource|blackberry|alcatel|amoi|ktouch|nexian|samsung|^sam-|s[cg]h|^lge|ericsson|philips|sagem|wellcom|bunjalloo|maui|symbian|smartphone|midp|wap|phone|windows ce|iemobile|^spice|^bird|^zte-|longcos|pantech|gionee|^sie-|portalmmm|jigs browser|hiptop|^benq|haier|^lct|operas*mobi|opera*mini|320x320|240x320|176x220)/i;
    return null == regex_match.exec(ua);
  }

  /**
   * 返回上页
   */
  view.back = function () {
    history.back();
  }

  /**
   * 从url中获取参数
   * @param url url
   * @param name 参数名称
   * @returns String 参数值
   */
  view.getUrlParam = function (name, url) {
    url = url ? url : window.location.search;
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = url.substr(1).match(reg);
    if (r != null) {
      return decodeURIComponent(r[2]);
    }
    return null;
  }

  /**
   * 获取url
   * @returns String url
   */
  view.getUrl = function () {
    var url = window.location.href;
    return url;
  }

  /**
   * 获取data
   * @returns {*} data
   */
  view.getData = function (name) {
    var db = layui.data(setter.tableName);
    if (!db) {
      return '';
    }
    var v = db[name];
    if (!v) {
      return '';
    }
    return v;
  }

  /**
   * 获取token
   * @returns {*} token
   */
  view.getToken = function () {
    return view.getData(setter.request.tokenName);
  }

  /**
   * 获取token并追加至url中
   * @returns {*} 含token的url
   */
  view.getToken4Url = function (url) {
    //var tjr_info = 'tjr_id=' + view.getToken();
    //
    //if (url.indexOf('?') == -1) {
    //  return url + '?' + tjr_info;
    //}
    //
    //var urlSearch = url.substring(url.indexOf('?'));
    //var tjr = view.getUrlParam('tjr', urlSearch);
    //if (!tjr) {
    //  url += '&' + tjr_info;
    //}
    return url;
  }

  /**
   * 跳转页面
   * @param url 路径url
   */
  view.changeUrl = function (url) {
    url = url.indexOf('http://') === 0 || url.indexOf('https://') === 0 ? url : layui.cache.path + url;
    location.href = url;
  }

  /**
   * 跳转页面-带token
   * @param url 路径url
   */
  view.changeUrl4Token = function (url) {
    url = view.getToken4Url(url);
    view.changeUrl(url);
  }

  /**
   * 刷新当前页面
   */
  view.f5 = function () {
    window.location.reload();
  }

  /**
   * 重新授权
   */
  view.token = function () {
    var url = encodeURIComponent(encodeURIComponent(location.href));
    view.changeUrl("/token?url=" + url);
  }

  //加载中
  view.load = function (options) {
    return layer.load($.extend(true, {}, options));
  };

  //移除加载
  view.removeLoad = function (index) {
    layer.close(index);
  };

  //提示消息
  view.msg = function (msg, options, callback) {
    callback = $.type(options) == 'function' ? options : callback;
    options = $.type(options) == 'object' ? options : {};
    options = $.extend({time: 1500}, options);
    layer.msg(msg, options, callback);
  }

  //Ajax请求
  view.req = function (options) {
    var that = this
      , loading = options.loading
      , success = options.success
      , fail = options.fail
      , error = options.error
      , done = options.done
      , request = setter.request
      , response = setter.response
      , debug = function () {
      return setter.debug ? options.url : '';
    }

      , loadIndex;
    loading = loading === false ? loading : true;

    options.url = layui.cache.path + options.url;
    options.data = options.data || {};
    options.headers = options.headers || {};
    options.headers[setter.request.tokenName] = view.getToken();

    delete options.success;
    delete options.fail;
    delete options.error;
    delete options.done;

    loading && (loadIndex = layer.load());
    return $.ajax($.extend(true, {
      type: 'get'
      , dataType: 'json'
      , success: function (res) {
        var resp = res['resp'];

        var resp_code = resp['resp_code'];

        //登录过期
        if (resp_code === keys.RESP_RESPCODE_SESSION_TIMEOUT) {
          loading && layer.close(loadIndex);
          layer.alert('未登录或登录过期，请先登录！', {icon: 5}, function () {
            var _url = view.getUrl();
            view.changeUrl("/dl?_url=" + _url);
          });
          return;
        }

        //未同意协议
        if (resp_code === keys.RESP_RESPCODE_WTY_XX) {
          loading && layer.close(loadIndex);
          layer.alert('请先阅读并同意使用协议', {icon: 5}, function () {
            var _url = view.getUrl();
            view.changeUrl("/bz?_url=" + _url);
          });
          return;
        }

        if (resp_code !== keys.RESP_RESPCODE_SUCCESS) {
          loading && layer.close(loadIndex);
          layer.alert(keys.RESP_RESPDESC_ERROR, {icon: 5}, function () {
            var resp_desc = resp['resp_desc'];
            layui.hint().error('Resp[' + resp_code + ',' + resp_desc + ',' + debug() + ']');
            typeof fail === 'function' && fail(res);
            typeof done === 'function' && done(res);
            view.changeUrl("/");
          });
          return;
        }

        var biz_code = resp['biz_code'];
        if (biz_code !== keys.RESP_BIZCODE_SUCCESS) {
          loading && layer.close(loadIndex);
          var biz_desc = resp['biz_desc'];
          top.layer.msg(biz_desc);
          typeof fail === 'function' && fail(res);
          typeof done === 'function' && done(res);
          return;
        }

        loading && layer.close(loadIndex);
        typeof success === 'function' && success(res);
        typeof done === 'function' && done(res);
      }
      , error: function (e, code) {
        loading && layer.close(loadIndex);
        top.layer.msg('请求接口异常，请重试！', {icon: 5});
        layui.hint().error('Error[' + e.status + ',' + code + ',' + debug() + ']');
        typeof done === 'function' && done();
        typeof error === 'function' && error(e, code);
      }
    }, options));
  };

  //上传
  view.upload = function (options) {
    var that = this
      , done = options.done
      , debug = function () {
      return setter.debug ? options.url : '';
    };

    options.url = layui.cache.path + options.url;
    options.data = options.data || {};
    options.headers = options.headers || {};
    options.headers[setter.request.tokenName] = view.getToken();

    delete options.error;
    delete options.done;

    upload.render($.extend(true, {
      done: function (res, index, uploadInst) {
        var resp = res['resp'];

        var resp_code = resp['resp_code'];

        //登录过期
        if (resp_code === keys.RESP_RESPCODE_SESSION_TIMEOUT) {
          view.changeUrl('/dl');
          return;
        }

        //未同意协议
        if (resp_code === keys.RESP_RESPCODE_WTY_XX) {
          view.changeUrl('/bz');
          return;
        }

        if (resp_code !== keys.RESP_RESPCODE_SUCCESS) {
          var resp_desc = resp['resp_desc'];
          top.layer.msg(keys.RESP_RESPDESC_ERROR, {icon: 5});
          layui.hint().error('Resp[' + resp_code + ',' + resp_desc + ',' + debug() + ']');
          return;
        }

        var biz_code = resp['biz_code'];
        if (biz_code !== keys.RESP_BIZCODE_SUCCESS) {
          var biz_desc = resp['biz_desc'];
          top.layer.msg(biz_desc);
          return;
        }

        typeof done === 'function' && done(res, index, uploadInst);
      }
    }, options));
  };

  ////弹窗
  //view.popup = function (options) {
  //  var success = options.success
  //    , skin = options.skin;
  //
  //  delete options.success;
  //  delete options.skin;
  //
  //  return layer.open($.extend({
  //    type        : 1
  //    , title     : '提示'
  //    , content   : ''
  //    , id        : 'LAY-system-view-popup'
  //    , skin      : 'layui-layer-admin' + (skin ? ' ' + skin : '')
  //    , shadeClose: true
  //    , closeBtn  : false
  //    , success   : function (layero, index) {
  //      var elemClose = $('<i class="layui-icon" close>&#x1006;</i>');
  //      layero.append(elemClose);
  //      elemClose..unbind('click').bind('click', function () {
  //        layer.close(index);
  //      });
  //      typeof success === 'function' && success.apply(this, arguments);
  //    }
  //  }, options))
  //};
  //
  ////异常提示
  //view.error = function (content, options) {
  //  return view.popup($.extend({
  //    content   : content
  //    , maxWidth: 300
  //    //,shade: 0.01
  //    , offset  : 't'
  //    , anim    : 6
  //    , id      : 'LAY_adminError'
  //  }, options))
  //};
  //
  ////发送验证码
  //view.sendAuthCode = function (options) {
  //  options = $.extend({
  //    seconds      : 60
  //    , elemPhone  : '#LAY_phone'
  //    , elemVercode: '#LAY_vercode'
  //  }, options);
  //
  //  var seconds = options.seconds
  //    , btn = $(options.elem)
  //    , token = null
  //    , timer, countDown = function (loop) {
  //      seconds--;
  //      if (seconds < 0) {
  //        btn.removeClass(DISABLED).html('获取验证码');
  //        seconds = options.seconds;
  //        clearInterval(timer);
  //      } else {
  //        btn.addClass(DISABLED).html(seconds + '秒后重获');
  //      }
  //
  //      if (!loop) {
  //        timer = setInterval(function () {
  //          countDown(true);
  //        }, 1000);
  //      }
  //    };
  //
  //  options.elemPhone = $(options.elemPhone);
  //  options.elemVercode = $(options.elemVercode);
  //
  //  btn..unbind('click').bind('click', function () {
  //    var elemPhone = options.elemPhone
  //      , value = elemPhone.val();
  //
  //    if (seconds !== options.seconds || $(this).hasClass(DISABLED)) return;
  //
  //    if (!/^1\d{10}$/.test(value)) {
  //      elemPhone.focus();
  //      return layer.msg('请输入正确的手机号')
  //    }
  //
  //    if (typeof options.ajax === 'object') {
  //      var success = options.ajax.success;
  //      delete options.ajax.success;
  //    }
  //
  //    view.req($.extend(true, {
  //      url      : '/auth/code'
  //      , type   : 'get'
  //      , data   : {
  //        phone: value
  //      }
  //      , success: function (res) {
  //        layer.msg('验证码已发送至你的手机，请注意查收', {
  //          icon   : 1
  //          , shade: 0
  //        });
  //        options.elemVercode.focus();
  //        countDown();
  //        success && success(res);
  //      }
  //    }, options.ajax));
  //  });
  //}

  ////屏幕类型
  //view.screen = function () {
  //  var width = $win.width();
  //  if (width >= 1200) {
  //    return 3; //大屏幕
  //  } else if (width >= 992) {
  //    return 2; //中屏幕
  //  } else if (width >= 768) {
  //    return 1; //小屏幕
  //  } else {
  //    return 0; //超小屏幕
  //  }
  //}
  //
  ////xss 转义
  //view.escape = function (html) {
  //  return String(html || '').replace(/&(?!#?[a-zA-Z0-9]+;)/g, '&amp;')
  //    .replace(/</g, '&lt;').replace(/>/g, '&gt;')
  //    .replace(/'/g, '&#39;').replace(/"/g, '&quot;');
  //}

  /**
   * 带data-url参数的跳转
   */
  view.bindDataUrl = function () {
    $('*[data-url]').unbind('click').bind('click', function () {
      var that = $(this);
      var url = that.data('url');
      view.changeUrl4Token(url);
    });
  }

  /**
   * 带show参数的img点击放大
   */
  view.bindImgShow = function () {
    $('img[show]').unbind('click').bind('click', function () {
      var that = $(this);
      layer.open({
        type: 1,
        title: false,
        area: 'auto',
        maxWidth: 800,
        maxHeight: 600,
        shadeClose: true,
        closeBtn: 0,
        content: '<img style="width:100%; height:100%;" src="' + that.attr('src') + '">'
      });
    });
  }

  /**
   * 获取hash并跳转到锚点
   */
  view.execHash = function () {
    var hash = window.location.hash;
    if (hash) {
      $('#' + hash.substr(1)).get(0).click();
    }
  }

  /**
   * 验证是否绑定手机号
   */
  view.validSjh = function () {
    var headers = {};
    headers[setter.request.tokenName] = view.getToken();
    $.ajax({
      type: 'post'
      , url: layui.cache.path + '/myd/isBindSjh'
      , dataType: 'json'
      , header: headers
      , data: {}
      , success: function (res) {
        var resp = res['resp'];

        var resp_code = resp['resp_code'];

        //登录过期
        if (resp_code == keys.RESP_CODE_NOTOKEN) {
          view.token();
          return;
        }

        if (resp_code != keys.RESP_CODE_SUCCESS) {
          var resp_desc = resp['resp_desc'];
          top.layer.msg(keys.RESP_DESC_ERROR, {icon: 5});
          layui.hint().error('Resp[' + resp_code + ',' + resp_desc + ',' + debug() + ']');
          typeof fail === 'function' && fail(res);
          typeof done === 'function' && done(res);
          return;
        }

        var biz_code = resp['biz_code'];
        if (biz_code != keys.BIZ_CODE_SUCCESS) {
          var biz_desc = resp['biz_desc'];
          top.layer.msg(biz_desc);
          typeof fail === 'function' && fail(res);
          typeof done === 'function' && done(res);
          return;
        }

        var data = res.data;
        if (data.is_bind_sjh == 'NO') {
          layer.open({
            title: '重要提示',
            icon: 5,
            content: '您尚未绑定手机号',
            btn: '立即绑定',
            btnAlign: 'c',
            closeBtn: false,
            resize: false,
            scrollbar: false,
            yes: function (index, layero) {
              layer.close(index);
              view.changeUrl4Token("/my/setSjh");
            }
          });
        }
      }
      , error: function (e, code) {
        layer.close(loadIndex);
        top.layer.msg('请求接口异常，请重试！', {icon: 5});
        layui.hint().error('Error[' + e.status + ',' + code + ',' + debug() + ']');
      }
    });
  }

  /**
   * 记录推荐人
   */
  view.setTjr = function () {
    var tjr_id = view.getUrlParam('tjr_id');
    if (tjr_id) {
      cookie.set('tjr_id', tjr_id, {expires: 7, path: '/'});
      layui.data(setter.tableName, {
        key: 'tjr_id',
        value: tjr_id
      });
    }
  }

  /**
   * 添加固定工具
   */
  view.fixbar = function (options) {
    options = options || {};
    util.fixbar($.extend({
      dom: '.main',
      bar1: true,
      bar2: true,
      showHeight: 1,
      css: {
        right: 10,
        bottom: 80
      },
      click: function (type) {
        if (type === 'bar1') {
          view.changeUrl("/bz#ptfw")
        } else if (type === 'bar2') {
          view.changeUrl("/bz#sybz")
        }
      }
    }, options));
  }

  /**
   * 检查预设行程
   */
  view.checkInfo = function (options) {
    options = options || {};
    var isCheckJbxx = options.isCheckJbxx || false;
    var isCheckClxx = options.isCheckClxx || false;
    var isCheckLx = options.isCheckLx || false;
    var isCheckXc = options.isCheckXc || false;

    var checkJbxx = function () {
      if (isCheckJbxx && $('#_jbxx').val() === 'NO') {
        layer.confirm('你尚未完善基本信息，填写姓名（或昵称）、QQ号、微信号、支付宝可以让别人联系你或给你转钱更方便噢', {
          icon: 6,
          btn: ['立即完善', '知道了']
        }, function () {
          view.changeUrl('/myJbxx')
        }, function () {
          checkClxx();
        });
      } else {
        checkClxx();
      }
    }

    var checkClxx = function () {
      if (isCheckClxx && $('#_yh_lx').val() === 'C' && $('#_clxx').val() === 'NO') {
        layer.confirm('你尚未完善车辆信息，完善车辆信息别人更容易识别你的爱车噢', {
          icon: 6,
          btn: ['立即完善', '知道了']
        }, function () {
          view.changeUrl('/myClxx')
        }, function () {
          checkLx();
        });
      } else {
        checkLx();
      }
    }

    var checkLx = function () {
      if (isCheckLx && parseInt($('#_sl_lx').val()) === 0) {
        layer.confirm('你尚未添加常用路线，添加后在行程>常用路线中可1秒快速发布噢', {
          icon: 6,
          btn: ['立即添加', '知道了']
        }, function () {
          view.changeUrl('/myLx')
        }, function () {
          checkXc();
        });
      } else {
        checkXc();
      }
    }

    var checkXc = function () {
      if (isCheckXc && parseInt($('#_sl_xc_default').val()) === 0) {
        layer.confirm('你尚发布任何行程，发布后别人才能知道你从哪里到哪里，什么时候出发噢', {
          icon: 6,
          btn: ['立即发布', '知道了']
        }, function () {
          view.changeUrl('/xcCy')
        }, function () {

        });
      }
    }

    checkJbxx();
  }

  /**
   * 初始化
   */
  view.init = function () {
    //左上返回
    $('.top > .nav-list > li[back]').unbind('click').bind('click', function () {
      view.back();
    });

    //绑定data-url点击跳转事件
    view.bindDataUrl();

    //绑定图片带show属性的点击放大查看
    view.bindImgShow();
  }

  //请求模板文件渲染
  //Class.prototype.render = function (views, params) {
  //  var that = this, router = layui.router();
  //  views = setter.views + views + setter.engine;
  //
  //  $('#' + LAY_BODY).children('.layadmin-loading').remove();
  //  view.loading(that.container); //loading
  //
  //  //请求模板
  //  $.ajax({
  //    url       : views
  //    , type    : 'get'
  //    , dataType: 'html'
  //    , data    : {
  //      v: layui.cache.version
  //    }
  //    , success : function (html) {
  //      html = '<div>' + html + '</div>';
  //
  //      var elemTitle = $(html).find('title')
  //        , title = elemTitle.text() || (html.match(/\<title\>([\s\S]*)\<\/title>/) || [])[1];
  //
  //      var res = {
  //        title : title
  //        , body: html
  //      };
  //
  //      elemTitle.remove();
  //      that.params = params || {}; //获取参数
  //
  //      if (that.then) {
  //        that.then(res);
  //        delete that.then;
  //      }
  //
  //      that.parse(html);
  //      view.removeLoad();
  //
  //      if (that.done) {
  //        that.done(res);
  //        delete that.done;
  //      }
  //
  //    }
  //    , error   : function (e) {
  //      view.removeLoad();
  //
  //      if (that.render.isError) {
  //        return view.error('请求视图文件异常，状态：' + e.status);
  //      }
  //      ;
  //
  //      if (e.status === 404) {
  //        that.render('template/tips/404');
  //      } else {
  //        that.render('template/tips/error');
  //      }
  //
  //      that.render.isError = true;
  //    }
  //  });
  //  return that;
  //};
  //
  ////解析模板
  //Class.prototype.parse = function (html, refresh, callback) {
  //  var that = this
  //    , isScriptTpl = typeof html === 'object' //是否模板元素
  //    , elem = isScriptTpl ? html : $(html)
  //    , elemTemp = isScriptTpl ? html : elem.find('*[template]')
  //    , fn = function (options) {
  //      var tpl = laytpl(options.dataElem.html());
  //
  //      options.dataElem.after(tpl.render($.extend({
  //        params: router.params
  //      }, options.res)));
  //
  //      typeof callback === 'function' && callback();
  //
  //      try {
  //        options.done && new Function('d', options.done)(options.res);
  //      } catch (e) {
  //        console.error(options.dataElem[0], '\n存在错误回调脚本\n\n', e)
  //      }
  //    }
  //    , router = layui.router();
  //
  //  elem.find('title').remove();
  //  that.container[refresh ? 'after' : 'html'](elem.children());
  //
  //  router.params = that.params || {};
  //
  //  //遍历模板区块
  //  for (var i = elemTemp.length; i > 0; i--) {
  //    (function () {
  //      var dataElem = elemTemp.eq(i - 1)
  //        , layDone = dataElem.attr('lay-done') || dataElem.attr('lay-then') //获取回调
  //        , url = laytpl(dataElem.attr('lay-url') || '').render(router) //接口 url
  //        , data = laytpl(dataElem.attr('lay-data') || '').render(router) //接口参数
  //        , headers = laytpl(dataElem.attr('lay-headers') || '').render(router); //接口请求的头信息
  //
  //      try {
  //        data = new Function('return ' + data + ';')();
  //      } catch (e) {
  //        hint.error('lay-data: ' + e.message);
  //        data = {};
  //      }
  //      ;
  //
  //      try {
  //        headers = new Function('return ' + headers + ';')();
  //      } catch (e) {
  //        hint.error('lay-headers: ' + e.message);
  //        headers = headers || {}
  //      }
  //      ;
  //
  //      if (url) {
  //        view.req({
  //          type      : dataElem.attr('lay-type') || 'get'
  //          , url     : url
  //          , data    : data
  //          , dataType: 'json'
  //          , headers : headers
  //          , success : function (res) {
  //            fn({
  //              dataElem: dataElem
  //              , res   : res
  //              , done  : layDone
  //            });
  //          }
  //        });
  //      } else {
  //        fn({
  //          dataElem: dataElem
  //          , done  : layDone
  //        });
  //      }
  //    }());
  //  }
  //
  //  return that;
  //};
  //
  ////自动渲染数据模板
  //Class.prototype.autoRender = function (id, callback) {
  //  var that = this;
  //  $(id || 'body').find('*[template]').each(function (index, item) {
  //    var othis = $(this);
  //    that.container = othis;
  //    that.parse(othis, 'refresh');
  //  });
  //};
  //
  ////直接渲染字符
  //Class.prototype.send = function (views, data) {
  //  var tpl = laytpl(views || this.container.html()).render(data || {});
  //  this.container.html(tpl);
  //  return this;
  //};
  //
  ////局部刷新模板
  //Class.prototype.refresh = function (callback) {
  //  var that = this
  //    , next = that.container.next()
  //    , templateid = next.attr('lay-templateid');
  //
  //  if (that.id != templateid) return that;
  //
  //  that.parse(that.container, 'refresh', function () {
  //    that.container.siblings('[lay-templateid="' + that.id + '"]:last').remove();
  //    typeof callback === 'function' && callback();
  //  });
  //
  //  return that;
  //};
  //
  ////视图请求成功后的回调
  //Class.prototype.then = function (callback) {
  //  this.then = callback;
  //  return this;
  //};
  //
  ////视图渲染完毕后的回调
  //Class.prototype.done = function (callback) {
  //  this.done = callback;
  //  return this;
  //};

  //对外接口
  exports('view', view);
});