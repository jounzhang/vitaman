layui.use(['index', 'form', 'layout', 'top'], function () {
  var $ = layui.$,
    admin = layui.admin,
    form = layui.form,
    layout = layui.layout,

    hyz = $('#hyz')
  ;

  admin.fixbar();
  admin.checkInfo({
    isCheckJbxx: true,
    isCheckClxx: true
  });

  layout.layout(3);

  $('button[qd]').unbind('click').bind('click', function () {
    var that = $(this);
    admin.req({
      type: 'post',
      data: {},
      url: '/myQd',
      success: function (res) {
        var data = res.data;
        hyz.text(data.hyz);

        that.addClass('layui-btn-disabled');
        that.unbind('click');

        admin.msg('记得坚持每天都来噢');
      }
    })
    return false;
  });

  form.on('switch(dx_is_kq_sq)', function (data) {
    admin.req({
      type: 'post',
      data: {
        dx_is_kq_sq: data.elem.checked ? 'YES' : 'NO'
      },
      url: '/myJbxxDxIsKqSq',
      success: function (res) {
        if (data.elem.checked) {
          admin.msg('开启成功');
        } else {
          admin.msg('关闭成功');
        }
      },
      fail: function () {
        form.val('formContent', {
          dx_is_kq_sq: false
        });
      }
    })
  });

  form.on('switch(dx_is_kq_qr)', function (data) {
    admin.req({
      type: 'post',
      data: {
        dx_is_kq_qr: data.elem.checked ? 'YES' : 'NO'
      },
      url: '/myJbxxDxIsKqQr',
      success: function (res) {
        if (data.elem.checked) {
          admin.msg('开启成功');
        } else {
          admin.msg('关闭成功');
        }
      },
      fail: function () {
        form.val('formContent', {
          dx_is_kq_qr: false
        });
      }
    })
  });

  form.on('switch(dx_is_kq_jj)', function (data) {
    admin.req({
      type: 'post',
      data: {
        dx_is_kq_jj: data.elem.checked ? 'YES' : 'NO'
      },
      url: '/myJbxxDxIsKqJj',
      success: function (res) {
        if (data.elem.checked) {
          admin.msg('开启成功');
        } else {
          admin.msg('关闭成功');
        }
      },
      fail: function () {
        form.val('formContent', {
          dx_is_kq_jj: false
        });
      }
    })
  });

  $('#xgSjh').unbind('click').bind('click', function () {
    admin.changeUrl("/mySjh")
    return false;
  });

  form.on('switch(sjh_is_gk)', function (data) {
    admin.req({
      type: 'post',
      data: {
        sjh_is_gk: data.elem.checked ? 'YES' : 'NO'
      },
      url: '/myJbxxSjh',
      success: function (res) {
        if (data.elem.checked) {
          admin.msg('公开成功');
        } else {
          admin.msg('隐藏成功');
        }
      },
      fail: function () {
        form.val('formContent', {
          sjh_is_gk: false
        });
      }
    })
  });

  $('input[type=button][zx]').unbind('click').bind('click', function () {
    layer.confirm('确认注销？', {icon: 3, title: '提示'}, function (index) {
      admin.changeUrl("/zx")
    })
    return false;
  });
});