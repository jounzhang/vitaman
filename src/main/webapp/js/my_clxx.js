layui.use(['index', 'form', 'upload'], function () {
  var $ = layui.$,
    admin = layui.admin,
    form = layui.form,

    loadindex = -1,
    showLoading = function () {
      loadindex = layer.load();
    },
    closeLoading = function () {
      loadindex > 0 && (layer.close(loadindex) , loadindex = -1);
    }
  ;

  form.on('submit(btnSubmit)', function (obj) {
    admin.req({
      type: 'post',
      data: obj.field,
      url: '/myClxxSave',
      success: function (res) {
        admin.msg('保存成功', function () {
          admin.changeUrl("/my");
        })
      }
    })
    return false;
  });

  //上传车辆照片
  admin.upload({
    elem: '#btnClImg'
    , url: '/myClxxImg'
    , before: function (obj) {
      showLoading();
    }
    , done: function (res) {
      var data = res.data;
      $('#cl_img').attr('src', data['url'] + "?r=" + new Date())
      closeLoading();
    }
    , error: function () {
      closeLoading();
      admin.msg('上传失败');
    }
  });
});