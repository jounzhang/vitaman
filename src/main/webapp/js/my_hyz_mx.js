layui.use(['index', 'laypage'], function () {
  var $ = layui.$,
    laytpl = layui.laytpl,
    admin = layui.admin,
    laypage = layui.laypage,

    pageIndex = $('#pageIndex'),
    pageSize = $('#pageSize'),
    contentList = $('#contentList'),
    tplItem = $('#tplItem'),

    search = function () {
      admin.req({
        type: 'post',
        data: {
          pageIndex: pageIndex.val(),
          pageSize: pageSize.val()
        },
        url: '/myHyzMxList',
        success: function (res) {
          var data = res.data;
          var pageConfig = {
            count: data.total,
            curr: pageIndex.val(),
            limit: pageSize.val(),
            layout: ['prev', 'next'],
            jump: function (obj, first) {
              if (!first) {
                pageIndex.val(obj.curr);
                pageSize.val(obj.limit);
                search();
              }
            }
          };

          laypage.render($.extend({elem: 'pageBottom'}, pageConfig));

          contentList.html('');
          laytpl(tplItem.html()).render(data, function (html) {
            contentList.html(html);
          });
        }
      })
    }
  ;

  search();
});