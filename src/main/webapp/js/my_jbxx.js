layui.use(['index', 'form'], function () {
  var $ = layui.$,
    admin = layui.admin,
    form = layui.form
  ;

  form.on('submit(btnSubmit)', function (obj) {
    admin.req({
      type: 'post',
      data: obj.field,
      url: '/myJbxxSave',
      success: function (res) {
        admin.msg('保存成功', function () {
          admin.changeUrl("/my");
        })
      }
    })
    return false;
  });
});