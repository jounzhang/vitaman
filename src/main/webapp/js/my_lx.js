layui.use(['index', 'form'], function () {
  var $ = layui.$,
    admin = layui.admin,
    form = layui.form,

    nowDate = new Date(),

    lx_id = $('#lx_id')
  ;

  if (!lx_id) {
    form.val('formContent', {
      HHBegin: nowDate.getHours(),
      mmBegin: '00'
    });
  }

  form.on('submit(btnSubmit)', function (obj) {
    admin.req({
      type: 'post',
      data: obj.field,
      url: '/myLxSave',
      success: function (res) {
        admin.msg('保存成功', function () {
          admin.changeUrl("/xcCy");
        })
      }
    })
    return false;
  });
});