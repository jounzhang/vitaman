layui.use(['index', 'form'], function () {
  var $ = layui.$,
    admin = layui.admin,
    form = layui.form,

    sjh = $('#sjh'),
    yzm = $('#yzm'),
    hqyzm = $('#hqyzm'),

    timeoutId,
    start = function () {
      hqyzm.unbind('click');

      var s = hqyzm.text().replace('秒后重新获取', '');
      if (s > 1) {
        hqyzm.text((s - 1) + '秒后重新获取');
        timeoutId = setTimeout(function () {
          start();
        }, 1000);
      } else {
        clearTimeout(timeoutId);
        hqyzm.text('重新获取');
        bindSendCode();
      }
    },
    bindSendCode = function () {
      hqyzm.unbind('click').bind('click', function () {
        var that = $(this);

        if (!sjh.val()) {
          admin.msg('请输入手机号');
          sjh.focus();
          return;
        }

        admin.req({
          type: 'post'
          , url: '/yzm'
          , data: {
            sjh: sjh.val()
          }
          , success: function (res) {
            admin.msg('验证码已发送至手机', function () {
              that.text('120秒后重新获取');
              start();
            });
          }
        });
      });
    }
  ;

  sjh.focus();

  bindSendCode();

  form.on('submit(btnSubmit)', function (obj) {
    admin.req({
      type: 'post',
      data: obj.field,
      url: '/mySjhSave',
      success: function (res) {
        admin.msg('修改成功', function () {
          admin.changeUrl("/my");
        })
      }
    })
    return false;
  });
});