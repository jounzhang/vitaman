layui.use(['index', 'form', 'layout', 'top'], function () {
  var $ = layui.$,
    element = layui.element,
    laytpl = layui.laytpl,
    admin = layui.admin,
    form = layui.form,
    layout = layui.layout,

    contentList = $('#contentList'),
    tplItem = $('#tplItem'),

    checkChilds = function (id) {
      var item = $('#divContent' + id);
      if (item.children().length === 1) {
        item.children().removeClass('layui-hide');
      }
    },
    checkInfo = function () {
      admin.checkInfo({
        isCheckJbxx: true,
        isCheckClxx: true,
        isCheckLx: true,
        isCheckXc: true
      });
    },
    bindDk = function () {
      contentList.find('input[type=button][dk]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        admin.req({
          type: 'post',
          data: {
            xc_id: id,
            zt: 'DK'
          },
          url: '/xcZt',
          success: function (res) {
            that.addClass('layui-hide');
            that.unbind('click');

            var gb = that.next();
            gb.removeClass('layui-hide');
            bindGb();

            admin.msg('别人看得到了');
          }
        });
        return false;
      });
    },
    bindGb = function () {
      contentList.find('input[type=button][gb]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        admin.req({
          type: 'post',
          data: {
            xc_id: id,
            zt: 'GB'
          },
          url: '/xcZt',
          success: function (res) {
            that.addClass('layui-hide');
            that.unbind('click');

            var dk = that.prev();
            dk.removeClass('layui-hide');
            bindDk();

            admin.msg('别人看不到了');
          }
        });
        return false;
      });
    },
    bindEdit = function () {
      contentList.find('input[type=button][edit]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        admin.changeUrl('/xcEdit?xc_id=' + id);
        return false;
      });
    },
    bindSlAdd = function () {
      contentList.find('input[type=button][sladd]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        admin.req({
          type: 'post',
          data: {
            xc_id: id
          },
          url: '/xcSlAdd',
          success: function (res) {
            var data = res.data;
            $('#sl_' + id).text(data.sl);

            admin.req({
              type: 'post',
              data: {
                xc_id: id
              },
              url: '/xcFz',
              success: function (res) {
                var data = res.data;
                var fzBtn = $('#fz_' + id);
                fzBtn.attr('data-clipboard-text', data.xc_copy);
                fzBtn.data('clipboard-text', data.xc_copy);
                admin.msg('更新成功');
              }
            });
          }
        });
        return false;
      });
    },
    bindSlMinus = function () {
      contentList.find('input[type=button][slminus]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        admin.req({
          type: 'post',
          data: {
            xc_id: id
          },
          url: '/xcSlMinus',
          success: function (res) {
            var data = res.data;
            $('#sl_' + id).text(data.sl);

            admin.req({
              type: 'post',
              data: {
                xc_id: id
              },
              url: '/xcFz',
              success: function (res) {
                var data = res.data;
                var fzBtn = $('#fz_' + id);
                fzBtn.attr('data-clipboard-text', data.xc_copy);
                fzBtn.data('clipboard-text', data.xc_copy);
                admin.msg('更新成功');
              }
            });
          }
        });
        return false;
      });
    },
    bindBz = function () {
      contentList.find('input[type=button][bz]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        var bz = that.data('bz');
        layer.prompt({
          formType: 2,
          value: bz,
          title: '请输入备注（' + ($('#_yh_lx').val() === 'C' ? '附近可接送' : '蓝色衣服，深色裤子') + '）'
        }, function (value, index, elem) {
          admin.req({
            type: 'post',
            data: {
              xc_id: id,
              bz: value
            },
            url: '/xcBz',
            success: function (res) {
              var bzSpan = $('#bz_' + id);
              if (value) {
                bzSpan.removeClass('layui-hide');
                bzSpan.html('<br>' + value);
                that.attr('data-bz', value);
                that.data('bz', value);
              } else {
                bzSpan.addClass('layui-hide');
                that.attr('data-bz', '');
                that.data('bz', '');
              }

              admin.req({
                type: 'post',
                data: {
                  xc_id: id
                },
                url: '/xcFz',
                success: function (res) {
                  var data = res.data;
                  var fzBtn = $('#fz_' + id);
                  fzBtn.attr('data-clipboard-text', data.xc_copy);
                  fzBtn.data('clipboard-text', data.xc_copy);
                  admin.msg('更新成功');
                },
                done: function () {
                  layer.close(index);
                }
              });
            }
          });
        });
        return false;
      });
    },
    bindFx = function () {
      contentList.find('input[type=button][fx]').unbind('click').bind('click', function () {
        var that = $(this);
        var xc_bh = that.data('xc-bh');
        admin.changeUrl('/' + xc_bh);
        return false;
      });
    },
    bindDefault = function () {
      contentList.find('input[type=button][default]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        admin.req({
          type: 'post',
          data: {
            xc_id: id
          },
          url: '/xcDefault',
          success: function (res) {
            contentList.find('input[type=button][default]').removeClass("layui-hide");
            that.addClass('layui-hide');

            contentList.find('span[span-default]').addClass("layui-hide");
            $('#spanDefault' + id).removeClass('layui-hide');

            var divRoot = $('#divRoot' + id);
            divRoot.remove();
            contentList.prepend(divRoot);

            bindAll();

            admin.msg('切换成功，快去火速联系');
          }
        });
        return false;
      });
    },
    bindQx = function () {
      contentList.find('input[type=button][qx]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        var is_default = that.data('is-default');
        layer.confirm('取消行程将删除行程及申请记录，确认继续？', {icon: 3, title: '提示'}, function (index) {
          admin.req({
            type: 'post',
            data: {
              xc_id: id
            },
            url: '/xcQx',
            success: function (res) {
              layer.close(index);
              admin.msg('取消成功');

              admin.f5();//由于需要进行信息检查，所以全部采用页面刷新的方式

              //if (is_default === 'YES') {//如果删除的是默认行程，处理逻辑太复杂了，重新查询渲染
              //search();
              //} else {
              //  that.parent().parent().parent().remove();
              //  if (contentList.html().trim().length === 0) {
              //    laytpl(tplItem.html()).render({list: []}, function (html) {
              //      contentList.html(html);
              //    });
              //  }
              //  checkInfo();
              //}
            }
          });
        })
        return false;
      });
    },
    bindZt = function () {
      contentList.find('input[type=button][zt]').unbind('click').bind('click', function () {
        admin.msg('这是个摆设(✪ω✪)');
      });
    },
    bindQr = function () {
      contentList.find('input[type=button][qr]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        admin.req({
          type: 'post',
          data: {
            hb_id: id
          },
          url: '/xcHbQr',
          success: function (res) {
            var data = res.data;
            $('#sl_' + that.data('item-id')).text(data.sl);

            $('#hz_' + id).remove();
            that.addClass("layui-btn-disabled");
            that.unbind('click');
          }
        });
        return false;
      });
    },
    bindJj = function () {
      contentList.find('input[type=button][jj]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        layer.confirm('确认拒绝和对方拼车？', {icon: 3, title: '提示'}, function (index) {
          admin.req({
            type: 'post',
            data: {
              hb_id: id
            },
            url: '/xcHbJj',
            success: function (res) {
              var data = res.data;
              $('#sl_' + that.data('item-id')).text(data.sl);

              that.parent().parent().parent().remove();
              admin.msg("缘已灭，无瓜gei");
              checkChilds(that.data('item-id'));
            }
          });
        })
        return false;
      });
    },
    bindSc = function () {
      contentList.find('input[type=button][sc]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        layer.confirm('确认该行程不与对方继续拼车？', {icon: 3, title: '提示'}, function (index) {
          admin.req({
            type: 'post',
            data: {
              hb_id: id
            },
            url: '/xcHbSc',
            success: function (res) {
              that.parent().parent().parent().remove();
              admin.msg("真是太残忍了");
              checkChilds(that.data('item-id'));
            }
          });
        })
        return false;
      });
    },
    bindUtil = function () {
      element.render('collapse');
    },
    bindCopy = function () {
      var clipboard = new ClipboardJS('input[type=button][fz]');
      clipboard.on('success', function (e) {
        admin.msg('复制成功，快去粘贴');
      });
      clipboard.on('error', function (e) {
        admin.msg('喔豁，当前环境不支持');
      });
    },
    bindAll = function () {
      checkInfo();
      bindDk();
      bindGb();
      bindEdit();
      bindSlAdd();
      bindSlMinus();
      bindBz();
      bindFx();
      bindDefault();
      bindQx();
      bindZt();
      bindQr();
      bindJj();
      bindSc();
      bindUtil();
      admin.bindImgShow();
    },
    search = function () {
      admin.req({
        type: 'post',
        data: {},
        url: '/xcList',
        success: function (res) {
          var data = res.data;

          contentList.html('');
          laytpl(tplItem.html()).render(data, function (html) {
            contentList.html(html);
            bindAll();
          });

          var sy_is_kq_xc = data.sy_is_kq_xc;
          var is_wcl = data.is_wcl;
          if (sy_is_kq_xc === 'YES') {
            if (is_wcl === 'YES') {
              var zxc = '<embed id="zxc" src="img/zxc.wav" hidden="true" autostart="true" loop="true">';
              $(document.body).append(zxc);
              setTimeout(function () {
                $('#zxc').remove();
              }, 4000);
            }

            setTimeout(function () {
              search();
            }, 30000);
          }
        }
      })
    }
  ;

  layout.layout(1);

  admin.fixbar();

  form.on('switch(sy_is_kq_xc)', function (data) {
    admin.req({
      type: 'post',
      data: {
        sy_is_kq_xc: data.elem.checked ? 'YES' : 'NO'
      },
      url: '/myJbxxSyIsKqXc',
      success: function (res) {
        if (data.elem.checked) {
          admin.msg('开启成功');
        } else {
          admin.msg('关闭成功');
        }
      },
      fail: function () {
        form.val('formContent', {
          sy_is_kq_xc: false
        });
      }
    })
  });

  search();

  bindCopy();
});