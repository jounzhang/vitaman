layui.use(['index', 'form', 'laydate'], function () {
  var $ = layui.$,
    admin = layui.admin,
    form = layui.form,
    laydate = layui.laydate,

    cf_rq = $('#cf_rq')
  ;

  laydate.render({
    elem: '#cf_rq',
    btns: ['now'],
    value: cf_rq.val(),
    done: function (value, date, endDate) {
      cf_rq.val(value);
    }
  });

  form.on('submit(btnSubmit)', function (obj) {
    admin.req({
      type: 'post',
      data: obj.field,
      url: '/xcEditSave',
      success: function (res) {
        admin.msg('保存成功', function () {
          admin.changeUrl("/xc");
        })
      }
    })
    return false;
  });

  $('input[type=button][ys]').unbind('click').bind('click', function () {
    var formData = form.val("formContent");
    delete formData.xc_id;
    admin.req({
      type: 'post',
      data: formData,
      url: '/myLxSave',
      success: function (res) {
        admin.msg('添加成功');
      }
    })
    return false;
  });
});