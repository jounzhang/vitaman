layui.use(['index', 'form', 'laydate'], function () {
  var $ = layui.$,
    admin = layui.admin,
    form = layui.form,
    laydate = layui.laydate,

    nowDate = new Date(),

    cf_rq = $('#cf_rq')
  ;

  laydate.render({
    elem: '#cf_rq',
    btns: ['now'],
    value: cf_rq.val(),
    done: function (value, date, endDate) {
      cf_rq.val(value);
    }
  });

  form.val('formContent', {
    HHBegin: nowDate.getHours(),
    mmBegin: '00'
  });

  form.on('select(lx)', function (data) {
    var obj = $(data.elem);
    var item = obj.children('option[value=' + data.value + ']');
    form.val('formContent', {
      mc: item.data('mc'),
      HHBegin: item.data('cf-sj-h'),
      mmBegin: item.data('cf-sj-m'),
      sl: item.data('sl'),
      je: item.data('je'),
      dd_str: item.data('dd-str'),
      bz: item.data('bz')
    });
  });

  form.on('submit(btnSubmit)', function (obj) {
    admin.req({
      type: 'post',
      data: obj.field,
      url: '/xcFbSave',
      success: function (res) {
        admin.msg('发布成功', function () {
          admin.changeUrl("/xc");
        })
      }
    })
    return false;
  });
});