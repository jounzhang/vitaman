layui.use(['index', 'form', 'laypage', 'laydate', 'layout', 'top'], function () {
  var $ = layui.$,
    admin = layui.admin,

    bindSq = function () {
      $('input[type=button][sq]').unbind('click').bind('click', function () {
        var that = $(this);
        var id = that.data('id');
        admin.req({
          type: 'post',
          data: {
            xc_id: id
          },
          url: '/homeXcSq',
          success: function (res) {
            that.addClass('layui-btn-disabled');
            that.unbind('click');
            admin.msg('申请成功，快到行程联系');
            admin.changeUrl('/xc')
          }
        });
        return false;
      });
    }
  ;

  bindSq();
});