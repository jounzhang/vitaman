layui.use(['index', 'form', 'layout', 'top'], function () {
  var $ = layui.$,
    admin = layui.admin
  ;

  admin.fixbar();
  admin.checkInfo({
    isCheckJbxx: false,
    isCheckClxx: false,
    isCheckLx: true,
    isCheckXc: false
  });

  $('input[type=button][bj]').unbind('click').bind('click', function () {
    var that = $(this);
    var id = that.data('id');
    admin.changeUrl('/myLx?lx_id=' + id);
    return false;
  });

  $('input[type=button][sc]').unbind('click').bind('click', function () {
    var that = $(this);
    var id = that.data('id');
    layer.confirm('确认删除？', {icon: 3, title: '提示'}, function (index) {
      admin.req({
        type: 'post',
        data: {
          lx_id: id
        },
        url: '/myLxDel',
        success: function (res) {
          layer.close(index);
          admin.msg('删除成功', function () {
            admin.f5();
          });
        }
      })
    })
    return false;
  });

  $('input[type=button][fb]').unbind('click').bind('click', function () {
    var that = $(this);
    var id = that.data('id');
    admin.req({
      type: 'post',
      data: {
        lx_id: id
      },
      url: '/xcFbLx',
      success: function (res) {
        admin.msg('发布成功', function () {
          admin.changeUrl('/xc');
        });
      }
    })
  })
  return false;
});