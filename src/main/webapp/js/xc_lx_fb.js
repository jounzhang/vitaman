layui.use(['index', 'form', 'laydate'], function () {
  var $ = layui.$,
    admin = layui.admin,
    form = layui.form,
    laydate = layui.laydate,

    cf_rq = $('#cf_rq')
  ;

  laydate.render({
    elem: '#cf_rq',
    btns: ['now'],
    value: cf_rq.val(),
    done: function (value, date, endDate) {
      cf_rq.val(value);
    }
  });

  form.on('submit(btnSubmit)', function (obj) {
    admin.req({
      type: 'post',
      data: obj.field,
      url: '/xcFbSave',
      success: function (res) {
        admin.msg('发布成功', function () {
          admin.changeUrl("/xc");
        })
      }
    })
    return false;
  });
});