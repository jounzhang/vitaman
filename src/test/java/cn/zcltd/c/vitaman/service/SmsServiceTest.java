package cn.zcltd.c.vitaman.service;

import cn.zcltd.btg.set.core.BtgSet;
import cn.zcltd.btg.set.sourcemanager.SourceManager;
import cn.zcltd.btg.set.sourcemanager.XmlFileSourceManager;
import cn.zcltd.c.vitaman.SysConfig;
import cn.zcltd.c.vitaman.util.SmsUtil;
import org.junit.Before;
import org.junit.Test;

public class SmsServiceTest {

    @Before
    public void setUp() throws Exception {
        SourceManager sourceManager = new XmlFileSourceManager("xml", System.getProperty("user.dir") + "/src/main/resources/config/xml", "UTF-8");
        BtgSet.init(SysConfig.isSysDevModel(), sourceManager, SysConfig.getSysCode());
    }

    @Test
    public void sendCode() {
        SmsService.me.sendCode("13541087392", SmsUtil.genSmsCode());
    }

    @Test
    public void xcTzSq() {
        SmsService.me.xcTzSq("13541087392", "1000000张家宁", "2019-05-24 17:30", "保利玫瑰花语到菁蓉国际广场");
    }

    @Test
    public void xcTzQr() {
        SmsService.me.xcTzQr("13541087392", "1000000张家宁", "2019-05-24 17:30", "保利玫瑰花语到菁蓉国际广场");
    }

    @Test
    public void xcTzJj() {
        SmsService.me.xcTzJj("13541087392", "1000000张家宁", "2019-05-24 17:30", "保利玫瑰花语到菁蓉国际广场");
    }
}