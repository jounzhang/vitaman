package cn.zcltd.c.vitaman.task;

import cn.zcltd.btg.sutil.DateUtil;
import cn.zcltd.c.vitaman.JfinalContext;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

public class TaskXcTjTest {

    @Before
    public void setUp() throws Exception {
        JfinalContext.init();
    }

    @Test
    public void work() {
        TaskXcTj.getInstance().work();
    }

    @Test
    public void day() {
        Date workDate = DateUtil.parseDate("2019-05-12");
        do {
            TaskXcTj.getInstance().day(DateUtil.formatDate(workDate));
            workDate = DateUtil.dateAdd(workDate, Calendar.DATE, 1);
        } while (workDate.getTime() < DateUtil.parseDate(DateUtil.formatDate(DateUtil.getNow())).getTime());
    }
}