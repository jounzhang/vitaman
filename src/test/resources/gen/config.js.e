layui.config({
  base: '#(contextPath)/js/modules/',
  path: '#(contextPath)',
  keys: {
    RESP_RESPCODE_SUCCESS: '0000',
    RESP_RESPDESC_SUCCESS: '请求成功',
    RESP_RESPCODE_ERROR: '9999',
    RESP_RESPDESC_ERROR: '系统出错，请联系管理员',
    RESP_RESPCODE_SESSION_TIMEOUT: '-1',
    RESP_RESPDESC_SESSION_TIMEOUT: '登录过期，请重新登录',
    RESP_RESPCODE_WTY_XX: '-2',
    RESP_RESPDESC_WTY_XX: '请先阅读使用协议并同意',
    RESP_BIZCODE_SUCCESS: '0000',
    RESP_BIZDESC_SUCCESS: '操作成功',
    RESP_BIZCODE_ERROR: '9999',
    RESP_BIZDESC_ERROR: '系统出错，请联系管理员'
  }
});