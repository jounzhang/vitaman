#namespace("tj")
  #sql("xc.getByKv")
    SELECT
    	tjxc.*
    FROM
    	t_tj_xc tjxc
    WHERE
    	1 = 1
    #if(xc_rq)
    AND tjxc.xc_rq = #para(xc_rq)
    #end
  #end
#end