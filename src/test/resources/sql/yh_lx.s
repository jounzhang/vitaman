#namespace("yh")
  #sql("lx.countByKv")
    SELECT
    	count(1)
    FROM
    	t_yh_lx lx
    WHERE
    	1 = 1
    #if(lx)
    AND lx.lx = #para(lx)
    #end
    #if(yh_id)
    AND lx.yh_id = #para(yh_id)
    #end
  #end

  #sql("lx.list")
    SELECT
      lx.*
    FROM
      t_yh_lx lx
    WHERE
      1 = 1
    #if(lx)
    AND lx.lx = #para(lx)
    #end
    #if(yh_id)
    AND lx.yh_id = #para(yh_id)
    #end
    ORDER BY
    	lx.cf_sj
  #end
#end