#namespace("yh")
  #sql("xc_hb.countByKv")
    SELECT
    	count(1)
    FROM
    	t_yh_xc_hb hb
    WHERE
    	1 = 1
    #if(lx)
    AND hb.lx = #para(lx)
    #end
    #if(xc_id)
    AND hb.xc_id = #para(xc_id)
    #end
    #if(hb_id)
    AND hb.hb_id = #para(hb_id)
    #end
    #if(sq_rq)
    AND DATE_FORMAT(hb.sq_sj, '%Y-%m-%d') = #para(sq_rq)
    #end
    #if(zt_not_str)
    AND #para(zt_not_str) LIKE concat('%', hb.zt, '%')
    #end
  #end

  #sql("xc_hb.getByKv")
    SELECT
    	hb.*
    FROM
    	t_yh_xc_hb hb
    WHERE
    	1 = 1
    #if(xc_id)
    AND hb.xc_id = #para(xc_id)
    #end
    #if(hb_id)
    AND hb.hb_id = #para(hb_id)
    #end
    #if(ly_xc_id)
    AND hb.ly_xc_id = #para(ly_xc_id)
    #end
  #end

  #sql("xc_hb.list")
    SELECT
    	hb.*, yh.bh,
      yh.xm,
    	yh.sjh,
    	yh.zh_qq,
    	yh.zh_wx,
    	yh.zh_zfb,
    	yh.cl_hm,
    	yh.cl_ppxh,
    	yh.cl_ys,
    	yh.cl_lx,
    	yh.cl_img,
    	yh.hyz,
    	xc.mc,
    	xc.cf_sj,
    	xc.sl,
    	xc.je,
    	xc.dd_str,
    	xc.bz,
    	xc.fb_sj
    FROM
    	t_yh_xc_hb hb
    INNER JOIN t_yh_xc xc ON xc.key_id = hb.ly_xc_id
    INNER JOIN t_yh yh ON yh.key_id = xc.yh_id
    WHERE
    	1 = 1
    #if(xc_id)
    AND hb.xc_id = #para(xc_id)
    #end
    #if(zt_not)
    AND hb.zt != #para(zt_not)
    #end
  #end

  #sql("xc_hb.deleteByKv")
    DELETE xc
    FROM
    	t_yh_xc_hb xc
    WHERE
    	1 = 1
    #if(xc_id)
    AND xc.xc_id = #para(xc_id)
    #end
    #if(ly_xc_id)
    AND xc.ly_xc_id = #para(ly_xc_id)
    #end
  #end
#end