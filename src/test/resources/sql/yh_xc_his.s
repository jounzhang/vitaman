#namespace("yh")
  #sql("xc_his.cdByKv")
    INSERT INTO t_yh_xc_his SELECT
    	xc.*
    FROM
    	t_yh_xc xc
    WHERE
    	1 = 1
    #if(key_id)
    AND xc.key_id = #para(key_id)
    #end
  #end

  #sql("xc_his.countByKv")
    SELECT
    	count(1)
    FROM
    	t_yh_xc_his xc_his
    WHERE
    	1 = 1
    #if(lx)
    AND xc_his.lx = #para(lx)
    #end
    #if(cf_rq)
    AND DATE_FORMAT(xc_his.cf_sj, '%Y-%m-%d') = #para(cf_rq)
    #end
  #end
#end